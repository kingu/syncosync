# Development
We use pycharm for development, but you could use any means you like.
As the philosophy of syncosync is, to use as many Linux features as possible,
testing on your local machine is possible but needs some pre-requirements
and can change some system settings, which you do not like.

So for full functional testing it is easier to use virtual machines.
The setup of those is easy an documented. Have a look at  
[amd_installer_emulation](amd_installer_emulation)

You can start the local scripts (src/soscore/scripts) with your own
user id, but you will receive a lot of exceptions as many of the needed changes
to the local Linux environment need root status.

No comes the list, of what is accessed and needs root or other rights:

* **/etc/passwd** - is used to add, modify and delete sos users. As long as
you do not have a sosusr group aside from the syncosync idea, it is save to
use it. Other users won't/can't be overwritten, deleted or modified. root
privilege is needed.

* **/etc/group** - will get only one more entry: sosusr. root privilege
is needed

* **/etc/syncosync** holds additional sos configuration stuff. You can
either add files here manually or change the access to be open for your user

* **/etc/lsyncd/lsyncd.conf.lua** is the lsyncd configuration

* **/mnt/local** This directory is used for the user data so, basically
it should be save to change it to be accessible by yourself. But as this
is a combined with adding/deleting users, root access is more helpful
for sure.

* **/usr/share/syncosync** holds configs and system descriptions. Copy those
both folders from package-static into that location.

## Demo Mode
Mainly for easier development of the ui, and demonstration purposes there is
a demo mode, which is activated by **setting an empty file
/etc/syncosync/demo**.  Take also care to **unpack /usr/share/syncosync/demodata.tar.gz** so that
a folder /usr/share/syncosync/demodata with many files exists afterwards.
This will provide demodata for accounts, syncstat settings etc.
Still the /etc/syncosync structure should exist.

## Environment
The document root for pycharm should be set to the src directory.
To have the right path to the modules while testing from bash, cd to src
and set the PYTHON_PATH:

    export PYTHONPATH=$PWD

If you decide to run SOS in PyCharm and intend to debug "normal" operations, root is required.
In order to not screw with permissions of PyCharm configurations etc, you should look into https://esmithy.net/2015/05/05/rundebug-as-root-in-pycharm/
TL;DR:
```
sudo visudo -f /etc/sudoers.d/python
insert >>>>>>> <user> <host> = (root) NOPASSWD: <full path to python>
nano <CWD>/venv/bin/python-sudo.sh
insert >>>>>>>
#!/bin/bash
sudo <CWD>/venv/bin/python "$@"
<<<<<<<<
chmod +x <CWD>/venv/bin/python-sudo.sh
```
Then just refer PyCharm to use python-sudo.sh as your interpreter (rather than the python-binary itself)

## Tools / Packages

For building the debian packet there are several tools needed:

    sudo apt install reprepro rsync debuild devscripts gbp git-buildpackage dh-python python3-all

On Ubuntu systems, installing libparted-dev will have to be installed for pyparted.

The build_deb.sh script does build the debian package and uploads it to the repository.
To use it, the following values should be set at .bashrc:

    export DEBFULLNAME="Stephan Skrodzki"
    export DEBEMAIL="stevieh@syncosync.org"
    export REPREPRO_BASE_DIR=/home/skrodzki/Nextcloud/syncosync-repository/debian/
    export SOSDEB_PASSWD="foobar"

The passwd of course the right one and user or even path is up to your gusto.


## Remote access

Avoid passwords. Install your pubkey in root's ssh authorized keys.
For access of the remote webui, setup a tunnel:

	ssh -p 55055 -L 8080:localhost:80 root@remotehost -N

 This tunnels remotehost's (which is accessible via port 55055) port 80 to local port 8080. Means: to access the WebUI on remotehost, open http://localhost:8080
