This is the dockerfile for building the syncosync*.deb file

to use them locally issue:

docker build -t sos-builder .

to use them for the CI/CD on gitlab:

docker build -t registry.gitlab.com/syncosync/syncosync/sos-builder .
docker push registry.gitlab.com/syncosync/syncosync/sos-builder
