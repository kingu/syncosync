import { Pipe, PipeTransform } from '@angular/core';
import {Calculators} from '../utils/calculators';

@Pipe({
  name: 'quotaUsedPercentage'
})
export class QuotaUsedPercentagePipe implements PipeTransform {

  transform(bytesUsed: number, totalSize: number, percentageAllowed: number): number {
    return Calculators.quotaUsed(bytesUsed, totalSize, percentageAllowed);
  }

}
