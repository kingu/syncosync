import { Pipe, PipeTransform } from '@angular/core';
import {LocalizedMessage} from "../model/localizedMessage";
import {LocalizationService} from "../services/localization.service";

@Pipe({
  name: 'localizeMessage'
})
export class LocalizeMessagePipe implements PipeTransform {

  constructor(private localizationService: LocalizationService) {
  }

  transform(localizedMessage: LocalizedMessage): string {
    return this.localizationService.getTranslation(localizedMessage);
  }

}
