import {Pipe, PipeTransform} from '@angular/core';
import {Calculators} from '../utils/calculators';

@Pipe({
    name: 'bytesToGb'
})

export class BytesToGbPipe  implements PipeTransform {
    transform(bytes: number): number {
        return bytes === undefined ? 0 : Calculators.bytesToGb(bytes);
    }
}
