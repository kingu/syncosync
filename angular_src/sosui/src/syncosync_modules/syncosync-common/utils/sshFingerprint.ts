import * as crypto from 'crypto-js';

export class SshFingerprint {
  private static pubre = /^(ssh-[dr]s[as]\s+)|(\s+.+)|\n/g;

  private static colon(hash: string): string {
    return hash.replace(/(.{2})(?=.)/g, '$1:');
  }

  private static hash(key: string): string {
    return crypto.MD5(crypto.enc.Latin1.parse(key)).toString(crypto.enc.Hex);
  }

  public static fingerprint(pubKey: string): string {
    const cleanPub = pubKey.replace(SshFingerprint.pubre, '');
    const pubbuffer = atob(cleanPub);
    return SshFingerprint.colon(SshFingerprint.hash(pubbuffer));
  }
}
