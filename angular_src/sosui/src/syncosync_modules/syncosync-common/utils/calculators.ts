export class Calculators {
    public static blocksToGb(blocks: number) {
        if (blocks === undefined) {
            return 0;
        } else {
            return Math.round(blocks / 1048576);
        }
    }

    public static bytesToGb(bytes: number) {
        if (bytes === undefined) {
            return 0;
        } else {
            return Math.round(bytes / 1e9);
        }
    }

    public static quotaUsed(bytesUsed: number, totalSize: number, percentageAllowed: number) {
        /***
         * param percentageAllowed is 0-100 formatted percentage
         */
        // Typescript is weird. Omitting /100 * 100 makes it wrong by 1e5
        return (bytesUsed) / (totalSize * (percentageAllowed / 100)) * 100;
    }
}
