import {EventEmitter, Injectable, OnInit, Output} from '@angular/core';
import {AbstractSubscriberComponent} from '../abstract-subscriber-component/abstract-subscriber.component';
import {FormBuilder, FormGroup} from '@angular/forms';

@Injectable()
export abstract class AbstractFormComponent<T = any> extends AbstractSubscriberComponent implements OnInit {
  @Output() modelChanged: EventEmitter<T> = new EventEmitter();
  @Output() formSubmittedSuccessfully: EventEmitter<T> = new EventEmitter();
  @Output() model: T = null;

  public formGroup: FormGroup;
  protected formBuilder: FormBuilder;

  protected constructor() {
    super();
    this.formBuilder = new FormBuilder();
  }

  ngOnInit() {
    super.ngOnInit();
  }

  protected abstract createFormGroup(): void;

  public abstract onSubmit(): void;

  protected setModel(newModel: T): void {
    if (this.model === null || this.model === undefined) {
      this.model = newModel;
    } else {
      Object.assign(this.model, newModel);
    }

    this.modelChanged.emit(this.model);
  }
}
