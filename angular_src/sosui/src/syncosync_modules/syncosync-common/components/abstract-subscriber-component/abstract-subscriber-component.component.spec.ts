import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractSubscriberComponent } from './abstract-subscriber.component';

describe('AbstractSubscriberComponentComponent', () => {
  let component: AbstractSubscriberComponent;
  let fixture: ComponentFixture<AbstractSubscriberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbstractSubscriberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractSubscriberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
