import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class AbstractSubscriberComponent implements OnInit, OnDestroy {
  protected unsubscribe: Subject<void> = new Subject();

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
    console.log('Destroy');
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
