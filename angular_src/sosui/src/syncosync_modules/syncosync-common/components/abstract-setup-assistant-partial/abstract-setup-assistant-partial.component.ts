import {EventEmitter, Injectable, OnInit, Output} from '@angular/core';

@Injectable()
export abstract class AbstractSetupAssistantPartialComponent implements OnInit {
  /** EventEmitter to be triggered when the step/configuration has finished **/
  @Output() configurationFinished: EventEmitter<any> = new EventEmitter();

  protected constructor() { }

  ngOnInit(): void {
  }

}
