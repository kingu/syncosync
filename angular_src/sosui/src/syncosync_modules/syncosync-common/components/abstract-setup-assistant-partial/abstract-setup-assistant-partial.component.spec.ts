import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractSetupAssistantPartialComponent } from './abstract-setup-assistant-partial.component';

describe('AbstractSetupAssistantPartialComponent', () => {
  let component: AbstractSetupAssistantPartialComponent;
  let fixture: ComponentFixture<AbstractSetupAssistantPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbstractSetupAssistantPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractSetupAssistantPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
