import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractSyncosyncModalComponent } from './abstract-syncosync-modal.component';

describe('AbstractSyncosyncModalComponent', () => {
  let component: AbstractSyncosyncModalComponent;
  let fixture: ComponentFixture<AbstractSyncosyncModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbstractSyncosyncModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractSyncosyncModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
