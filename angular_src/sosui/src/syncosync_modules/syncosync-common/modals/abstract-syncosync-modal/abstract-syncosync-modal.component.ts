import {Injectable, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Injectable()
export class AbstractSyncosyncModalComponent<T, S> implements OnInit {
  @Input() title: S;
  @Input() content: T;
  @Input() additionalContent: string = null;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  closeModal() {
    this.activeModal.close('Closed');
  }

}
