import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AbstractSyncosyncModalComponent} from "../abstract-syncosync-modal/abstract-syncosync-modal.component";
import {LocalizedMessage} from "../../model/localizedMessage";
import {ConfirmationModalResult} from "../../model/confirmationModalResult";

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent extends AbstractSyncosyncModalComponent<LocalizedMessage, LocalizedMessage> implements OnInit {
  @Output() onResult: EventEmitter<ConfirmationModalResult> = new EventEmitter();

  constructor(public activeModal: NgbActiveModal) {
    super(activeModal);
  }

  ngOnInit(): void {
  }

  confirmModal() {
    this.onResult.emit(ConfirmationModalResult.CONFIRM);
    this.closeModal();
  }

  cancelModal() {
    this.onResult.emit(ConfirmationModalResult.CANCEL);
    this.closeModal();
  }
}
