import {Component, OnInit} from '@angular/core';
import {AbstractSyncosyncModalComponent} from "../abstract-syncosync-modal/abstract-syncosync-modal.component";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {InfoLevel} from "../../model/infoLevel";
import {LocalizedMessage} from "../../model/localizedMessage";

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.css']
})
export class InfoModalComponent extends AbstractSyncosyncModalComponent<LocalizedMessage, InfoLevel> implements OnInit {
  public infoLevelEnum = InfoLevel;
  constructor(public activeModal: NgbActiveModal) {
    super(activeModal);
  }

  ngOnInit(): void {
  }

}
