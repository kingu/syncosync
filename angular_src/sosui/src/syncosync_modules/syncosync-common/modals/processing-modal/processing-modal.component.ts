import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractSyncosyncModalComponent} from "../abstract-syncosync-modal/abstract-syncosync-modal.component";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Observable} from "rxjs";
import {LocalizedMessage} from "../../model/localizedMessage";

@Component({
  selector: 'app-processing-modal',
  templateUrl: './processing-modal.component.html',
  styleUrls: ['./processing-modal.component.css']
})
export class ProcessingModalComponent<T> extends AbstractSyncosyncModalComponent<LocalizedMessage, LocalizedMessage> implements OnInit {
  @Input() cancelObservable: Observable<any> = null;
  @Input() actionAwaitedFor: Observable<T> = null;
  /** Emits the result of the actionAwaitedFor. Thus the action called should handle a cancel properly, even if it is just returning null. **/
  @Output() result: EventEmitter<T> = new EventEmitter<T>();

  constructor(public activeModal: NgbActiveModal) {
    super(activeModal);
  }

  ngOnInit(): void {
    // TODO: Do we need to catch errors here somehow?

  }

  /** Method to be called after actionAwaitedFord (and optionally cancelObservable) has been set. **/
  public startAction(): void {
    this.actionAwaitedFor
        .subscribe(result => {
          this.result.emit(result);
          this.closeModal();
        })
  }

  cancelModal() {
    if (this.cancelObservable == null) {
      console.log("Cannot cancel if there's no information on how to do so.")
      return;
    }
    this.cancelObservable.subscribe(result => {
      console.log("Cancelled processing with result: " + result)
    });

  }
}
