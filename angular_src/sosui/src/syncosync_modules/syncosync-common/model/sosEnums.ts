export enum NicMode {
    DHCP = 0,
    STATIC = 1,
    AUTO = 2, // v6 only
    MANUAL = 3 // off
}

export enum RemoteUpStatus {
    // not available (hostname is not set?)
    NOT_AVAILABLE = 0,
    // host is not reachable (wrong hostname or port?)
    NO_REPLY = 1,
    // not successful
    FAULTY = 2,
    // host is up and key is fine
    UP = 3
}

export enum SyncStatus {
    // not available (hostname is not set?)
    NOT_AVAILABLE = 0,
    // there is an active session / synchronization ongoing
    ACTIVE = 1,
    // Connection is idling, no active synchronization
    IDLE = 2,
    // No SSH session present
    NO_SSH = 4,
    // SSH key file is corrupted or the remote host rejected it
    BAD_KEY = 5
}

export enum SyncstatStatus {
    // not available
    NOT_AVAILABLE = 0,
    // available
    AVAILABLE = 1
}
