export class SystemSetupData {
    pub_key: string = "";
    free: number = 0;
    local: number = 0;
    remote: number = 0;
    action: SsdAction = SsdAction.INTERACTIVE_LEADER;

}

export enum SsdAction {
    INTERACTIVE_LEADER,
    INTERACTIVE_FOLLOWER
}