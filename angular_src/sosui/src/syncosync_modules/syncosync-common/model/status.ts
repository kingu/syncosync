
/* tslint:disable:variable-name */
import {RemoteUpStatus, SyncstatStatus, SyncStatus} from './sosEnums';

export class Status {
  syncstat_status: SyncstatStatus;
  remote_up: RemoteUpStatus;
  remote_down_since: number;
  syncup: SyncStatus;
  syncup_change_since: number;
  syncdown: SyncStatus;
  syncdown_change_since: number;
  bw_up: number; // bytes / sec
  bw_down: number; // bytes / sec
  date: number;
  driveup: string[];
  total_size: number;
  free_space: number;
  iused: number;
  to_check: number; // amount of files that have to be checked for sync
  akt_file: string;
  percent: number; // % transferred of akt_file
  admin_last_mail: number;
}

export enum Drivestate {
  down,
  up
}
