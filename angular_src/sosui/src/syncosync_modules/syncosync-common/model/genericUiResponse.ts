import {LocalizedMessage} from "./localizedMessage";

export class GenericUiResponse {
    public status: GenericUiResponseStatus = GenericUiResponseStatus.OK;
    public localized_reason: LocalizedMessage = null;
    public additional_information: string = null;
}


export enum GenericUiResponseStatus {
    OK = 0,
    ERROR = 1
}