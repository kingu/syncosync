import {Directive, OnInit} from '@angular/core';
import {FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {validatePassword} from "../validators/FormValidators";

@Directive({
  selector: '[validateAccountPassword]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ValidateAccountPasswordDirective, multi: true }
  ]
})
export class ValidateAccountPasswordDirective implements Validator, OnInit {

  ngOnInit() {
  }

  validate(formControl: FormControl) : ValidationErrors | null {
    return validatePassword(formControl);
  }
}