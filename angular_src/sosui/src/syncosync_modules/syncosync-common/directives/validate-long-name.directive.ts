import {Directive, OnInit} from '@angular/core';
import {FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {validateAccountLongName} from "../validators/FormValidators";

@Directive({
  selector: '[validateLongName]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ValidateLongNameDirective, multi: true }
  ]
})
export class ValidateLongNameDirective implements Validator, OnInit {

  ngOnInit() {
  }

  validate(formControl: FormControl) : ValidationErrors | null {
    return validateAccountLongName(formControl);
  }
}