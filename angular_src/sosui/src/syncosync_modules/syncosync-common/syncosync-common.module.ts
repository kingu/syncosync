import {APP_INITIALIZER, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {BytesToGbPipe} from "./pipes/bytes-to-gb-pipe";
import {QuotaUsedPercentagePipe} from "./pipes/quota-used-percentage.pipe";
import { ValidateLongNameDirective } from './directives/validate-long-name.directive';
import {FormsModule} from "@angular/forms";
import { ValidateAccountNameDirective } from './directives/validate-account-name.directive';
import { ValidateAccountPasswordDirective } from './directives/validate-account-password.directive';
import { ConfirmationModalComponent } from './modals/confirmation-modal/confirmation-modal.component';
import { ProcessingModalComponent } from './modals/processing-modal/processing-modal.component';
import { InfoModalComponent } from './modals/info-modal/info-modal.component';
import { LocalizeMessagePipe } from './pipes/localize-message.pipe';
import {LocalizationService} from "./services/localization.service";

/** Upon loading the module, we want to request the localized messages in order to have them at hand
 * whenever we need it **/
export function requestTranslations(localizationService: LocalizationService) {
    return () => localizationService.initTranslations();
}

@NgModule({
    exports: [
        BytesToGbPipe,
        QuotaUsedPercentagePipe,
        ValidateLongNameDirective,
        ValidateAccountNameDirective,
        ValidateAccountPasswordDirective
    ],
  declarations: [
    BytesToGbPipe,
    QuotaUsedPercentagePipe,
    ValidateLongNameDirective,
    ValidateAccountNameDirective,
    ValidateAccountPasswordDirective,
    ConfirmationModalComponent,
    ProcessingModalComponent,
    InfoModalComponent,
    LocalizeMessagePipe,
  ],
  imports: [
      FormsModule,
      CommonModule
  ],
    providers: [
        { provide: APP_INITIALIZER,useFactory: requestTranslations, multi: true, deps: [LocalizationService]}
    ],
})
export class SyncosyncCommonModule { }
