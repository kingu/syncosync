import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {SosServiceBaseUnauthenticatedService} from "./sos-service-base-unauthenticated.service";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {LocalizedMessage} from "../model/localizedMessage";
import {UiLanguageService} from "../../public/services/ui-language.service";
import {syncosyncEndpoints} from "../../../environments/endpoints";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class LocalizationService extends SosServiceBaseUnauthenticatedService {
  protected serviceName: string = "LocalizationService";

  private translations: Map<LocalizedMessage, string> = new Map();

  protected constructor(
      protected httpClient: HttpClient,
      protected authService: AuthService,
      @Inject(LOCALE_ID) protected localeId: string
  ) {
    super(httpClient, authService);
  }

  public getTranslation(localizedMessagedIdentifier: LocalizedMessage): string {
    if (this.translations.size == 0) {
      this.requestTranslations();
      return "error " + localizedMessagedIdentifier;
    } else if (!this.translations.has(localizedMessagedIdentifier)) {
      return `Missing localization for ${localizedMessagedIdentifier.valueOf().toString()}`;
    } else {
      return this.translations.get(localizedMessagedIdentifier);
    }

  }

  public initTranslations(): void {
    this.requestTranslations();
  }

  // TODO: Try until we get a result?
  private requestTranslations(): void {
    const url = syncosyncEndpoints.resource.localizedMessagesPartialUrl(this.localeId);
    this.httpClient.get<Object>(url)
        .pipe(
            catchError(this.handleError<Object>('requestTranslations'))
        )
        .subscribe(res => {
          Object.keys(res).forEach(
              key => this.translations.set(LocalizedMessage[LocalizedMessage[+key]], res[key])
          );
        });
  }
}
