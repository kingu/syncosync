import { TestBed } from '@angular/core/testing';

import { SosServiceBaseService } from './sos-service-base.service';

describe('SosServiceBaseService', () => {
  let service: SosServiceBaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SosServiceBaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
