import {EventEmitter, Injectable} from '@angular/core';
import {SosServiceBaseUnauthenticatedService} from "./sos-service-base-unauthenticated.service";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";
import {ConfirmationModalComponent} from "../modals/confirmation-modal/confirmation-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {LocalizedMessage} from "../model/localizedMessage";
import {ConfirmationModalResult} from "../model/confirmationModalResult";
import {InfoLevel} from "../model/infoLevel";
import {InfoModalComponent} from "../modals/info-modal/info-modal.component";
import {Observable} from "rxjs";
import {ProcessingModalComponent} from "../modals/processing-modal/processing-modal.component";

@Injectable({
  providedIn: 'root'
})
export class SyncosyncModalService extends SosServiceBaseUnauthenticatedService {
  protected serviceName: string = "SyncosyncModalServiceService";

  protected constructor(
      protected httpClient: HttpClient,
      protected authService: AuthService,
      private modalService: NgbModal
  ) {
    super(httpClient, authService);
  }

  public showConfirmationModal(title: LocalizedMessage, content: LocalizedMessage, additionalContent?: string) : EventEmitter<ConfirmationModalResult> {
    const confirmationModal = this.modalService.open(ConfirmationModalComponent,
        { size: 'lg', backdrop: 'static', keyboard: false});

    confirmationModal.componentInstance.content = content;
    confirmationModal.componentInstance.additionalContent = additionalContent;
    confirmationModal.componentInstance.title = title;

    return confirmationModal.componentInstance.onResult;
  }

  public showInfoModal(infoLevel: InfoLevel, content: LocalizedMessage, additionalContent?: string) : void {
    const infoModal = this.modalService.open(InfoModalComponent,
        { size: 'lg', backdrop: 'static', keyboard: false});

    infoModal.componentInstance.content = content;
    infoModal.componentInstance.title = infoLevel;
    infoModal.componentInstance.additionalContent = additionalContent;
  }

  /** Shows the processing modal that optionally offers a cancel button if an action for cancelling the action is
   * passed. The resultEmitter has to be subscribed for beforehand in order to be notified about the result.
   * @param title
   * @param content
   * @param actionAwaitedFor
   * @param cancelObservable may be null in order to have a non-cancellable modal
   * @param resultEmitter
   * @param additionalContent
   */
  public showProcessingModal<T>(title: LocalizedMessage, content: LocalizedMessage, actionAwaitedFor: Observable<T>,
                                cancelObservable: Observable<any>, resultEmitter: EventEmitter<T>, additionalContent?: string) : void {
    const processingModal = this.modalService.open(ProcessingModalComponent,
        { size: 'lg', backdrop: 'static', keyboard: false});

    processingModal.componentInstance.content = content;
    processingModal.componentInstance.title = title;
    processingModal.componentInstance.actionAwaitedFor = actionAwaitedFor;
    processingModal.componentInstance.cancelObservable = cancelObservable;
    processingModal.componentInstance.result = resultEmitter;
    processingModal.componentInstance.additionalContent = additionalContent;
    processingModal.componentInstance.startAction();
  }
}
