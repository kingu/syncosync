import { Injectable } from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthService, LoginState} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export abstract class SosServiceBaseService {
  protected abstract serviceName: string;
  protected unsubscribe: Subject<void> = new Subject();
  protected continue: Subject<void> = new Subject();

  protected constructor(
      protected httpClient: HttpClient,
      protected authService: AuthService
  ) {
    authService.registerNotification(this);
  }

  protected log(message: string): void {
    console.log(`${this.serviceName}: ${message}`);
  }

  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error === undefined) return of(result as T);
      if (error.statusCode == 401) {
        this.unsubscribe.next();
        this.unsubscribe.complete();
      }
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for account consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Upon login/logout, the AuthService will notify the service about the state which needs to be handled individually
   * @param loginState
   */
  abstract notifyLoginState(loginState: LoginState);
}
