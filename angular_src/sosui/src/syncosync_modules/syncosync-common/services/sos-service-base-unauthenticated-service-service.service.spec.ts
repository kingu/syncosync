import { TestBed } from '@angular/core/testing';

import { SosServiceBaseUnauthenticatedService } from './sos-service-base-unauthenticated.service';

describe('SosServiceBaseUnauthenticatedServiceServiceService', () => {
  let service: SosServiceBaseUnauthenticatedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SosServiceBaseUnauthenticatedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
