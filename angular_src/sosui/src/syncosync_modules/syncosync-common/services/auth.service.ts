import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {SosuiUser} from '../model/sosui-user.model';
import {map} from 'rxjs/operators';
import {syncosyncEndpoints} from '../../../environments/endpoints';
import {SosServiceBaseService} from "./sos-service-base.service";


export enum LoginState {
    LOGIN,
    LOGOUT
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

    // TODO: visitor pattern for auth vs public services to pause/unpause depending on login/logout
    private notifyLoginLogoutServices: SosServiceBaseService[] = []
  private userSubject: BehaviorSubject<SosuiUser>;
  public user: Observable<SosuiUser>;

  constructor(
      private router: Router,
      private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject<SosuiUser>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): SosuiUser {
      return this.userSubject.value;
  }

  public registerNotification(service: SosServiceBaseService) {
      this.notifyLoginLogoutServices.push(service);
  }

  public unregisterNotification(service: SosServiceBaseService) {
      const index = this.notifyLoginLogoutServices.indexOf(service);
      if (index > -1) {
          this.notifyLoginLogoutServices.splice(index, 1);
      }
  }

  login(password: string) {
    return this.http.post<SosuiUser>(syncosyncEndpoints.authLoginUrl, { password })
        .pipe(map(user => {
          console.log(user);
          const userToStore = new SosuiUser();
          userToStore.token = user.token;
          // store account details and jwt token in local storage to keep account logged in between page refreshes
          localStorage.setItem('user', JSON.stringify(userToStore));
          this.userSubject.next(user);
          this.notifyLoginLogoutServices.map(service => service.notifyLoginState(LoginState.LOGIN))
          return user;
        }));
  }

  logout() {
    // remove account from local storage and set current account to null
    localStorage.removeItem('user');
    this.userSubject.next(null);
      this.notifyLoginLogoutServices.map(service => service.notifyLoginState(LoginState.LOGOUT))
      this.router.navigate(['/auth/login']);
  }

  /*update(newPassword: string) {
    return this.http.put(`${environment.apiUrl}/users/${id}`, params)
        .pipe(map(x => {
          // update stored account if the logged in account updated their own record
          if (id == this.userValue.id) {
            // update local storage
            const account = { ...this.userValue, ...params };
            localStorage.setItem('account', JSON.stringify(account));

            // publish updated account to subscribers
            this.userSubject.next(account);
          }
          return x;
        }));
  }*/
}
