import {APP_INITIALIZER, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SyncosyncPublicGraphsModule} from "./graphs/syncosync-public-graphs.module";
import {FormsModule} from "@angular/forms";
import { LanguageSwitchComponent } from './components/language-switch/language-switch.component';
import {NgxFlagPickerModule} from "ngx-flag-picker";


@NgModule({
  declarations: [LanguageSwitchComponent],
    imports: [
        FormsModule,
        CommonModule,
        NgxFlagPickerModule
    ],
    exports: [
        SyncosyncPublicGraphsModule,
        LanguageSwitchComponent
    ],

})
export class SyncosyncPublicModule { }
