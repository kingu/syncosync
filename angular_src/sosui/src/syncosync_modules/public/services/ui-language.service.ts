import { Injectable } from '@angular/core';
import {SosServiceBaseUnauthenticatedService} from "../../syncosync-common/services/sos-service-base-unauthenticated.service";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AuthService} from "../../syncosync-common/services/auth.service";
import {AccountAdd} from "../../syncosync-common/model/sosaccount";
import {syncosyncEndpoints} from "../../../environments/endpoints";
import {catchError, tap} from "rxjs/operators";
import {SupportedLanguage} from "../../syncosync-common/model/supportedLanguage";
import {UiLanguage} from "../../syncosync-common/model/uiLanguage";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UiLanguageService extends SosServiceBaseUnauthenticatedService {
  protected serviceName: string = "UiLanguageService";

  constructor(
      protected httpClient: HttpClient,
      protected authService: AuthService,
      private router: Router
  ) {
    super(httpClient, authService);
  }

  public changeLanguage(languageToBeSet: UiLanguage) {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    this.httpClient.post(syncosyncEndpoints.public.uiLanguageUrl, languageToBeSet, options)
        .pipe(
            tap(_ => this.log('Changed language')),
            catchError(this.handleError<any>('addAccount', false))
        )
        .subscribe(() => {
          let url = window.location.protocol + "://" + window.location.hostname + ":" + window.location.port
          this.log("Redirecting to " + url)
          document.location.href = "/"
        });
  }

  public getLanguage(): Observable<UiLanguage> {
    return this.httpClient.get<UiLanguage>(syncosyncEndpoints.public.uiLanguageUrl)
        .pipe(
            catchError(this.handleError<UiLanguage>('getLanguage'))
        );
  }
}
