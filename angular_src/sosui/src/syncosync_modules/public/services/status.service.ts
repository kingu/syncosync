import {Injectable} from '@angular/core';
import {Status} from '../../syncosync-common/model/status';
import {BehaviorSubject, Observable, timer} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {syncosyncEndpoints} from '../../../environments/endpoints';
import {AuthService} from "../../syncosync-common/services/auth.service";
import {SosServiceBaseUnauthenticatedService} from "../../syncosync-common/services/sos-service-base-unauthenticated.service";

@Injectable({
  providedIn: 'root'
})
export class StatusService extends SosServiceBaseUnauthenticatedService {
  protected serviceName = 'StatusService';

  private latestStatus: Status = new Status();
  newStatusSubject: BehaviorSubject<any> = new BehaviorSubject<any>(new Status());
  latestStatusObservable$: Observable<Status> = this.newStatusSubject.asObservable();
  statusInterval: Observable<number> = null;

  constructor(protected httpClient: HttpClient,
              protected authService: AuthService) {
    super(httpClient, authService);

    // We never pause this
    this.statusInterval = timer(0, 5000);
    this.statusInterval.pipe(
        switchMap(() => this.fetchStatus())
      )
      .subscribe(res => {
        if (res === undefined) {
          return;
        }
        res.date = Date.now().valueOf();
        /*res.networkState.Down = Math.floor(Math.random() * 100) + 1;
        res.networkState.Up = Math.floor(Math.random() * 40) + 1;*/
        Object.assign(this.latestStatus, res);
        this.newStatusSubject.next(res);
      });
  }

  private fetchStatus(): Observable<Status> {
    return this.httpClient.get<Status>(syncosyncEndpoints.public.statusApiUrl)
      .pipe(
        catchError(this.handleError<Status>('getStatus'))
      );
  }

  public getStatus(): Status {
    return this.latestStatus;
  }

}
