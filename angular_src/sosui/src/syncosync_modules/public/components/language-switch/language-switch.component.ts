import {Component, OnInit} from '@angular/core';
import {UiLanguage} from "../../../syncosync-common/model/uiLanguage";
import {SupportedLanguage, SupportedLanguageUtil} from "../../../syncosync-common/model/supportedLanguage";
import {UiLanguageService} from "../../services/ui-language.service";

@Component({
  selector: 'app-language-switch',
  templateUrl: './language-switch.component.html',
  styleUrls: ['./language-switch.component.css']
})
export class LanguageSwitchComponent implements OnInit {
  selectedCountryCode = '';
  countryCodes = ['', 'us', 'de'];
  customLabels = {
    '': "Browser default",
    'us': 'English',
    'de': 'Deutsch',
  };

  constructor(private uiLanguageService: UiLanguageService) {
    this.uiLanguageService.getLanguage().subscribe(res => {
      if (res === undefined) {
        return;
      }
      if (res.language == SupportedLanguage.ENGLISH) {
        this.selectedCountryCode = "us";
      } else if (res.language == SupportedLanguage.UNSET) {
        this.selectedCountryCode = "";
      } else {
        this.selectedCountryCode = SupportedLanguageUtil.toIso(res.language);
      }
    });
  }

  ngOnInit(): void {
  }


  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;

    if (value == "us") {
      // change it to en-US for now...
      value = "en-US"
    }
    let uiLanguageToSet: UiLanguage = new UiLanguage()
    uiLanguageToSet.language = SupportedLanguageUtil.fromIso(value)

    this.uiLanguageService.changeLanguage(uiLanguageToSet);
    // now reload the page!
  }

}
