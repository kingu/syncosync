import {Component, Input, OnInit} from '@angular/core';
import {StatusService} from '../../services/status.service';
import {takeUntil} from 'rxjs/operators';
import {Status} from '../../../syncosync-common/model/status';
import {faCheckCircle, faExclamationCircle, faSync} from '@fortawesome/free-solid-svg-icons';
import {SizeProp} from '@fortawesome/fontawesome-svg-core';
import {AbstractSubscriberComponent} from '../../../syncosync-common/components/abstract-subscriber-component/abstract-subscriber.component';
import {RemoteUpStatus, SyncStatus} from '../../../syncosync-common/model/sosEnums';

enum SyncDisplayState {
  ACTIVE,
  OK,
  WARNING
}

@Component({
  selector: 'app-sync-status-dot',
  templateUrl: './sync-status-dot.component.html',
  styleUrls: ['./sync-status-dot.component.css']
})
export class SyncStatusDotComponent extends AbstractSubscriberComponent implements OnInit {
  @Input() sizeOfIcon: SizeProp = '4x';
  faSync = faSync;
  faExclamationCircle = faExclamationCircle;
  faCheckCircle = faCheckCircle;
  syncDisplayState = SyncDisplayState;

  statusIconToShow: SyncDisplayState = SyncDisplayState.OK;
  constructor(private statusService: StatusService) {
    super();
  }

  ngOnInit() {
    console.log('OnInit');
    this.statusService.latestStatusObservable$
        .pipe(
            takeUntil(this.unsubscribe)
        )
        .subscribe(status => {
          this.handleStatus(status);
        });
  }

  private handleStatus(status: Status) {
    if (status === undefined || status === null) {
      return;
    }
    if (status.syncdown === SyncStatus.ACTIVE
        || status.syncup === SyncStatus.ACTIVE) {
      this.statusIconToShow = SyncDisplayState.ACTIVE;
    } else if (status.remote_up !== RemoteUpStatus.UP) {
      // display an exclamation mark indicating an issue...
      this.statusIconToShow = SyncDisplayState.WARNING;
    } else {
      this.statusIconToShow = SyncDisplayState.OK;
    }
  }

  /*
  remoteUp:
   ENA: Not Available -> show yellow
   ETRUE: Host is up and responding -> show green
   EFALSE: host is not up or not responding or no key or whatever -> show red
   */
  getColor(): string {
    const latestStatus = this.statusService.getStatus();
    if (latestStatus === undefined) {
      return 'warning';
    }
    switch (latestStatus.remote_up) {
      case RemoteUpStatus.NOT_AVAILABLE:
        return 'warning';
      case RemoteUpStatus.UP:
        return 'ok';
      default:
        return 'error';
    }
  }

  getText() {
    let textToShow = '';
    const latestStatus = this.statusService.getStatus();
    if (latestStatus === undefined) {
      return textToShow;
    }
    // first check if there is an information on the remote host to be shown
    if (latestStatus.remote_up !== RemoteUpStatus.UP) {
      // well, there's an issue with the remote host...
      const remoteDownSince = new Date(latestStatus.remote_down_since);
      textToShow += 'Issue with remote host since ' + remoteDownSince.toLocaleString() + '. ';
    }
    if (latestStatus.akt_file !== undefined && latestStatus.akt_file.trim().length > 0) {
      textToShow += 'Currently syncing file "' + latestStatus.akt_file.trim() + '". ';
    }

    textToShow += 'Sync to partner last changed ' + new Date(latestStatus.syncup_change_since).toLocaleString() + '. ';
    textToShow += 'Sync from partner last changed ' + new Date(latestStatus.syncdown_change_since).toLocaleString() + '. ';
    return textToShow;
  }
}
