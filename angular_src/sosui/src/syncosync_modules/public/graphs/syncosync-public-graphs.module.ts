import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NetspeedGraphComponent} from './netspeed-graph/netspeed-graph.component';
import {ChartsModule} from 'ng2-charts';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    NetspeedGraphComponent,
  ],
  imports: [
    CommonModule,
    ChartsModule,
    FontAwesomeModule,
    NgbModule
  ],
    exports: [
        NetspeedGraphComponent
    ]
})
export class SyncosyncPublicGraphsModule { }
