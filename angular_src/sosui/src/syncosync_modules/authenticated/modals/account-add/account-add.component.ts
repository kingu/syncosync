import {ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {faFileUpload, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AccountApiService} from '../../services/account-api.service';
import {AccountAdd} from '../../../syncosync-common/model/sosaccount';
import {SshFingerprint} from '../../../syncosync-common/utils/sshFingerprint';

@Component({
  selector: 'app-account-add',
  templateUrl: './account-add.component.html',
  styleUrls: ['./account-add.component.css']
})
export class AccountAddComponent implements OnInit {
  faFileUpload = faFileUpload;
  faTrashBin = faTrashAlt;
  infoLevels = ['None', 'Debug', 'Info', 'Warning', 'Error'];
  account: AccountAdd = new AccountAdd();
  newFingerprint: string;
  sshKeyContent: string;

  constructor(private formBuilder: FormBuilder,
              public activeModal: NgbActiveModal,
              private cd: ChangeDetectorRef,
              private accountApiService: AccountApiService
  ) {
  }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.close('Closed');
  }

  onSubmit() {
    console.log('Submitting...');
    if (this.sshKeyContent !== null) {
      this.account.ssh_pub_key = this.sshKeyContent;
    }
    this.accountApiService.addAccount(this.account)
        .subscribe(result => {
          if (result) {
            this.closeModal();
          } else {
            // TODO: show error
          }
        });
  }

  onSSHKeyChange($event, field: string) {
    if ($event.target.files && $event.target.files.length) {
      const [file] = $event.target.files;
      console.log(file);
      // just checking if it is an pub key with file ending .pub or if it's more than 4 MB
      const fileEnding = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (!fileEnding.startsWith('pub') || file.size > 2 ** 12) {
        this.cd.markForCheck();
      } else {
        file.text().then(content => {
          console.log(content);
          console.log(SshFingerprint.fingerprint(content));
          this.newFingerprint = SshFingerprint.fingerprint(content);
          this.sshKeyContent = content;
        });
        // unlike most tutorials, i am using the actual Blob/file object instead of the data-url
        /*this.editUserForm.patchValue({
          [field]: file.text()
        });*/
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      }
    }
  }

}
