import {ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {AccountInfo, AccountAdd} from '../../../syncosync-common/model/sosaccount';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SshFingerprint} from '../../../syncosync-common/utils/sshFingerprint';
import {faFileUpload, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {AccountApiService} from '../../services/account-api.service';

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.css']
})
export class AccountEditComponent implements OnInit {
  @Input() account: AccountInfo;
  faFileUpload = faFileUpload;
  faTrashBin = faTrashAlt;
  infoLevels = ["none", "warning", "error"]
  newFingerprint: string;
  sshKeyContent: string = null;
  editedAccount = new AccountAdd();
  form: any;

  constructor(public formsModule: FormsModule,
              public activeModal: NgbActiveModal,
              private cd: ChangeDetectorRef,
              private accountApiService: AccountApiService
  ) {
  }

  ngOnInit() {
    Object.keys(this.account).forEach(key => this.editedAccount[key] = this.account[key]);
  }

  closeModal() {
    this.activeModal.close('Closed');
  }

  onSubmit() {
    console.log('Submitting...');
    if (this.sshKeyContent !== null) {
      this.editedAccount.ssh_pub_key = this.sshKeyContent;
    }
    this.accountApiService.updateAccount(this.editedAccount)
        .subscribe(result => {
          if (result) {
            this.closeModal();
          } else {
            // TODO: show error
          }
        });
  }

  revert() {
    console.log('Resetting...');
  }

  onSSHKeyChange($event, field: string) {
    if ($event.target.files && $event.target.files.length) {
      const [file] = $event.target.files;
      console.log(file);
      // just checking if it is an pub key with file ending .pub or if it's more than 4 MB
      const fileEnding = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (!fileEnding.startsWith('pub') || file.size > 2 ** 12) {
        /* TODO
        this.editUserForm.get(field).setErrors({
          required: true
        });*/
        this.cd.markForCheck();
      } else {
        file.text().then(content => {
          console.log(content);
          console.log(SshFingerprint.fingerprint(content));
          this.newFingerprint = SshFingerprint.fingerprint(content);
          this.sshKeyContent = content.trim();
        });
        // unlike most tutorials, i am using the actual Blob/file object instead of the data-url
        /*this.editUserForm.patchValue({
          [field]: file.text()
        });*/
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      }
    }
  }
}
