import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AccountDeleteComponent} from './account-delete/account-delete.component';
import {AccountEditComponent} from './account-edit/account-edit.component';
import {AccountAddComponent} from './account-add/account-add.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {SyncosyncCommonModule} from "../../syncosync-common/syncosync-common.module";


@NgModule({
  declarations: [
    AccountDeleteComponent,
    AccountEditComponent,
    AccountAddComponent
  ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        SyncosyncCommonModule
    ],
  exports: [
    AccountDeleteComponent,
    AccountEditComponent,
    AccountAddComponent
  ]
})
export class SyncosyncAuthenticatedModalsModule { }