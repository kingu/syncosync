import {Component, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {validateHostnameIp} from '../../../syncosync-common/validators/FormValidators';
import {Hostname} from '../../../syncosync-common/model/hostname';
import {NetworkConfigService} from '../../services/network-config.service';
import {takeUntil} from 'rxjs/operators';
import {AbstractFormComponent} from '../../../syncosync-common/components/abstract-form-component/abstract-form.component';

@Component({
  selector: 'app-hostname-form',
  templateUrl: './hostname-form.component.html',
  styleUrls: ['./hostname-form.component.css']
})
export class HostnameFormComponent extends AbstractFormComponent<Hostname> implements OnInit {
  constructor(public networkConfigService: NetworkConfigService
  ) {
    super();
  }
  ngOnInit() {
    super.ngOnInit();
    this.networkConfigService.getHostname()
        .subscribe(hostname => {
          if (hostname !== undefined) {
            this.model = new Hostname();
            this.handleHostname(hostname);
            this.createFormGroup();
          }
        });
  }

  protected createFormGroup() {
    this.formGroup = this.formBuilder.group({
      hostname: this.formBuilder.group({
        hostname: [this.model.hostname, Validators.compose([validateHostnameIp, Validators.required])]
      }),
    });

    this.formGroup.get('hostname.hostname').valueChanges
        .pipe(takeUntil(this.unsubscribe))
        .subscribe(value => this.model.hostname = value);
  }

  onSubmit() {
    // TODO: work with result...
    this.networkConfigService.setHostname(this.model)
        .subscribe(hostname => {
          console.log('Received: ', hostname);
          if (hostname !== undefined) {
            this.handleHostname(hostname);
            this.createFormGroup();
          } else {
            alert('Failed setting hostname');
          }
        });
  }

  private handleHostname(hostname: Hostname) {
    this.model.hostname = hostname.hostname;
  }
}
