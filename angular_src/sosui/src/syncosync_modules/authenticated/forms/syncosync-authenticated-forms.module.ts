import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HostnameFormComponent} from './hostname-form/hostname-form.component';
import {NameserverFormComponent} from './nameserver-form/nameserver-form.component';
import {NetworkInterfaceFormComponent} from './network-interface-form/network-interface-form.component';
import {SshPortFormComponent} from './ssh-port-form/ssh-port-form.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { GenerateSyncKeyComponent } from './generate-sync-key/generate-sync-key.component';
import { SyncSetupRemoteConfigComponent } from './sync-setup-remote-config/sync-setup-remote-config.component';


@NgModule({
  declarations: [
    HostnameFormComponent,
    NameserverFormComponent,
    NetworkInterfaceFormComponent,
    SshPortFormComponent,
    GenerateSyncKeyComponent,
    SyncSetupRemoteConfigComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    FormsModule
  ],
  exports: [
    HostnameFormComponent,
    NameserverFormComponent,
    NetworkInterfaceFormComponent,
    SshPortFormComponent,
    GenerateSyncKeyComponent,
    SyncSetupRemoteConfigComponent
  ]
})
export class SyncosyncAuthenticatedFormsModule { }
