import { Component, OnInit } from '@angular/core';
import {AbstractFormComponent} from '../../../syncosync-common/components/abstract-form-component/abstract-form.component';
import {SshConfig} from '../../../syncosync-common/model/sshConfig';
import {NetworkConfigService} from '../../services/network-config.service';
import {Validators} from '@angular/forms';
import {validatePort} from '../../../syncosync-common/validators/FormValidators';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-ssh-port-form',
  templateUrl: './ssh-port-form.component.html',
  styleUrls: ['./ssh-port-form.component.css']
})
export class SshPortFormComponent extends AbstractFormComponent<SshConfig> implements OnInit {
  constructor(
      public networkConfigService: NetworkConfigService
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    // TODO: repeat until config was successfully received
    this.networkConfigService.getSshConfig()
        .subscribe(config => {
          console.log(config);
          if (config !== undefined) {
            this.setModel(config);
            this.createFormGroup();
          }
        });
  }

  protected createFormGroup(): void {
    this.formGroup = this.formBuilder.group({
      remote_listening_port: [this.model.remote_sync_port, Validators.compose([validatePort, Validators.required])]
    });
    this.formGroup.get('remote_listening_port').valueChanges
        .pipe(
            takeUntil(this.unsubscribe)
        )
        .subscribe(
            value => this.model.remote_sync_port = value
        );
  }

  public onSubmit(): void {
    // TODO: work with result...
    console.log('Current form: ', this.formGroup.value);
    console.log('Sending: ', this.model);
    this.networkConfigService.setSshConfig(this.model)
        .subscribe(config => {
          console.log('Received: ', config);
          if (config !== undefined) {
            this.setModel(config);
            this.createFormGroup();
          } else {
            alert('Failed setting nameservers');
          }
        });
  }

}
