import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SshPortFormComponent } from './ssh-port-form.component';

describe('SshPortFormComponent', () => {
  let component: SshPortFormComponent;
  let fixture: ComponentFixture<SshPortFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SshPortFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SshPortFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
