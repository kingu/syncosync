import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameserverFormComponent } from './nameserver-form.component';

describe('NameserverFormComponent', () => {
  let component: NameserverFormComponent;
  let fixture: ComponentFixture<NameserverFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NameserverFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameserverFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
