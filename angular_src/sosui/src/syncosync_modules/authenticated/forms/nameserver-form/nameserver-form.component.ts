import {Component, OnDestroy, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {optionalValidator, validateIPv4IPv6} from '../../../syncosync-common/validators/FormValidators';
import {NetworkConfigService} from '../../services/network-config.service';
import {ResolvConfig} from '../../../syncosync-common/model/resolvConfig';
import {takeUntil} from 'rxjs/operators';
import {AbstractFormComponent} from '../../../syncosync-common/components/abstract-form-component/abstract-form.component';

@Component({
    selector: 'app-nameserver-form',
    templateUrl: './nameserver-form.component.html',
    styleUrls: ['./nameserver-form.component.css']
})
export class NameserverFormComponent extends AbstractFormComponent<ResolvConfig> implements OnInit, OnDestroy {
    constructor(
        public networkConfigService: NetworkConfigService
    ) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
        // TODO: repeat until config was successfully received
        this.networkConfigService.getResolvConf()
            .subscribe(config => {
                if (config !== undefined) {
                    this.handleNameserverConfig(config);
                    this.createFormGroup();
                }
            });
    }

    protected createFormGroup() {
        this.formGroup = this.formBuilder.group({
            first_nameserver: [this.model.first_nameserver, Validators.compose([validateIPv4IPv6, Validators.required])],
            second_nameserver: [this.model.second_nameserver, optionalValidator([validateIPv4IPv6])],
            third_nameserver: [this.model.third_nameserver, optionalValidator([validateIPv4IPv6])],
        });
        this.formGroup.get('first_nameserver').valueChanges
            .pipe(
                takeUntil(this.unsubscribe)
            )
            .subscribe(
                value => this.model.first_nameserver = value
            );
        this.formGroup.get('second_nameserver').valueChanges
            .pipe(
                takeUntil(this.unsubscribe)
            )
            .subscribe(
                value => this.model.second_nameserver = value
            );
        this.formGroup.get('third_nameserver').valueChanges
            .pipe(
                takeUntil(this.unsubscribe)
            )
            .subscribe(
                value => this.model.third_nameserver = value
            );
    }

    private handleNameserverConfig(resolvConfig: ResolvConfig) {
        this.model = resolvConfig;
    }

    onSubmit() {
        // TODO: work with result...
        console.log('Current form: ', this.formGroup.value);
        console.log('Sending: ', this.model);
        this.networkConfigService.setResolvConf(this.model)
            .subscribe(config => {
                console.log('Received: ', config);
                if (config !== undefined) {
                    this.handleNameserverConfig(config);
                    this.createFormGroup();
                } else {
                    alert('Failed setting nameservers');
                }
            });
    }

}
