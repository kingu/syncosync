import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateSyncKeyComponent } from './generate-sync-key.component';

describe('GenerateSyncKeyComponent', () => {
  let component: GenerateSyncKeyComponent;
  let fixture: ComponentFixture<GenerateSyncKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateSyncKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateSyncKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
