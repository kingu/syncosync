import {Component, EventEmitter, OnInit} from '@angular/core';
import {AbstractFormComponent} from "../../../syncosync-common/components/abstract-form-component/abstract-form.component";
import {SosKey} from "../../../syncosync-common/model/sosKey";
import {SyncConfigurationService} from "../../services/sync-configuration.service";
import {SyncosyncModalService} from "../../../syncosync-common/services/syncosync-modal.service";
import {LocalizedMessage} from "../../../syncosync-common/model/localizedMessage";
import {ConfirmationModalResult} from "../../../syncosync-common/model/confirmationModalResult";
import {InfoLevel} from "../../../syncosync-common/model/infoLevel";

@Component({
  selector: 'app-generate-sync-key',
  templateUrl: './generate-sync-key.component.html',
  styleUrls: ['./generate-sync-key.component.css']
})
export class GenerateSyncKeyComponent extends AbstractFormComponent<SosKey> implements OnInit {

  constructor(
      public syncConfigurationService: SyncConfigurationService,
      private modalService: SyncosyncModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this.syncConfigurationService.getCurrentLocalKey()
        .subscribe(key => {
          if (key !== undefined) {
            console.log(key);
            this.setModel(key);
            this.createFormGroup();
          }
        });
  }

  private generateKey() {
    const resultOfKeyGenerationEmitter: EventEmitter<SosKey> = new EventEmitter<SosKey>();
    resultOfKeyGenerationEmitter.subscribe(result => {
      if (result !== undefined) {
        this.setModel(result);
      } else {
        this.modalService.showInfoModal(
            InfoLevel.ERROR,
            LocalizedMessage.GENERATE_NEW_KEY_FAILED);
      }
    })
    this.modalService.showProcessingModal<SosKey>(
        LocalizedMessage.GENERATING_NEW_KEY_PROCESSING_MODAL_TITLE,
        LocalizedMessage.GENERATING_NEW_KEY_PROCESSING_MODAL_CONTENT,
        this.syncConfigurationService.generateLocalKey(),
        null,
        resultOfKeyGenerationEmitter
    );
  }

  onSubmit() {
    this.modalService.showConfirmationModal(
        LocalizedMessage.GENERATE_NEW_KEY_CONFIRMATION_MODAL_TITLE,
        LocalizedMessage.GENERATE_NEW_KEY_CONFIRMATION_MODAL_CONTENT
    ).subscribe(userDecision => {
      if (userDecision == ConfirmationModalResult.CONFIRM) {
        this.generateKey();
      }
    });
  }

  protected createFormGroup(): void {
  }
}
