import {Component, OnInit} from '@angular/core';
import {AbstractFormComponent} from '../../../syncosync-common/components/abstract-form-component/abstract-form.component';
import {NicConfig} from '../../../syncosync-common/model/nicConfig';
import {Validators} from '@angular/forms';
import {validateIPv4, validateIPv6} from '../../../syncosync-common/validators/FormValidators';
import {takeUntil} from 'rxjs/operators';
import {NetworkConfigService} from '../../services/network-config.service';
import {faEthernet, faPowerOff} from '@fortawesome/free-solid-svg-icons';
import {NicMode} from '../../../syncosync-common/model/sosEnums';

@Component({
    selector: 'app-network-interface-form',
    templateUrl: './network-interface-form.component.html',
    styleUrls: ['./network-interface-form.component.css']
})
export class NetworkInterfaceFormComponent extends AbstractFormComponent<NicConfig[]> implements OnInit {
    // TODO: we have a list of nics in a single form!
    nicModes: typeof NicMode = NicMode;
    faPowerOff = faPowerOff;
    faEthernet = faEthernet;

    constructor(
        public networkConfigService: NetworkConfigService
    ) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
        this.networkConfigService.getNetworkInterfaces()
            .subscribe(nics => {
                if (nics !== undefined) {
                    console.log('Init received:');
                    console.log(nics);
                    this.setModel(nics);
                    this.createFormGroup();
                }
            });
    }

    protected createFormGroup() {
        // TODO: form group needs to also differentiate IPv4 and IPV6 for example...
        this.formGroup = this.formBuilder.group({});

        for (const nicConfig of this.model) {
            const nameOfNic = nicConfig.interface;
            this.formGroup.addControl(nameOfNic,
                this.formBuilder.group(
                    {
                        v4: this.formBuilder.group(
                            {
                                ip_mode: [nicConfig.ip4_mode, Validators.compose([Validators.required])],
                                ip_address: [nicConfig.ip4_address, Validators.compose([validateIPv4, Validators.required])],
                                netmask: [nicConfig.ip4_netmask, Validators.compose([validateIPv4, Validators.required])],
                                gateway: [nicConfig.ip4_gateway, Validators.compose([validateIPv4, Validators.required])],
                        }),
                        v6: this.formBuilder.group(
                            {
                                ip_mode: [nicConfig.ip6_mode, Validators.compose([Validators.required])],
                                ip_address: [nicConfig.ip6_address, Validators.compose([validateIPv6, Validators.required])],
                                netmask: [nicConfig.ip6_netmask, Validators.compose([validateIPv6, Validators.required])],
                                gateway: [nicConfig.ip6_gateway, Validators.compose([validateIPv6, Validators.required])],
                            })

                }));

            const v6ipMode = this.formGroup.get(nicConfig.interface + '.v6.ip_mode');
            const v4ipAddressControl = this.formGroup.get(nicConfig.interface + '.v4.ip_address');
            const v4netmaskControl = this.formGroup.get(nicConfig.interface + '.v4.netmask');
            const v4gatewayControl = this.formGroup.get(nicConfig.interface + '.v4.gateway');
            const v6ipAddressControl = this.formGroup.get(nicConfig.interface + '.v6.ip_address');
            const v6netmaskControl = this.formGroup.get(nicConfig.interface + '.v6.netmask');
            const v6gatewayControl = this.formGroup.get(nicConfig.interface + '.v6.gateway');

            this.updateControls(nicConfig);
            v4ipAddressControl.valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => nicConfig.ip4_address = value);
            v4netmaskControl.valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => nicConfig.ip4_netmask = value);
            v4gatewayControl.valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => nicConfig.ip4_gateway = value);

            v6ipMode.valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => {
                    nicConfig.ip6_mode = parseInt(value, 10);
                    this.updateControls(nicConfig);
                });
            v6ipAddressControl.valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => nicConfig.ip6_address = value);
            v6netmaskControl.valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => nicConfig.ip6_netmask = value);
            v6gatewayControl.valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => nicConfig.ip6_gateway = value);
        }

    }

    onSubmit() {
        // TODO
        console.log(this.model);
        this.networkConfigService.setNetworkInterfaces(this.model)
            .subscribe(nicConfig => {
                console.log('Received: ', nicConfig);
                if (nicConfig !== undefined) {
                    this.setModel(nicConfig);
                    this.createFormGroup();
                } else {
                    alert('Failed setting network config');
                }
            });
    }

    getColor(nicMode: NicMode) {
        if (nicMode !== NicMode.MANUAL) {
            return 'on';
        } else {
            return 'off';
        }
    }

    private updateControls(nicConfig: NicConfig) {
        const v4ipAddressControl = this.formGroup.get(nicConfig.interface + '.v4.ip_address');
        const v4netmaskControl = this.formGroup.get(nicConfig.interface + '.v4.netmask');
        const v4gatewayControl = this.formGroup.get(nicConfig.interface + '.v4.gateway');
        const v6ipAddressControl = this.formGroup.get(nicConfig.interface + '.v6.ip_address');
        const v6netmaskControl = this.formGroup.get(nicConfig.interface + '.v6.netmask');
        const v6gatewayControl = this.formGroup.get(nicConfig.interface + '.v6.gateway');

        if (nicConfig.ip4_mode === NicMode.STATIC) {
            v4ipAddressControl.enable();
            v4netmaskControl.enable();
            v4gatewayControl.enable();
        } else {
            v4ipAddressControl.disable();
            v4netmaskControl.disable();
            v4gatewayControl.disable();
        }

        if (nicConfig.ip6_mode === NicMode.STATIC) {
            v6ipAddressControl.enable();
            v6netmaskControl.enable();
            v6gatewayControl.enable();
        } else {
            v6ipAddressControl.disable();
            v6netmaskControl.disable();
            v6gatewayControl.disable();
        }
    }

    toogleIpv4DhcpStatic(nicConfig: NicConfig) {
        console.log('Toggling v4 DHCP');
        if (nicConfig.ip4_mode === NicMode.DHCP) {
            nicConfig.ip4_mode = NicMode.STATIC;
        } else {
            nicConfig.ip4_mode = NicMode.DHCP;
        }

        this.updateControls(nicConfig);
    }

    toggleInterfaceModeIpv4(nicConfig: NicConfig) {
        console.log('Toggling');
        if (nicConfig.ip4_mode === NicMode.MANUAL) {
            nicConfig.ip4_mode = NicMode.DHCP;
        } else {
            nicConfig.ip4_mode = NicMode.MANUAL;
        }

        this.updateControls(nicConfig);
    }

    toggleInterfaceModeIpv6(nicConfig: NicConfig) {
        console.log('Toggling');
        if (nicConfig.ip6_mode === NicMode.MANUAL) {
            nicConfig.ip6_mode = NicMode.AUTO;
        } else {
            nicConfig.ip6_mode = NicMode.MANUAL;
        }

        this.updateControls(nicConfig);
    }
}
