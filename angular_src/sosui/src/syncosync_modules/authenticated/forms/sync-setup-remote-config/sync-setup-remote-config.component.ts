import {Component, EventEmitter, OnInit} from '@angular/core';
import {AbstractFormComponent} from "../../../syncosync-common/components/abstract-form-component/abstract-form.component";
import {RemoteHost} from "../../../syncosync-common/model/remoteHost";
import {SyncConfigurationService} from "../../services/sync-configuration.service";
import {LocalizedMessage} from "../../../syncosync-common/model/localizedMessage";
import {InfoLevel} from "../../../syncosync-common/model/infoLevel";
import {SyncosyncModalService} from "../../../syncosync-common/services/syncosync-modal.service";

@Component({
    selector: 'app-sync-setup-remote-config',
    templateUrl: './sync-setup-remote-config.component.html',
    styleUrls: ['./sync-setup-remote-config.component.css']
})
export class SyncSetupRemoteConfigComponent extends AbstractFormComponent<RemoteHost> implements OnInit {
    constructor(public syncConfigurationService: SyncConfigurationService,
                private modalService: SyncosyncModalService
    ) {
        super();
        this.model = new RemoteHost();
    }

    ngOnInit(): void {
        const resultEmitter : EventEmitter<RemoteHost> = new EventEmitter<RemoteHost>();
        resultEmitter.subscribe(remoteHostConfig => {
            if (remoteHostConfig !== undefined) {
                console.log(remoteHostConfig);
                this.setModel(remoteHostConfig);
                this.createFormGroup();
            }
        })
        this.modalService.showProcessingModal(
            LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
            LocalizedMessage.REMOTE_CONFIG_GET_PROCESSING_MODAL_CONTENT,
            this.syncConfigurationService.getCurrentRemoteHostConfig(),
            null,
            resultEmitter
        );
    }

    /**
     * Simply saves the config
     */
    onSubmit() {
        const resultEmitter : EventEmitter<RemoteHost> = new EventEmitter<RemoteHost>();
        resultEmitter.subscribe(result => {
            if (result !== undefined) {
                console.log(result);
                this.setModel(result);
                this.createFormGroup();
            } else {
                this.savingRemoteConfigFailedModal();
            }
        })
        this.modalService.showProcessingModal(
            LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
            LocalizedMessage.REMOTE_CONFIG_SAVE_PROCESSING_MODAL_CONTENT,
            this.syncConfigurationService.saveRemoteHostConfig(this.model),
            null,
            resultEmitter
        );
    }

    protected createFormGroup(): void {
    }

    private savingRemoteConfigFailedModal() {
        this.modalService.showInfoModal(
            InfoLevel.ERROR,
            LocalizedMessage.REMOTE_CONFIG_SAVE_FAILED_CONTENT
        );
    }



}
