import {Component, EventEmitter, OnInit} from '@angular/core';
import {AbstractFormComponent} from "../../../syncosync-common/components/abstract-form-component/abstract-form.component";
import {SosKey} from "../../../syncosync-common/model/sosKey";
import {LocalizedMessage} from "../../../syncosync-common/model/localizedMessage";
import {ConfirmationModalResult} from "../../../syncosync-common/model/confirmationModalResult";
import {InfoLevel} from "../../../syncosync-common/model/infoLevel";
import {SyncConfigurationService} from "../../services/sync-configuration.service";
import {SyncosyncModalService} from "../../../syncosync-common/services/syncosync-modal.service";

@Component({
  selector: 'app-remote-key',
  templateUrl: './remote-key.component.html',
  styleUrls: ['./remote-key.component.css']
})
export class RemoteKeyComponent extends AbstractFormComponent<SosKey> implements OnInit {

  constructor(public syncConfigurationService: SyncConfigurationService,
              private modalService: SyncosyncModalService) {
    super();
  }

  ngOnInit(): void {
    const resultEmitter : EventEmitter<SosKey> = new EventEmitter<SosKey>();
    resultEmitter.subscribe(remoteKey => {
      if (remoteKey !== undefined) {
        console.log(remoteKey);
        this.setModel(remoteKey);
        this.createFormGroup();
      }
    })
    this.modalService.showProcessingModal<SosKey>(
        LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
        LocalizedMessage.REMOTE_KEY_GET_PROCESSING_MODAL_CONTENT,
        this.syncConfigurationService.getRemoteKey(),
        null,
        resultEmitter
    );
  }

  protected createFormGroup(): void {
  }

  /** Calls for the removal of the remote public key stored on the box in order to not sync with the partner
   * anymore **/
  onSubmit(): void {
    this.modalService.showConfirmationModal(
        LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_TITLE,
        LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_CONTENT
    ).subscribe(userDecision => {
      if (userDecision == ConfirmationModalResult.CONFIRM) {
        this.removeRemoteKey();
      } else {
        this.modalService.showInfoModal(
            InfoLevel.WARNING,
            LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_CANCELLED
        );
      }
    })
  }


  private removeRemoteKey() {
    const resultEmitter : EventEmitter<SosKey> = new EventEmitter<SosKey>();
    resultEmitter.subscribe(result => {
      if (result !== undefined) {
        alert("TODO: handle result, likely indicating successful deletion of remote key");
      } else {
        alert("TODO: handle result indicating failure to delete remote key");
      }
    })
    this.modalService.showProcessingModal<SosKey>(
        LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_TITLE,
        LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_PROCESSING_CONTENT,
        this.syncConfigurationService.removeRemoteKey(),
        null,
        resultEmitter
    );
  }
}
