import {Component, Input, OnInit} from '@angular/core';
import {AccountInfo} from '../../../syncosync-common/model/sosaccount';
import {Status} from '../../../syncosync-common/model/status';
import {AccountEditComponent} from '../../modals/account-edit/account-edit.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AccountDeleteComponent} from '../../modals/account-delete/account-delete.component';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-accountdetail-drop',
  templateUrl: './accountdetail-drop.component.html',
  styleUrls: ['./accountdetail-drop.component.css']
})
export class AccountdetailDropComponent implements OnInit {
  @Input() account: AccountInfo;
  @Input() latestStatus: Status;
  faPencil = faPencilAlt;
  faTrash = faTrash;

  showDetails = false;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  editModal($event: MouseEvent, account: AccountInfo) {
    $event.stopPropagation();
    const editModalRef = this.modalService.open(AccountEditComponent, { size: 'lg' });
    editModalRef.componentInstance.account = account;
  }

  deleteModal($event: MouseEvent, account: AccountInfo) {
    $event.stopPropagation();
    const deleteModalRef = this.modalService.open(AccountDeleteComponent);
    deleteModalRef.componentInstance.account = account;
  }

}
