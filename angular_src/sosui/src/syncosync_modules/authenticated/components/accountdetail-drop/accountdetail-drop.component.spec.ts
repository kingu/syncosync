import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountdetailDropComponent } from './Accountdetail-drop.component';

describe('AccountdetailDropComponent', () => {
  let component: AccountdetailDropComponent;
  let fixture: ComponentFixture<AccountdetailDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountdetailDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountdetailDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
