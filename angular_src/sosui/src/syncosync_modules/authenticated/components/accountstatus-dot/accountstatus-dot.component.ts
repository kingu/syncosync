import {Component, Input, OnInit} from '@angular/core';
import {AccountInfo} from '../../../syncosync-common/model/sosaccount';
import {Status} from '../../../syncosync-common/model/status';
import {faCircle} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-accountstatus-dot',
  templateUrl: './accountstatus-dot.component.html',
  styleUrls: ['./accountstatus-dot.component.css']
})
export class AccountstatusDotComponent implements OnInit {
  @Input() account: AccountInfo;
  @Input() status?: Status;
  faCircle = faCircle;
  private spaceUsedPercentage = 0;

  constructor() {

  }

  ngOnInit() {
    if (this.status !== undefined) {
      this.spaceUsedPercentage = Math.round((10000 * this.account.space_used)
        / (this.status.total_size * this.account.max_space_allowed)
      );
    }
  }

  getColor(): string {
    if (this.spaceUsedPercentage >= 99) {
      return 'error';
    } else if (this.account.overdue !== 0) {
      return 'error';
    } else if (this.spaceUsedPercentage >= 90) {
      return 'warning';
    } else {
      return 'ok';
    }
  }

  getTooltipMessage(): string {
    // console.log('TODO: add localization');
    if (this.spaceUsedPercentage >= 99) {
      return 'Space full';
    } else if (this.account.overdue !== 0) {
      return 'Update overdue for ' + 'TODO' + ' days and ' + 'TODO' + ' hours';
    } else if (this.spaceUsedPercentage >= 90) {
      return 'Space almost full';
    } else {
      return 'Everything ok';
    }
  }
}
