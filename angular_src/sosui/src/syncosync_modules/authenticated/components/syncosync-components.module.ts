import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountstatusDotComponent} from './accountstatus-dot/accountstatus-dot.component';
import {AccountdetailDropComponent} from './accountdetail-drop/accountdetail-drop.component';
import {AccountsDropListingComponent} from './accounts-drop-listing/accounts-drop-listing.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SyncosyncAuthenticatedGraphsModule} from '../graphs/syncosync-authenticated-graphs.module';
import {SyncosyncCommonModule} from "../../syncosync-common/syncosync-common.module";
import { KeyExchangeButtonsComponent } from './key-exchange-buttons/key-exchange-buttons.component';
import { RemoteKeyComponent } from './remote-key/remote-key.component';


@NgModule({
    declarations: [
        AccountstatusDotComponent,
        AccountdetailDropComponent,
        AccountsDropListingComponent,
        KeyExchangeButtonsComponent,
        RemoteKeyComponent,
    ],
    imports: [
        CommonModule,
        SyncosyncCommonModule,
        FontAwesomeModule,
        NgbModule,
        SyncosyncAuthenticatedGraphsModule,
    ],
    exports: [
        AccountstatusDotComponent,
        AccountdetailDropComponent,
        AccountsDropListingComponent,
        RemoteKeyComponent,
        KeyExchangeButtonsComponent,
    ]
})
export class SyncosyncComponentsModule {
}
