import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {AbstractSetupAssistantPartialComponent} from "../../../syncosync-common/components/abstract-setup-assistant-partial/abstract-setup-assistant-partial.component";
import {LocalizedMessage} from "../../../syncosync-common/model/localizedMessage";
import {SystemSetupData} from "../../../syncosync-common/model/systemSetupData";
import {SyncosyncModalService} from "../../../syncosync-common/services/syncosync-modal.service";
import {SyncConfigurationService} from "../../services/sync-configuration.service";
import {InfoLevel} from "../../../syncosync-common/model/infoLevel";
import {SshFingerprint} from "../../../syncosync-common/utils/sshFingerprint";
import {ConfirmationModalResult} from "../../../syncosync-common/model/confirmationModalResult";
import {GenericUiResponse, GenericUiResponseStatus} from "../../../syncosync-common/model/genericUiResponse";

@Component({
  selector: 'app-key-exchange-buttons',
  templateUrl: './key-exchange-buttons.component.html',
  styleUrls: ['./key-exchange-buttons.component.css']
})
export class KeyExchangeButtonsComponent extends AbstractSetupAssistantPartialComponent implements OnInit {
  @Input() disableButtons: boolean = true;

  constructor(
      public syncConfigurationService: SyncConfigurationService,
      private modalService: SyncosyncModalService
  ) {
    super();
  }

  ngOnInit(): void {
  }

  initiateLeadKeyExchange() {
    const resultEmitter : EventEmitter<SystemSetupData> = new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe(result => {
      if (result !== undefined) {
        this.acceptOppositeKeyDialog(result);
      } else {
        this.keyExchangeFailedModal();
      }
    })
    this.modalService.showProcessingModal<SystemSetupData>(
        LocalizedMessage.SYNC_SETUP_LEADING_KEY_EXCHANGE_PROCESSING_MODAL_TITLE,
        LocalizedMessage.SYNC_SETUP_LEADING_KEY_EXCHANGE_PROCESSING_MODAL_CONTENT,
        this.syncConfigurationService.leadKeyExchange(),
        this.syncConfigurationService.cancelKeyExchange(),
        resultEmitter
    );
  }

  private keyExchangeFailedModal() {
    this.modalService.showInfoModal(
        InfoLevel.ERROR,
        LocalizedMessage.KEY_EXCHANGE_FAILED_CONTENT
    );
  }

  private acceptOppositeKeyDialog(result: SystemSetupData) {
    this.modalService.showConfirmationModal(
        LocalizedMessage.KEY_EXCHANGE_ACCEPT_REMOTE_KEY_TITLE,
        LocalizedMessage.KEY_EXCHANGE_ACCEPT_REMOTE_KEY_CONFIRMATION_EXPLANATION,
        SshFingerprint.fingerprint(result.pub_key)
    ).subscribe(userDecision => {
      if (userDecision == ConfirmationModalResult.CONFIRM) {
        this.acceptOppositeKey();
      } else {
        this.modalService.showInfoModal(
            InfoLevel.WARNING,
            LocalizedMessage.KEY_EXCHANGE_FAILED_REMOTE_KEY_NOT_ACCEPTED_CONFIRMATION_CONTENT
        );
      }
    })
  }

  followKeyExchange() {
    const resultEmitter : EventEmitter<SystemSetupData> = new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe(result => {
      if (result !== undefined) {
        this.acceptOppositeKeyDialog(result);
      } else {
        this.keyExchangeFailedModal();
      }
    })
    this.modalService.showProcessingModal<SystemSetupData>(
        LocalizedMessage.SYNC_SETUP_FOLLOW_KEY_EXCHANGE_PROCESSING_MODAL_TITLE,
        LocalizedMessage.SYNC_SETUP_FOLLOW_KEY_EXCHANGE_PROCESSING_MODAL_CONTENT,
        this.syncConfigurationService.followKeyExchange(),
        this.syncConfigurationService.cancelKeyExchange(),
        resultEmitter
    );
  }

  private acceptOppositeKey() {
    const resultEmitter : EventEmitter<GenericUiResponse> = new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe(result => {
      if (result !== undefined && result.status == GenericUiResponseStatus.OK) {
        alert("TODO: handle result, likely indicating successful accepting of remote key");
        // TODO: We will need to propagate the result as an @Output...
      } else {
        this.showAcceptKeyFailed();
      }
    })
    this.modalService.showProcessingModal<GenericUiResponse>(
        LocalizedMessage.SYNC_SETUP_ACCEPT_REMOTE_KEY_PROCESSING_TITLE,
        LocalizedMessage.SYNC_SETUP_ACCEPT_REMOTE_KEY_PROCESSING_CONTENT,
        this.syncConfigurationService.acceptRemoteKey(),
        null,
        resultEmitter
    );
  }

  private showAcceptKeyFailed() {
    this.modalService.showInfoModal(
        InfoLevel.ERROR,
        LocalizedMessage.KEY_EXCHANGE_ACCEPT_REMOTE_KEY_FAILED_CONTENT
    );
  }
}
