import {Component, Input, OnInit} from '@angular/core';
import {Status} from '../../../syncosync-common/model/status';
import {AccountApiService} from '../../services/account-api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AccountAddComponent} from '../../modals/account-add/account-add.component';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { faLaptopMedical } from '@fortawesome/free-solid-svg-icons';
import {AbstractSubscriberComponent} from '../../../syncosync-common/components/abstract-subscriber-component/abstract-subscriber.component';

@Component({
  selector: 'app-accounts-drop-listing',
  templateUrl: './accounts-drop-listing.component.html',
  styleUrls: ['./accounts-drop-listing.component.css']
})
export class AccountsDropListingComponent extends AbstractSubscriberComponent implements OnInit {
  faUserPlus = faUserPlus;
  faLaptopMedical = faLaptopMedical;
  @Input() latestStatus: Status;

  constructor(public accountService: AccountApiService,
              private modalService: NgbModal) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  addAccount() {
    const addModalRef = this.modalService.open(AccountAddComponent, { size: 'lg' });
  }
}
