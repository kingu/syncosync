import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsDropListingComponent } from './accounts-drop-listing.component';

describe('AccountsDropListingComponent', () => {
  let component: AccountsDropListingComponent;
  let fixture: ComponentFixture<AccountsDropListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsDropListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsDropListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
