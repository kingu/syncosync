import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DiskUsageDonutComponent} from './disk-usage-donut/disk-usage-donut.component';
import {SyncStatusDotComponent} from '../../public/graphs/sync-status-dot/sync-status-dot.component';
import {ChartsModule} from 'ng2-charts';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    SyncStatusDotComponent,
    DiskUsageDonutComponent
  ],
  imports: [
    CommonModule,
    ChartsModule,
    FontAwesomeModule,
    NgbModule
  ],
  exports: [
    SyncStatusDotComponent,
    DiskUsageDonutComponent,
  ]
})
export class SyncosyncAuthenticatedGraphsModule { }
