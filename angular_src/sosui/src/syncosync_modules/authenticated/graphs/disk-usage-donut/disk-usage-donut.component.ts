import {Component, OnInit, ViewChild} from '@angular/core';
import {BaseChartDirective, Label} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {StatusService} from '../../../public/services/status.service';
import {takeUntil} from 'rxjs/operators';
import {AccountApiService} from '../../services/account-api.service';
import {Status} from '../../../syncosync-common/model/status';
import {Calculators} from '../../../syncosync-common/utils/calculators';
// tslint:disable-next-line:max-line-length
import {AbstractSubscriberComponent} from '../../../syncosync-common/components/abstract-subscriber-component/abstract-subscriber.component';
import {AccountList} from '../../../syncosync-common/model/sosaccount';

@Component({
  selector: 'app-disk-usage-donut',
  templateUrl: './disk-usage-donut.component.html',
  styleUrls: ['./disk-usage-donut.component.css']
})
export class DiskUsageDonutComponent extends AbstractSubscriberComponent implements OnInit {
  private static accountColours = ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(150, 120, 59, 0.2)'];

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  private latestTotalSize: number;

  public barChartOptions: ChartOptions = {
    scales: {
      xAxes: [{
        stacked: true,
        display: false,
        ticks: {
          beginAtZero: true,
          max: 100,
        }
      }],
      yAxes: [{
        stacked: true
      }]
    },
    responsive: true,
  };
  public barChartLabels: Label[] = ['0 GB'];
  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = false;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65], barPercentage: 1, categoryPercentage: 1, label: 'Free', stack: 'a', backgroundColor: '#50A050',
      hoverBackgroundColor: '#50A050' },
  ];

  constructor(private statusService: StatusService,
              private accountApiService: AccountApiService,
  ) {
    super();
  }

  ngOnInit() {
    console.log('OnInit');
    this.statusService.latestStatusObservable$
        .pipe(
            takeUntil(this.unsubscribe)
        )
        .subscribe(status => {
          this.handleStatus(status);
        });
    this.accountApiService.latestAccountlistObservable$
        .pipe(
            takeUntil(this.unsubscribe)
        )
        .subscribe(accounts => {
          this.handleAccounts(accounts);
        });
    if (this.chart !== undefined) {
      console.log('Updating chart');
      this.chart.update(0);
    }
  }

  private handleStatus(status: Status) {
    if (status === undefined) {
      return;
    }

    this.latestTotalSize = status.total_size;

    const totalSizeGb = Calculators.bytesToGb(this.latestTotalSize);
    this.barChartOptions.scales.xAxes[0].ticks.max = totalSizeGb;
    this.barChartLabels[0] = totalSizeGb + ' GB';
    if (this.chart !== undefined) {
      console.log('Updating chart');
      this.chart.update(0);
    }
  }

  private handleAccounts(accountList: AccountList) {
    if (accountList === undefined) {
      return;
    }
    this.barChartData.length = 1;
    let freeSpace = this.latestTotalSize;
    let i = 0;
    for (const account of accountList.getAccounts()) {
      const spaceUsed = account.space_used;
      freeSpace -= spaceUsed;
      const colourToUse = DiskUsageDonutComponent.accountColours[i % DiskUsageDonutComponent.accountColours.length];
      this.barChartData.push({ data: [Calculators.bytesToGb(spaceUsed)],
        barPercentage: 1, categoryPercentage: 1, label: account.name, stack: 'a',
        backgroundColor: colourToUse,
        hoverBackgroundColor: colourToUse});
      i++;
    }
    this.barChartData[0].data[0] = Calculators.bytesToGb(freeSpace);

    if (this.chart !== undefined) {
      console.log('Updating chart');
      this.chart.update(0);
    }
  }
}
