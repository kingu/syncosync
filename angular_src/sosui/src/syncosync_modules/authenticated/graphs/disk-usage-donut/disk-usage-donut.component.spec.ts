import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiskUsageDonutComponent } from './disk-usage-donut.component';

describe('DiskUsageDonutComponent', () => {
  let component: DiskUsageDonutComponent;
  let fixture: ComponentFixture<DiskUsageDonutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiskUsageDonutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiskUsageDonutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
