import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {SyncosyncAuthenticatedModalsModule} from "./modals/syncosync-authenticated-modals.module";



@NgModule({
  declarations: [
  ],
  imports: [
    FormsModule,
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [
    SyncosyncAuthenticatedModalsModule
  ]
})
export class SyncosyncAuthenticatedModule { }
