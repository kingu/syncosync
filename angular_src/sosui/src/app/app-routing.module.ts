import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './helpers/guards/auth.guard';

const syncosyncModule = () => import('./pages/syncosync/syncosync.module').then(x => x.SyncosyncModule);
const authModule = () => import('./pages/auth/auth.module').then(x => x.AuthModule);

const routes: Routes = [
  { path: 'auth', loadChildren: authModule },
  { path: '', loadChildren: syncosyncModule, canActivate: [AuthGuard] },

  // otherwise redirect to home
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
