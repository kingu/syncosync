import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from '../../../syncosync_modules/syncosync-common/services/auth.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class RestErrorInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        console.log("Logging out");
        this.authService.logout();
      }
      if (err.error === undefined || err.error.message == undefined) {
        return throwError(err.statusText);
      }
      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
