import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../../syncosync_modules/syncosync-common/services/auth.service";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-disaster-swap-login',
  templateUrl: './disaster-swap-login.component.html',
  styleUrls: ['./disaster-swap-login.component.css']
})
export class DisasterSwapLoginComponent implements OnInit {

  form: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    // this.alertService.clear();

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    alert('No functionality yet, sorry');
    /*this.authService.login(this.f.password.value)
        .pipe(first())
        .subscribe(
            data => {
              this.router.navigate([this.returnUrl]);
            },
            error => {
              // this.alertService.error(error);
              this.loading = false;
            });*/
  }

}
