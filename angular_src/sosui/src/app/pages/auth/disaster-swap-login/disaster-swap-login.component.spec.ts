import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisasterSwapLoginComponent } from './disaster-swap-login.component';

describe('DisasterSwapLoginComponent', () => {
  let component: DisasterSwapLoginComponent;
  let fixture: ComponentFixture<DisasterSwapLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisasterSwapLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisasterSwapLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
