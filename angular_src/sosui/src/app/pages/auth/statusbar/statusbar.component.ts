import { Component, OnInit } from '@angular/core';
import {NetworkStatusService} from "../../../../syncosync_modules/public/services/network-status.service";
import {Hostname} from "../../../../syncosync_modules/syncosync-common/model/hostname";

@Component({
  selector: 'app-statusbar',
  templateUrl: './statusbar.component.html',
  styleUrls: ['./statusbar.component.css']
})
export class StatusbarComponent implements OnInit {

  public hostname: Hostname = new Hostname();
  constructor(public networkStatusService: NetworkStatusService) { }

  ngOnInit(): void {
    this.networkStatusService.getHostname()
        .subscribe(hostname =>  {
          if (hostname !== undefined) {
            this.hostname = hostname
          }
        })
  }

}
