import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../../syncosync_modules/syncosync-common/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  showSidebar = false;
  screenHeight = 0;
  screenWidth = 0;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.evaluateScreenSize();
  }

  constructor(
      private router: Router,
      private authService: AuthService
  ) {
    // redirect to home if already logged in
    if (this.authService.userValue) {
      console.log('Already logged in');
      // TODO: Redirect to ESA / disaster recovery helper / whatever assistant if required to here or after successful auth
      this.router.navigate(['/syncosync/sosui']);
    }
    this.onResize();
  }

  ngOnInit() {
  }

  private evaluateScreenSize() {
    this.showSidebar = this.screenWidth >= 720;
  }

  toggleClass(wrapper: HTMLDivElement, classToToggle: string) {
    const hasClass = wrapper.classList.contains(classToToggle);
    this.showSidebar = !this.showSidebar;

    if (hasClass) {
      wrapper.classList.remove(classToToggle);
    } else {
      wrapper.classList.add(classToToggle);
    }
  }
}
