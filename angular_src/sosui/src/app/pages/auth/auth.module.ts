import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthRoutes} from './auth.routes';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import {AuthComponent} from './auth.component';
import { StatusbarComponent } from './statusbar/statusbar.component';
import {ReactiveFormsModule} from "@angular/forms";
import { DisasterSwapLoginComponent } from './disaster-swap-login/disaster-swap-login.component';

@NgModule({
    declarations: [
        AuthComponent,
        LoginComponent,
        StatusbarComponent,
        DisasterSwapLoginComponent,
    ],
    exports: [
    ],
    imports: [
        CommonModule,
        AuthRoutes,
        ReactiveFormsModule,
        NgbModule,
    ],
    providers: [
        NgbActiveModal,
    ],
    entryComponents: [
        AuthComponent
    ]
})
export class AuthModule {
}
