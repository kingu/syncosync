import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SyncosyncRoutes} from './syncosync.routes';
import {StatusComponent} from './pages/status/status.component';
import {SyncosyncComponent} from './syncosync.component';
import {NavbarComponent} from './navbar/navbar.component';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ChartsModule} from 'ng2-charts';
import {AccountDeleteComponent} from '../../../syncosync_modules/authenticated/modals/account-delete/account-delete.component';
import {AccountEditComponent} from '../../../syncosync_modules/authenticated/modals/account-edit/account-edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccountAddComponent} from '../../../syncosync_modules/authenticated/modals/account-add/account-add.component';
import {StatusbarComponent} from './statusbar/statusbar.component';
import {NetworkSetupComponent} from './pages/network-setup/network-setup.component';
import {SyncosyncCommonModule} from '../../../syncosync_modules/syncosync-common/syncosync-common.module';
import {SyncosyncAuthenticatedModalsModule} from '../../../syncosync_modules/authenticated/modals/syncosync-authenticated-modals.module';
import {SyncosyncComponentsModule} from '../../../syncosync_modules/authenticated/components/syncosync-components.module';
import {SyncosyncAuthenticatedFormsModule} from '../../../syncosync_modules/authenticated/forms/syncosync-authenticated-forms.module';
import {SyncosyncAuthenticatedGraphsModule} from '../../../syncosync_modules/authenticated/graphs/syncosync-authenticated-graphs.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {RestErrorInterceptor} from '../../helpers/interceptors/rest-error-interceptor.service';
import {SyncosyncPublicGraphsModule} from "../../../syncosync_modules/public/graphs/syncosync-public-graphs.module";
import {SyncosyncPublicModule} from "../../../syncosync_modules/public/syncosync-public.module";
import { SyncSetupComponent } from './pages/sync-setup/sync-setup.component';

@NgModule({
    declarations: [
        StatusComponent,
        SyncosyncComponent,
        NetworkSetupComponent,
        StatusbarComponent,
        NavbarComponent,
        SyncSetupComponent,
    ],
    exports: [
        SyncosyncComponent,
    ],
    imports: [
        CommonModule,
        SyncosyncRoutes,
        NgbModule,
        FontAwesomeModule,
        ChartsModule,
        ReactiveFormsModule,
        FormsModule,
        SyncosyncCommonModule,
        SyncosyncAuthenticatedModalsModule,
        SyncosyncComponentsModule,
        SyncosyncAuthenticatedFormsModule,
        SyncosyncAuthenticatedGraphsModule,
        SyncosyncPublicGraphsModule,
        SyncosyncPublicModule
    ],
    providers: [
        NgbActiveModal,
        {provide: HTTP_INTERCEPTORS, useClass: RestErrorInterceptor, multi: true},
    ],
    entryComponents: [
        AccountDeleteComponent,
        AccountEditComponent,
        AccountAddComponent
    ]
})
export class SyncosyncModule {
}
