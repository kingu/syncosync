import { NgModule } from '@angular/core';
import {StatusComponent} from './pages/status/status.component';
import {RouterModule, Routes} from '@angular/router';
import {NetworkSetupComponent} from './pages/network-setup/network-setup.component';
import {SyncosyncComponent} from './syncosync.component';
import {SyncSetupComponent} from "./pages/sync-setup/sync-setup.component";

const routes: Routes = [
  {
    path: '',
    component: SyncosyncComponent,
    children: [
      { path: '', component: StatusComponent },
      { path: 'syncosync/status', component: StatusComponent },
      { path: 'syncosync/networksetup', component: NetworkSetupComponent },
      { path: 'syncosync/syncsetup', component: SyncSetupComponent },
    ]
  },
   { path: '**', redirectTo: 'syncosync/status' }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SyncosyncRoutes { }
