import {Component, HostListener, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-syncosync',
  templateUrl: './syncosync.component.html',
  styleUrls: ['./syncosync.component.css'],
})
export class SyncosyncComponent implements OnInit {
  showSidebar = false;
  screenHeight = 0;
  screenWidth = 0;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.evaluateScreenSize();
  }

  constructor() {
    this.onResize();
  }

  ngOnInit() {
  }

  private evaluateScreenSize() {
    this.showSidebar = this.screenWidth >= 720;
  }

  toggleClass(wrapper: HTMLDivElement, classToToggle: string) {
    const hasClass = wrapper.classList.contains(classToToggle);
    this.showSidebar = !this.showSidebar;

    if (hasClass) {
      wrapper.classList.remove(classToToggle);
    } else {
      wrapper.classList.add(classToToggle);
    }
  }
}
