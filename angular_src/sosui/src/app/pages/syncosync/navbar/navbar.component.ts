import {Component, OnInit} from '@angular/core';
import {AbstractSubscriberComponent} from "../../../../syncosync_modules/syncosync-common/components/abstract-subscriber-component/abstract-subscriber.component";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent extends AbstractSubscriberComponent implements OnInit {


  constructor() {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
  }

}
