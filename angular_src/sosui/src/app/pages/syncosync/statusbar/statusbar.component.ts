import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SizeProp} from '@fortawesome/fontawesome-svg-core';
import {AuthService} from "../../../../syncosync_modules/syncosync-common/services/auth.service";

@Component({
  selector: 'app-statusbar',
  templateUrl: './statusbar.component.html',
  styleUrls: ['./statusbar.component.css']
})
export class StatusbarComponent implements OnInit {
  @Output()
  toggleNavbar = new EventEmitter<void>();
  sizeProp: SizeProp = '2x';
  constructor(private authService: AuthService) { }

  ngOnInit() {
    // this.showNavbar.emit(true);
  }

    logout() {
      this.authService.logout();
    }
}
