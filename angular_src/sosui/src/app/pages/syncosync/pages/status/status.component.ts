import {Component, OnInit} from '@angular/core';
import {StatusService} from '../../../../../syncosync_modules/public/services/status.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {

  constructor(public statusService: StatusService) { }

  ngOnInit(): void {
  }
}
