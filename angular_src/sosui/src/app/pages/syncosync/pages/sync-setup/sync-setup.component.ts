import { Component, OnInit } from '@angular/core';
import {SosKey} from "../../../../../syncosync_modules/syncosync-common/model/sosKey";
import {RemoteHost} from "../../../../../syncosync_modules/syncosync-common/model/remoteHost";
import {AbstractSetupAssistantPartialComponent} from "../../../../../syncosync_modules/syncosync-common/components/abstract-setup-assistant-partial/abstract-setup-assistant-partial.component";

@Component({
  selector: 'app-sync-setup',
  templateUrl: './sync-setup.component.html',
  styleUrls: ['./sync-setup.component.css']
})
export class SyncSetupComponent extends AbstractSetupAssistantPartialComponent implements OnInit {
  displayRemoteSettings: boolean = false;
  displayKeyExchangeButtons: boolean = false;

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

  enableRemoteSettings($event: SosKey) {
    /**
     * Enables the inputs of the remote partner settings as well as syncing up.
     */
    if ($event !== undefined && $event !== null) {
      // TODO: Toggle stuff for remote settings
      this.displayRemoteSettings = true;
    }
  }

  enableKeyExchangeButtons(remoteHost: RemoteHost) {
    if (remoteHost !== undefined && remoteHost !== null) {
      //
    }
  }

  triggerStepFinished() {

  }
}
