import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SyncosyncModule } from './pages/syncosync/syncosync.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {SyncosyncCommonModule} from '../syncosync_modules/syncosync-common/syncosync-common.module';
import {JwtInterceptor} from './helpers/interceptors/jwt.interceptor';
import {RestErrorInterceptor} from "./helpers/interceptors/rest-error-interceptor.service";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SyncosyncModule,
    HttpClientModule,
    FontAwesomeModule,
    SyncosyncCommonModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
// and returns simulated server responses.
// Remove it when a real server is ready to receive requests.
    /*HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),*/
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RestErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
