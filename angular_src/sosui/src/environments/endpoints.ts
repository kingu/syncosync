
export const syncosyncEndpoints = {
    public: {
        networkHostnameUrl: '/api/public/hostname',
        networkBandwidthHistoryUrl: '/api/public/bandwidth_history',
        statusApiUrl: '/api/public/status/sync_status',
        uiLanguageUrl: '/api/public/ui_language',
    },
    authenticated: {
        hostnameUrl: '/api/setup/hostname',
        resolvConfigUrl: '/api/setup/resolv_config',
        networkInterfacesConfigUrl: '/api/setup/nic_config',
        sshConfigUrl: '/api/setup/ssh_config',
        accountUpdateUrl: '/api/account_management/edit_account',
        accountAddUrl: '/api/account_management/add_account',
        accountDeleteUrl: '/api/account_management/delete_account',
        accountListUrl: '/api/account_management/account_list',
        syncConfigurationLocalKey: "/api/setup/local_key",
        syncConfigurationRemoteHostConfig: "/api/setup/remote_host",
        keyExchangeCancel: "/api/setup/key_exchange_cancel",
        keyExchangeLead: "/api/setup/key_exchange_lead",
        keyExchangeFollow: "/api/setup/key_exchange_follow",
        remoteKey: "/api/setup/remote_key"


    },
    authLoginUrl: '/api/auth/login',
    resource: {
        localizedMessagesPartialUrl: (iso_code: string) => `/files/localization/${iso_code}.json`
    }
};
