#!/bin/bash
echo Cloning syncosync from gitlab
git clone https://gitlab.com/syncosync/syncosync.git
echo Starting build stage
cd syncosync
docker run --volume=$PWD:/syncosync sos-builder:latest /syncosync/build_deb.sh
if [ ! -f -f syncosync*.deb ]
then
    echo No deb file generated. Aborting.
    exit 1
fi
docker run --device=/dev/kvm:/dev/kvm --volume=$PWD:/syncosync sos-qemu-runner /syncosync/amd_installer_emulation/full_test.sh
