This is the dockerfile for running the qemus for creating and testing sos buster images.

to use them locally issue:

docker build -t sos-qemu-runner .

to use them for the CI/CD on gitlab:

docker build -t registry.gitlab.com/syncosync/syncosync/sos-qemu-runner .
docker push registry.gitlab.com/syncosync/syncosync/sos-qemu-runner
