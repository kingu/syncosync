#!/usr/bin/env python3
"""
All about starting and managing sos (and other) emulators
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import threading
import subprocess
import socket
import time

import paramiko

# note qcow has to be emitted from hda as this is also used by the hostname
exmample_start = '{' \
                 '"startcmd": "qemu-system-x86_64 -cpu host -accel kvm",' \
                 '"hda":"alice",' \
                 '"hdb":"empty.img",' \
                 '}'


class SerializerBase(object):
    """
    base class for getting and setting json from objects
    """

    @classmethod
    def from_json(cls, json_string):
        return cls(**json.loads(json_string))

    def to_json(self):
        return json.dumps(self.__dict__, default=lambda o: o.__dict__)


class QemuConfig(threading.Thread):
    # this is the
    base_address = 15
    base_ip = "10.0.2."  # this will be expanded in the init routine
    base_mac = 0x525498765432
    base_ssh = 2222
    base_remote = 55055
    base_flask = 8888

    def __init__(self, startcmd="qemu-system-x86_64 -cpu host -accel kvm", hda="", hdb="", hdc="", hdd="", num_if=1):
        self.startcmd = startcmd
        self.hda = hda
        self.hdb = hdb
        self.hdc = hdc
        self.hdd = hdd
        self.myprocess = None
        self.num_if = num_if
        self.ip = self.base_ip + str(QemuConfig.base_address)
        QemuConfig.base_address += 1
        if QemuConfig.base_address > 255:
            raise OverflowError("Too many ips requested")
        self.mac = []
        self.ssh = QemuConfig.base_ssh
        QemuConfig.base_ssh += 1
        self.remote = QemuConfig.base_remote
        QemuConfig.base_remote += 1
        self.flask = QemuConfig.base_flask
        QemuConfig.base_flask += 1
        for i in range(num_if):
            hex_num = hex(QemuConfig.base_mac)[2:].zfill(12)
            macstr = "{}{}:{}{}:{}{}:{}{}:{}{}:{}{}".format(*hex_num)
            self.mac.append(macstr)
            QemuConfig.base_mac += 1
        self.stdout = None
        self.stderr = None
        threading.Thread.__init__(self)

    def create_command(self):
        """
         creates the command to start the vm
        :return: command
        """
        command = self.startcmd
        if self.hda is None or self.hda == "":
            raise BaseException("no root disk defined")
        else:
            command = command + f" -hda {self.hda}.qcow"
        if self.hdb is not None and self.hdb != "":
            command = command + f" -hdb {self.hdb}"
        if self.hdc is not None and self.hdc != "":
            command = command + f" -hdc {self.hdc}"
        if self.hdd is not None and self.hdd != "":
            command = command + f" -hdd {self.hdd}"
        command += f" -netdev user,id=net0,hostname={self.hda},domainname=localdomain,net=10.0.2.0/24," \
                   f"dhcpstart={self.ip},hostfwd=tcp::{str(self.ssh)}-:22," \
                   f"hostfwd=tcp::{str(self.remote)}-:55055,hostfwd=tcp::{str(self.flask)}-:5000"
        for i in range(self.num_if - 1):
            command += f" -netdev user,id=net{str(i + 1)}"
        for i in range(self.num_if):
            command += f" -device rtl8139,netdev=net{str(i)},mac={self.mac[i]}"
        command += f" -boot once=n -m 2048 -nographic"
        return command

    def run(self):
        command = self.create_command()
        print(f"Starting: {command}")
        p = subprocess.Popen(command.split(),
                             shell=False,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        self.stdout, self.stderr = p.communicate()

    def wait_for_ssh(self):
        """
        waits for ssh to come up
        Returns true if up
        false if not up
        """
        client = None
        while True:
            time.sleep(1)
            if client is not None:
                client.close()
            time.sleep(1)
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.WarningPolicy())
            print(f"waiting for system {self.hda} to come up on ssh")
            time.sleep(1)
            try:
                client.connect('localhost', self.ssh, username="root", timeout=1, key_filename="robotkey")
            except paramiko.SSHException as e:
                if str(e) == "No existing session":
                    continue
                else:
                    print(f"ssh error: {e}: {str(e)}")
                    continue
            except OSError as e:
                print("was here:", e)
            client.close()
            return True

    def exec(self, command):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.WarningPolicy())
        client.connect(hostname="localhost", port=self.ssh, username="root", key_filename="robotkey")
        stdin, stdout, stderr = client.exec_command(command)
        stdoutarr = stdout.readlines()
        stderrarr = stderr.readlines()
        client.close()
        return stdoutarr, stderrarr


if "__main__" == __name__:
    myfirstqemu = QemuConfig(hda="alice", hdb="alice.img", num_if=1)
    mysecondqemu = QemuConfig(hda="bob", hdb="bob.img", num_if=1)

    print("1st:", myfirstqemu.create_command())
    print("2nd:", mysecondqemu.create_command())

    myfirstqemu.start()
    # ok, the qemu is started now wait for ssh to come up
    myfirstqemu.wait_for_ssh()
    print(f"ok, {myfirstqemu.hda} is connected")
    stdoutarr, stderrarr = myfirstqemu.exec('ls -la')
    print(stdoutarr)
    stdoutarr, stderrarr = myfirstqemu.exec('poweroff')
    myfirstqemu.join()
    print(myfirstqemu.stdout)
