#!/bin/bash

# for this magic, see: https://unix.stackexchange.com/questions/281589/how-to-run-mkfs-on-file-image-partitions-without-mounting
diskimg=diskimg                                          # filename of resulting disk image
size=$((500 * (1 << 20)))                                # desired size in bytes, 500MB in this case
alignment=1048576                                        # align to next MB (https://www.thomas-krenn.com/en/wiki/Partition_Alignment)
size=$(((size + alignment - 1) / alignment * alignment)) # ceil(size, 1MB)

echo Generate test images for drivesetup with a size of 500MB each

############################################
# empty-alice.img and empty-bob.img
# this is really an empty image
############################################

echo -n create emtpy disks for alice and bob: alice.img, bob.img.
rm alice.img bob.img
truncate -s $size alice.img
truncate -s $size bob.img
echo Ready
############################################
# linux.img
# this is just a standard hdd with a linux partition on it
############################################
echo -n create linux disk: linux.img.
diskimg=linux.img
rm "${diskimg}"
rm "${diskimg}".ext3
# mkfs.ext3 requires size as an (undefined) block-count; seem to be units of 1k
mkfs.ext3 -b 1024 -L "linuxext3" "${diskimg}".ext3 $((size >> 10))
# insert the filesystem to a new file at offset 1MB
dd if="${diskimg}".ext3 of="${diskimg}" conv=sparse obs=512 seek=$((alignment / 512))
# extend the file by 1MB
truncate -s "+${alignment}" "${diskimg}"
echo apply partitioning
parted --align optimal "${diskimg}" mklabel gpt mkpart linux "${alignment}B" '100%' set 1 boot on
rm "${diskimg}".ext3
echo Ready
############################################
# dos.img
# this is just a standard hdd with a dos partition on it
############################################
echo -n create dos disk: dos.img.
diskimg=dos.img
rm "${diskimg}"
rm "${diskimg}".fat
# mkfs.fat requires size as an (undefined) block-count; seem to be units of 1k
mkfs.fat -C -F32 -n "DOS" "${diskimg}".fat $((size >> 10))

# insert the filesystem to a new file at offset 1MB
dd if="${diskimg}".fat of="${diskimg}" conv=sparse obs=512 seek=$((alignment / 512))
# extend the file by 1MB
truncate -s "+${alignment}" "${diskimg}"
parted --align optimal "${diskimg}" mklabel gpt mkpart fat "${alignment}B" '100%' set 1 boot on
rm "${diskimg}".fat

rm -r ali_ta1.data
if [ ! -d ali_ta1.data ]; then
  echo -n create testdata for ali_ta1
  mkdir ali_ta1.data
  for i in $(seq 0 6); do
    echo $i
    dd if=/dev/zero of=ali_ta1.data/ali_ta1_$i.data bs=10M count=1
  done
  echo .
fi

rm -r bob_ta1.data
if [ ! -d bob_ta1.data ]; then
  echo -n create testdata for bob_ta1
  mkdir bob_ta1.data
  for i in $(seq 0 6); do
    echo $i
    dd if=/dev/zero of=bob_ta1.data/bob_ta1_$i.data bs=10M count=1
  done
  echo .
fi

if [ ! -f ali_ta1.pub ]; then
  echo "no key for ali_ta1 existing, creating one"
  ssh-keygen -N '' -b 2048 -t rsa -m pem -f "ali_ta1" -C "Alice Test Account 1"
fi

if [ ! -f bob_ta1.pub ]; then
  echo "no key for ali_ta1 existing, creating one"
  ssh-keygen -N '' -b 2048 -t rsa -m pem -f "bob_ta1" -C "Bob Test Account 1"
fi

echo Ready
