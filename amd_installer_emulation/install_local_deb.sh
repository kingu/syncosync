#!/bin/bash -e

# copy the local syncosync*.deb to the machines
# (c) 2019,2020 Stephan Skrodzki <stevieh@syncosync.org>

mydeb=$(ls -t -1 ../syncosync*.deb | rev | cut -d'/' -f-1 | rev)
scp  -o StrictHostKeyChecking=no -i robotkey -P 2222 ../$mydeb root@localhost: || exit 1

ssh  -o StrictHostKeyChecking=no -i robotkey -p 2222 root@localhost "yes | apt install -y --allow-downgrades ./$mydeb" || exit 1

scp  -o StrictHostKeyChecking=no -i robotkey -P 2223 ../$mydeb root@localhost: || exit 1

ssh  -o StrictHostKeyChecking=no -i robotkey -p 2223 root@localhost "yes | apt install -y --allow-downgrades ./$mydeb" || exit 1
