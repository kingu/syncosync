#!/bin/bash -e
# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=$(dirname $SCRIPT)
echo $SCRIPTPATH
cd $SCRIPTPATH
./init_img.sh
./gen_testdrives.sh
./start_img.sh alice bob
date
echo "Waiting for image to come up"
if [ -c /dev/kvm ]
then
    echo Sleeping for 30s
    sleep 30
else
    echo We have no /dev/kvm, so we wait for 5 min
    sleep 300
fi
date
./install_local_deb.sh
echo "Waiting for debian package to be installed"
if [ -c /dev/kvm ]
then
    echo Sleeping for 30s
    sleep 30
else
    echo We have no /dev/kvm, so we wait for 5 min
    sleep 300
fi
if [ ! -z "$DISPLAY" ]
then
    robot --xunit xunit.xml syncosync.robot
else
    # only with no DISPLAY we should also check if we are on a slow machine with no kvm
    if [ -c /dev/kvm ]
    then
        robot --xunit xunit.xml --variable HEADLESS:True syncosync.robot
    else
        robot --xunit xunit.xml --variable HEADLESS:True --variable SLOW:True syncosync.robot
    fi  
fi    
./stop_img.sh
