#!/bin/bash -e

# This scripts sets up a x86 based sos appliance
# it downloads - if not available - a netinst.iso from debian
# it uses a preesed.cfg file
# after successful execution, there will be a file sos_x86.qcow
# (c) 2019 Stephan Skrodzki <stevieh@syncosync.org>

# according to https://wiki.debian.org/DebianInstaller/Preseed/EditIso


if [ -c /dev/kvm ]
then
    ACCEL="-cpu host -accel kvm"
else
    ACCEL=""
fi

if [ -f config ]; then
  source config
else
  echo "no config file found"
  exit 1
fi

echo "Checking for availability and consitency of original "$NETINST
if [ ! -f $NETINST.iso ]; then
  wget -c -nd "$URL_ROOT$NETINST.iso"
fi
if md5sum --quiet -c $NETINST.md5; then
  echo $NETINST.iso is there and correct
else
  echo "md5sum for $NETINST.iso is not correct, please check"
  exit 1
fi

if [ ! -f preseed.cfg ]; then
  echo "no preseed.cfg file there? Aborting"
  exit 1
fi

if [ -d isofiles ]; then
  rm -rf isofiles
fi

if [ -f installer.log ]; then
  rm installer.log
fi

echo "Running nc to capture syslogs... To view installer steps use: tail -f installer.log"
nc -u -l -p10514 > installer.log &
NC_PID=$!

mkdir isofiles
xorriso -osirrox on -indev $NETINST.iso -extract / isofiles
chmod -R +w isofiles
gunzip isofiles/install.amd/initrd.gz
echo preseed.cfg | cpio -H newc -o -A -F isofiles/install.amd/initrd
gzip isofiles/install.amd/initrd
cp isofiles/install.amd/initrd.gz .
cp isofiles/install.amd/vmlinuz .

echo "Creating disk image for Debian Buster x86_64..."
qemu-img create -f qcow2 $PROJECT.qcow 4G

echo "Running Debian Installer...To view installer steps use: tail -f installer.log in a different terminal"
qemu-system-x86_64 $ACCEL -hda $PROJECT.qcow -netdev user,id=net0,hostname=$PROJECT,domainname=localdomain -device rtl8139,netdev=net0,mac=52:54:98:76:54:32 -boot once=n -m 2048 -kernel vmlinuz -append "auto=true DEBIAN_FRONTEND=text log_host=10.0.2.2 log_port=10514 --- console=ttyS0 net.ifnames=0 biosdevname=0" -initrd initrd.gz -cdrom $NETINST.iso -nographic

if [ -d isofiles ]; then
  rm -rf isofiles
fi

echo Setting ssh keys and qemu systype

if [ ! -f robotkey.pub ]; then
  echo "no robotkey existing, creating one"
  ssh-keygen -N '' -m pem -f "robotkey" -C "Robot key"
fi

robotkey=$(cat robotkey.pub)

if [ -f developer_key.pub ]; then
  developer_key=$(cat developer_key.pub)
else
  developer_key=""
fi

./set_ssh_keys.exp "$robotkey" "$developer_key"

echo "Creating disk images for testing as snapshot from base image:"
qemu-img create -f qcow2 -b $PROJECT.qcow $ALICE.qcow
qemu-img create -f qcow2 -b $PROJECT.qcow $BOB.qcow
./set_hostname_alice.exp
./set_hostname_bob.exp

echo "Finally stopping nc"
kill $NC_PID
if kill -0 $NC_PID >/dev/null 2>&1; then
  kill $NC_PID
fi
sleep 2
if kill -0 $NC_PID >/dev/null 2>&1; then
  echo killing nc $NC_PID with -9 as it is still running
  kill -9 $NC_PID
fi
