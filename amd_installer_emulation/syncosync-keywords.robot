*** Settings ***
Documentation          Keywords and other settings for syncosync.robot tests

Library                SSHLibrary
Library                Process
Library                Collections
Library                OperatingSystem
Library                String
Library                SeleniumLibrary

*** Variables ***
${ALICEHOST}           localhost
${ALICEPORT}           2222
${BOBHOST}             localhost
${BOPPORT}             2223
${USERNAME}            root
${PASSWORD}            none
${HEADLESS}            False
${SLOW}                False

*** Keywords ***
Compare Remote
    [Arguments]                   ${system}  ${subpath}  ${dircontent}
    [Documentation]               looks at systems remote location, if some data for name has arrived
                                  Switch Connection  ${system}
    ${content}                    Execute Command    ls /mnt/rjail/remote/${subpath}
    Should Be Equal               ${content}         ${dircontent}  

Compare Local Remote
    [Arguments]                   ${local}  ${remote}  ${subpath}
    [Documentation]               Compares the data from name on the local and remote system.
    ...                           As syncing takes some time, it waits for a timeout
                                  Switch Connection  ${local}
    ${starttime}                  Get Time  epoch
    ${content}                    Execute Command    ls /mnt/local/${subpath}
                                  Wait Until Keyword Succeeds  2min  5s  Compare Remote  ${remote}  ${subpath}  ${content}
    ${endtime}                    Get Time  epoch
    ${duration}                   Evaluate  ${endtime}-${starttime}
                                  Log  Compare Local Remote time: ${duration} seconds  console=yes
Remote Check Loop
    [Arguments]                   ${system}
    [Documentation]               Just check if the remote host is up
                                  Switch Connection  ${system}
    ${stdout}                     Execute Command   remotehost.py -c
    Should contain                ${stdout}         RemoteUpStatus.UP

Remote Check
    [Documentation]               Checks if the remote host is up with some timeout
    [Arguments]                   ${system}
    ${starttime}                  Get Time  epoch
                                  Wait Until Keyword Succeeds  2min  5s  Remote Check Loop  ${system}
    ${endtime}                    Get Time  epoch
    ${duration}                   Evaluate  ${endtime}-${starttime}
                                  Run Keyword If  ${duration} > 10  Log  Remote Check time: ${duration} seconds  console=yes
     
    
Open Connection On Host
    [Arguments]                   ${host}  ${port}  ${name}
                                  Open Connection   ${host}  port=${port}   alias=${name}
                                  Login With Public Key    root  robotkey
    
Open Connection And Log In
                                  Run  ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2222"
                                  Run  ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2223"
                                  Wait Until Keyword Succeeds  2min  5s  Open Connection On Host  ${ALICEHOST}  ${ALICEPORT}  alice   
                                  Wait Until Keyword Succeeds  2min  5s  Open Connection On Host  ${BOBHOST}  ${BOPPORT}  bob   

Open Chrome
    [Arguments]    ${url}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Run Keyword If 	${HEADLESS} == True  Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Goto   ${url}
