#!/bin/bash -e

# starts multiple images
# (c) 2019,2020 Stephan Skrodzki <stevieh@syncosync.org>


if [ -c /dev/kvm ]
then
    ACCEL="-cpu host -accel kvm"
else
    ACCEL=""
fi


start_qemu() {
  echo Running $1
  echo Ports: $2 for ssh, $3 for sos, $4 for webui, ip: 10.2.0.$6
  if [ ! -f $1.qcow ]; then
    echo $1.qcow is not existing. You should build it with init_img.sh first
    return
  fi
  if [ ! -f $1.img ]; then
    echo $1.img is not existing. You should build it with gen_testdrives.sh first
    return
  fi
  nohup qemu-system-x86_64 $ACCEL -name $1 -hda $1.qcow \
    -hdb $1.img \
    -netdev user,id=net0,dhcpstart=10.0.2.$6,hostname=$1,domainname=localdomain,hostfwd=tcp::$2-:22,hostfwd=tcp::$3-:55055,hostfwd=tcp::$4-:5000 \
    -device rtl8139,netdev=net0,mac=52:54:98:76:54:$5 \
    -boot once=n -m 2048 $graphics >$1.log &
  echo $! $2 >>img_run.stat
}

# Usage info
show_help() {
  cat <<EOF
Usage: ${0##*/} [-h] [-g] <alice> <bob>
Start syncosync instances with X86 qemus 

    -h          display this help and exit
    -g          start with graphics (to get logins)

EOF
}

graphics="-nographic"

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "hg" opt; do
  case "$opt" in
  h)
    show_help
    exit 0
    ;;
  g)
    graphics=""
    ;;

  '?')
    show_help >&2
    exit 1
    ;;
  esac
done

shift "$((OPTIND - 1))" # Shift off the options and optional --.

if [ -f img_run.stat ]; then
  rm img_run.stat
fi
THISSSHPORT=2222
THISSOSPORT=55055
THISUIPORT=5555
THISMAC=10
THISIP=15

if [ $# -eq 0 ]; then
  start_qemu alice $THISSSHPORT $THISSOSPORT $THISUIPORT $THISMAC $THISIP
else
  for name in "$@"; do
    start_qemu $name $THISSSHPORT $THISSOSPORT $THISUIPORT $THISMAC $THISIP
    ((THISSSHPORT++))
    ((THISSOSPORT++))
    ((THISUIPORT++))
    ((THISMAC++))
    ((THISIP++))
  done
fi
