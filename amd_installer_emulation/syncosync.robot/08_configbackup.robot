*** Settings ***
Documentation          Config Backup and restore testing

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections


*** Test Cases ***
create a configbackup
    [Documentation]               Just create a configuration backup and look if it is there
                                  Switch Connection  alice  
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${cfgbu_ali}                   Get Substring  ${content}  26 
                                  SSHLibrary.File Should Exist  ${cfgbu_ali}
    
    
create a configbackup on bob
    [Documentation]               Just create a configuration backup on bob and check if it is on alice
                                  Switch Connection  bob
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${filepath}                   Get Substring  ${content}  37
                                  Compare Local Remote  bob  alice  syncosync
    
restore configbackup 
    [Documentation]               Restore configbackup on alice
                                  Switch Connection  alice  
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${filepath}                   Get Substring  ${content}  26 
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    sysstate.py -v DEBUG --default -c '${filepath}'

restore with additional dir
    [Documentation]               Restore with an additional directory
                                  Switch Connection  alice
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${filepath}                   Get Substring  ${content}  26 
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    cp -r /mnt/local/ali_ta1 /mnt/local/add_acc
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    sysstate.py -v DEBUG --default -c '${filepath}'
  
delete this account again
    [Documentation]               Delete add acc
                                  Switch Connection  alice  
    ${content}                    Execute Command    sosaccounts.py -d '{"name":"add_acc"}'  return_stderr=True
      
 