*** Settings ***
Documentation          Add, Modify and Delete Accounts

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***
add an account on alice
    [Documentation]               Add an account on alice
                                  Switch Connection  alice  
    ${fingerprint}                Run                ssh-keygen -l -E md5 -f ali_ta1.pub | awk '{ print substr($2,5) }'
    ${pubkey}                     OperatingSystem.Get File           ali_ta1.pub
    ${pubkey}                     Strip String       ${pubkey}
    ${content}                    Execute Command    sosaccounts.py -a '{"name":"ali_ta1","rlongname": "Alice Test Account 1", "ssh_pub_key": "${pubkey}"}'  return_stderr=True
    Should Not Contain Match      ${content}         *soscore.soserrors.AccountAlreadyExistingError*
    # check for account    
    ${content}                    Execute Command    sosaccounts.py -l
    Should Contain                ${content}         "backup_period": 7
    Should Contain                ${content}         "info_period": 7
    Should Contain                ${content}         "last_mail": 0
    Should Contain                ${content}         "mail_address": ""
    Should Contain                ${content}          "mail_info_level": "error"
    Should Contain                ${content}         "max_space_allowed": 100
    Should Contain                ${content}         "name": "ali_ta1"
    Should Contain                ${content}         "remind_duration": 22
    Should Contain                ${content}         "rlongname": "Alice Test Account 1"
    Should Contain                ${content}         "key_fingerprint": "${fingerprint}"    
    SSHLibrary.Directory Should Exist 	      /mnt/local/ali_ta1/data
    SSHLibrary.File Should Exist             /etc/syncosync/accountkeys/ali_ta1    

add same account again
    [Documentation]               Add same account
                                  Switch Connection  alice  
    ${content}                    Execute Command    sosaccounts.py -a '{"name":"ali_ta1","rlongname": "Alice Test Account 1"}'  return_stderr=True 
    Should Contain Match          ${content}         *soscore.soserrors.AccountAlreadyExistingError*

mod account 
    [Documentation]               Modify ali_ta1
                                  Switch Connection  alice  
    ${content}                    Execute Command    sosaccounts.py -m '{"name":"ali_ta1","mail_address": "ali_ta1@syncosync.org"}'  return_stderr=True
    ${content}                    Execute Command    sosaccounts.py -l
    Should Contain                ${content}         "mail_address": "ali_ta1@syncosync.org"


del account 
    [Documentation]               Delete ali_ta1
                                  Switch Connection  alice  
    ${content}                    Execute Command    sosaccounts.py -d '{"name":"ali_ta1"}'  return_stderr=True
    ${content}                    Execute Command    sosaccounts.py -l
    Should Contain                ${content}         []
    SSHLibrary.Directory Should Not Exist 	  /mnt/local/ali_ta1  
    SSHLibrary.File Should Not Exist         /etc/syncosync/accountkeys/ali_ta1  
    

add the account again
    [Documentation]               Add an account on alice
                                  Switch Connection  alice  
    ${pubkey}                     OperatingSystem.Get File           ali_ta1.pub
    ${pubkey}                     Strip String       ${pubkey}
    ${content}                    Execute Command    sosaccounts.py -a '{"name":"ali_ta1","rlongname": "Alice Test Account 1", "ssh_pub_key": "${pubkey}"}'  return_stderr=True

add an account on bob
    [Documentation]               Add an account on bob
                                  Switch Connection  bob
    ${password}                   Run                openssl passwd -6 Bobta1pwd  
    ${content}                    Execute Command    sosaccounts.py -a '{"name":"bob_ta1","rlongname": "Bob Test Account 1", "password":"${password}"}'  return_stderr=True 

# After these tests, we should have on Alice one account ali_ta1 with login via ssh key and on Bob the account bob_ta1 with password login Bobta1pwd

