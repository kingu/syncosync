*** Settings ***
Documentation          Check UI Login

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${URL ALICE}           http://localhost:5555/en-US/
${BROWSER}             Chrome
${DELAY}               0.5

*** Test Cases ***
Valid Login
    Open Chrome          ${URL ALICE}
    Set Selenium Speed   ${DELAY}
    Page Should Contain  Login
    Input Text           password  sosadmin
    Click Button         Login
    Log Location
    Page Should Contain  Logout

Mod Account
    [Documentation]      Modify account ali_ta1
    Click Element        ali_ta1
    BuiltIn.Sleep                3s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Click Element        modAccount
    Input Text           xpath: //input[@id="longName"]    ALICE TEST ACCOUNT One
    Click Element        xpath: //button[@type="submit"]
    BuiltIn.Sleep                3s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Click Element        ali_ta1
    Page Should Contain  ALICE TEST ACCOUNT One

Add Account
    Click Element       addAccount
    BuiltIn.Sleep               3s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Element Should Contain        xpath: //form[@id="addAccount"]    Add Account
    Input Text           xpath: //input[@id="accountname"]    ali_ta2
    Input Text           xpath: //input[@id="longName"]    Da Robot Made It
    Input Text           xpath: //input[@id="password"]    Joshua20
    Input Text           xpath: //input[@id="passwordCheck"]    Joshua20
    BuiltIn.Sleep                2s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Click Element        xpath: //button[@type="submit"]
    BuiltIn.Sleep                3s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Page Should Contain  Da Robot Made It

Delete Account
    [Documentation]      Delete account ali_ta2
    Click Element        ali_ta2
    BuiltIn.Sleep                2s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Click Element        delAccount
    BuiltIn.Sleep                2s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Click Element        xpath: //button[@id="delConfirm"]
    BuiltIn.Sleep                4s
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    Page Should Not Contain  ali_ta2
