*** Settings ***
Documentation          Sync Data to sos accounts

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections


*** Test Cases ***
sync data to ali_ta1
    [Documentation]               Put some demodata on alice
                                  Switch Connection  alice  
                                  Run                rsync -a --rsh="ssh -p 2222 -o StrictHostKeyChecking=no -l ali_ta1 -i ali_ta1" ali_ta1.data/* localhost:    
    ${content}                    Execute Command    ls /mnt/local/ali_ta1/data
    Should Contain                ${content}         ali_ta1_0.data	ali_ta1_1.data ali_ta1_2.data ali_ta1_3.data ali_ta1_4.data	ali_ta1_5.data ali_ta1_6.data  
    ${heresum}                    Run                md5sum < ali_ta1.data/ali_ta1_0.data
    ${theresum}                   Execute Command    md5sum < /mnt/local/ali_ta1/data/ali_ta1_0.data
    Should Be Equal               ${heresum}  ${theresum}

sync data to bob_ta1
    [Documentation]               Put some demodata on bob
                                  Switch Connection  bob  
                                  Run                sshpass -pBobta1pwd rsync -a --rsh="ssh -p 2223 -o StrictHostKeyChecking=no -l bob_ta1" bob_ta1.data/* localhost:    
    Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  10
    ${content}                    Execute Command    ls /mnt/local/bob_ta1/data
    Should Contain                ${content}         bob_ta1_0.data	bob_ta1_1.data bob_ta1_2.data bob_ta1_3.data bob_ta1_4.data	bob_ta1_5.data bob_ta1_6.data  
    ${heresum}                    Run                md5sum < bob_ta1.data/bob_ta1_0.data
    ${theresum}                   Execute Command    md5sum < /mnt/local/bob_ta1/data/bob_ta1_0.data
    Should Be Equal               ${heresum}  ${theresum}

is data synced to bob
    [Documentation]               is the data above synced to bob?
                                  Compare Local Remote  alice  bob  ali_ta1/data    

is data synced to alice
    [Documentation]               is the data above synced to alice?
                                  Compare Local Remote  bob  alice  bob_ta1/data    

delete data on ali_ta1
    [Documentation]               Delete the data on ali_ta1 and check results
                                  Switch Connection  alice 
                                  Execute Command    rm /mnt/local/ali_ta1/data/*
                                  Compare Local Remote  alice  bob  ali_ta1/data    

create the data again          
    [Documentation]               Just put again the data for later checks
                                  Run                rsync -a --rsh="ssh -p 2222 -o StrictHostKeyChecking=no -l ali_ta1 -i ali_ta1" ali_ta1.data/* localhost:
    

