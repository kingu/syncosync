*** Settings ***
Documentation          sysstate switch testing

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections


*** Test Cases ***
check for sysstate
    [Documentation]               Check for actual sysstate
                                  Switch Connection  alice  
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${filepath}                   Get Substring  ${content}  26 
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    sysstate.py -v DEBUG --default -c '${filepath}'
    
