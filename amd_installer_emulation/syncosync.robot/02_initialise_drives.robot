*** Settings ***
Documentation          Init Drives and check mounting

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***
format drives
    [Documentation]              Format all available drives
                                 Switch Connection  alice
    ${stdout}                    Execute Command    sysstate.py -o
    ${stdout}                    Execute Command    drivemanager.py -u
    ${stdout}                    Execute Command    drivemanager.py -f all -y
    Should Contain               ${stdout}          Success, free space 124 extends
    ${stdout}                    Execute Command    pvs --separator ","
    Should Contain               ${stdout}          /dev/sdb,sos,lvm2,a--,496.00m
                                 Switch Connection  bob
    ${stdout}                    Execute Command    sysstate.py -o
    ${stdout}                    Execute Command    drivemanager.py -u
    ${stdout}                    Execute Command    drivemanager.py -f all -y
    Should Contain               ${stdout}          Success, free space 124 extends
    ${stdout}                    Execute Command    pvs --separator ","
    Should Contain               ${stdout}          /dev/sdb,sos,lvm2,a--,496.00m

create volumes
    [Documentation]              Create Volumes
                                 Switch Connection  alice  
    ${stdout}                    Execute Command    drivemanager.py -p '{"local": 56 , "remote": 56 }' -y
    Should Contain               ${stdout}          Success
    ${stdout}                    Execute Command    lvs --separator ","
    Should Contain               ${stdout}          local,sos,-wi-a-----,224.00m,,,,,,,,
    Should Contain               ${stdout}          remote,sos,-wi-a-----,224.00m,,,,,,,,
    Should Contain               ${stdout}          system,sos,-wi-a-----,48.00m,,,,,,,,
                                 Switch Connection  bob  
    ${stdout}                    Execute Command    drivemanager.py -p '{"local": 56 , "remote": 56 }' -y
    Should Contain               ${stdout}          Success
    ${stdout}                    Execute Command    lvs --separator ","
    Should Contain               ${stdout}          local,sos,-wi-a-----,224.00m,,,,,,,,
    Should Contain               ${stdout}          remote,sos,-wi-a-----,224.00m,,,,,,,,
    Should Contain               ${stdout}          system,sos,-wi-a-----,48.00m,,,,,,,,

mount volumes
    [Documentation]              Mount Volumes
                                 Switch Connection  alice  
    ${stdout}                    Execute Command    drivemanager.py -m
    Should Contain               ${stdout}          All sos volumes mounted
    ${stdout}                    Execute Command    mount
    Should Contain               ${stdout}          /dev/mapper/sos-local on /mnt/local type ext4 (rw,relatime,jqfmt=vfsv0,usrjquota=aquota.user)
    Should Contain               ${stdout}          /dev/mapper/sos-remote on /mnt/rjail/remote type ext4 (rw,relatime)
    Should Contain               ${stdout}          /dev/mapper/sos-system on /mnt/system type ext4 (rw,relatime
    ${stdout}                    Execute Command    drivemanager.py -u
    Should Contain               ${stdout}          All sos volumes unmounted
    ${stdout}                    Execute Command    mount
    Should Not Contain           ${stdout}          /dev/mapper/sos-local on /mnt/local type ext4 (rw,relatime,jqfmt=vfsv0,usrjquota=aquota.user)
    Should Not Contain           ${stdout}          /dev/mapper/sos-remote on /mnt/rjail/remote type ext4 (rw,relatime)
    Should Not Contain           ${stdout}          /dev/mapper/sos-system on /mnt/system type ext4 (rw,relatime
