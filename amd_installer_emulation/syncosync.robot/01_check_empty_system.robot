*** Settings ***
Documentation          Check parameters for an empty system

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***
Execute Command And Verify Output  
    [Documentation]          Just test, if echo works
                             Switch Connection  alice
    ${output}=               Execute Command    echo Hello SSHLibrary!
    Should Be Equal          ${output}          Hello SSHLibrary!
syncosync service started
    [Documentation]          Is syncosync service running?
                             Switch Connection  alice
    ${output}=               Execute Command    systemctl is-active syncosync.service
    Should Be Equal          ${output}          active
chroot env generated
    [Documentation]          Is the sos chroot environment generated?
                             Switch Connection  alice
    ${output}=               Execute Command    ls -1 /var/lib/syncosync/chroot/bin/
    Should Contain           ${output}          rsync
    Should Contain           ${output}          bash
disk recognized
    [Documentation]          Is there a disk available?
                             Switch Connection  alice
    ${output}=               Execute Command    drivemanager.py -g
    Should Contain           ${output}          "device": "sdb" 
disk empty
    [Documentation]          Is the disk empty?
                             Switch Connection  alice
    ${output}=               Execute Command    drivemanager.py -g
    Should Contain           ${output}          "sos_found": false
add account with no volumes mounted
    [Documentation]          Could I add an account?
                             Switch Connection  alice
    ${stderr}=               Execute Command    sosaccounts.py -a '{"name": "sostst1"}'     return_stderr=True 
    Should Contain Match     ${stderr}          *soserrors.VolumeNotMounted*
list accounts should return empty list
    [Documentation]          We should get an empty list of sos accounts
                             Switch Connection  alice
    ${stdout}                Execute Command    sosaccounts.py -l
    Should Be Equal          ${stdout}          []
check sosconfig
    [Documentation]          check sosconfig set by postinst
                             Switch Connection  alice
                             SSHLibrary.File Should Exist         /etc/syncosync/sosconfig.json

    

