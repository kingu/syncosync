# amd64 Installer and Emulation

The emulation allows easy testing and image preparation without influencing the developer's host.

## Setup:

If you want your own public key to be added to the images (for testing and development!) copy it inside this directory with the name: developer_key.pub

start init_img.sh it will need some time and does the following steps:

 * Download (if not happened already) debian netinstaller iso img
 * Create an empty qcow img syncosync.qcow
 * Install this image using preseed.cfg
   For this step: this may take some time, as it is a debian installation and also produces network traffic to download everything. If you are not connected to the internet, you could download a debian mirror and change preseed.cfg to use this.
 * Generate a robotkey (for automated testing) without password (and if not existing already)
   Important: due to restrictions in paramiko for automated testing, do not generate a 4096bit key. e.g. use
   `ssh-keygen -t rsa -b 1024 -f robotkey`
 * Install robotkey and developer_key on the image (using set_ssh_keys.exp)
 * Copy the ready to use syncosync.qcow to alice.qcow and bob.qcow

Now you have a running syncosync installation.

Still you need data volumes to be used with this installations. This is done by starting gen_testdrives.sh

gen_testdrives.sh generates the following img files:

 * alice.img, bob.img - These are really plain empty images and should be used for all run tests except disksetup testing
 * linux.img - a simple ext3 formatted partition on it
 * dos.img - an image containing a dos partition

The linux and the dos image are useful to test ui and core presentation of non sos disks at installation time.
For the creation of sos images and test their detection inside a syncosync installation, there will be another script.

Now use `start_img.sh alice` to start alice, or `start_img.sh alice bob` to start alice and bob.

## Requirements (Debian):
apt install qemu-system-x86 qemu-kvm ncat xorriso qemu-utils expect
