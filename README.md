# syncosync - secure peer to peer backup synchronization

syncosync is a software for running a backup appliance, normally a
Raspberry Pi4 with a USB3 connected hard disk drive. The main function
is to receive backups from other computers on the local network and
synchronize these backups to a similar device at a different local
position. The device on the other side of the network does the same, so
two devices on two sides of the network are always in sync.

It is Free Software and released under the
[GNU Affero General Public License V3](http://www.gnu.org/licenses/agpl.html).

Its website can be found at [syncosync.org](https://syncosync.org/?utm_source=github&utm_medium=readme).

The documentation is here in the [Wiki](https://git.stevekist.de/syncosync/syncosync/wikis/home)

![Screenshot](https://syncosync.org/images/sos_screenshot.png)

You are currently looking at the source code repository of syncosync.



