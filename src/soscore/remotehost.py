"""
Managing remote hosts
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import copy
import os
import re
import socket

import paramiko

from sosmodel import sosconstants
from sosmodel.remotehost import RemoteHostModel
from sosmodel.sos_enums import RemoteUpStatus
from soscore import sysstate, configbackup

import logging

logger = logging.getLogger(__name__)


class IgnorePolicy(paramiko.MissingHostKeyPolicy):
    """
    Policy for accepting any host key.
    TODO: is this secure enough?
    """

    def missing_host_key(self, client, hostname, key):
        return


def check_host(hostname, port, keyfile) -> RemoteUpStatus:
    """
    this checks any other host if it is reachable via ssh, could be used for evaluation
    :param keyfile: the path to a keyfile, this is not the key itself!
    :param hostname: hostname of remote host
    :param port: port
    :return: RemoteUpStatus...
            Attention: if BAD_KEY is issued, this means, that the host is reachable and there is an issue
            with the keyfile - either it is wrong or even not there
    """
    if hostname is None or hostname == "":
        return RemoteUpStatus.NOT_AVAILABLE
    if (keyfile is not None) and (not os.path.isfile(keyfile)):
        logger.log(logging.DEBUG, "Checking host {hostname} port {str(port)} with no keyfile")
        keyfile = None
    else:
        logger.log(logging.DEBUG, f"keyfile: {keyfile}")
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.WarningPolicy())
    try:
        client.connect(hostname, int(port), username="syncosync", timeout=1, allow_agent=False, key_filename=keyfile)
    except paramiko.SSHException as e:
        if str(e) == 'No existing session' \
                or str(e) == "Error reading SSH protocol banner[Errno 104] Connection reset by peer":
            client.close()
            return RemoteUpStatus.NO_SSH
        if str(e) == 'Private key file is encrypted':
            client.close()
            return RemoteUpStatus.BAD_KEY
        else:
            logger.error(f"ssh exception: {str(e)}")
            client.close()
            return RemoteUpStatus.FAULTY
    except socket.error as e:
        logger.error(f"socket error: {str(e)}")
        client.close()
        return RemoteUpStatus.NO_REPLY
    except Exception as e:
        client.close()
        return RemoteUpStatus.HOST_CLOSED
    client.close()
    return RemoteUpStatus.UP


def read_from_file(filepath=sosconstants.LSYNCD_CONF_FILE) -> RemoteHostModel:
    if not os.path.isfile(filepath):
        return False
    my_remotehost = RemoteHostModel()
    input_file = open(sosconstants.LSYNCD_CONF_FILE, "r")
    file_content = input_file.read()
    m = re.search(r'.*target.*=.*\"(.*):.*', file_content)
    if m:
        my_remotehost.hostname = m.group(1)
    else:
        my_remotehost.hostname = ""

    m = re.search(r'.*_extra.*-p ([0-9]*).*', file_content)
    if m:
        my_remotehost.port = m.group(1)
    else:
        my_remotehost.port = ""
    return my_remotehost


class RemoteHost:
    """
    remote host getter, setter and controls
    """
    old_remotehost: RemoteHostModel = None

    def __init__(self):
        self.remotehost = RemoteHostModel()
        self.last_read = 0
        self.__read_config()

    def __read_config(self):
        """
        internal method for reading configuration from lsyncd.config
        """
        if round(os.stat(sosconstants.LSYNCD_CONF_FILE).st_mtime) > self.last_read:
            input_file = open(sosconstants.LSYNCD_CONF_FILE, "r")
            file_content = input_file.read()
            m = re.search(r'.*target.*=.*\"(.*):.*', file_content)
            if m:
                self.remotehost.hostname = m.group(1)
            else:
                self.remotehost.hostname = ""

            m = re.search(r'.*_extra.*-p ([0-9]*).*', file_content)
            if m:
                self.remotehost.port = m.group(1)
            else:
                self.remotehost.port = ""
            self.last_read = round(os.stat(sosconstants.LSYNCD_CONF_FILE).st_mtime)
            input_file.close()
            self.old_remotehost = copy.deepcopy(self.remotehost)
        return

    def __write_config(self):
        """
        internal method for writing configuration to lsyncd.config
        """
        input_file = open(sosconstants.LSYNCD_CONF_FILE, "r")
        file_content = input_file.read()
        input_file.close()
        m = re.compile('(.*target.*=.*").*(:.*)')
        res = m.sub(r'\g<1>' + str(self.remotehost.hostname) + r'\g<2>', file_content)
        file_content = res
        m = re.compile(r'(.*_extra.*-p )[0-9]*(.*)')
        res = m.sub(r'\g<1>' + str(self.remotehost.port) + r'\g<2>', file_content)
        file_content = res
        f = open(sosconstants.LSYNCD_CONF_FILE, "w")
        f.write(file_content)
        f.close()
        self.last_read = round(os.stat(sosconstants.LSYNCD_CONF_FILE).st_mtime)
        return

    def set(self, new_remotehost: RemoteHostModel):
        """
        sets a new remotehost
        :param new_remotehost: RemoteHostModel
        """
        if self.old_remotehost.to_json() != self.remotehost.to_json():
            self.old_config = copy.deepcopy(self.remotehost)
            self.__write_config()
            # we could try do detect, if we should open now the sshd
            my_sysstate = sysstate.SysState()
            my_sysstate.resetmode()
            my_configbackup = configbackup.ConfigBackup()
            my_configbackup.backup()

            return True
        else:
            return False

    def get(self):
        """
        Returns a RemoteHostModel
        :return:  RemoteHostModel
        """
        return self.remotehost

    def check_host(self) -> RemoteUpStatus:
        """
        checks if a remote host is listening
        :return: RemoteUpStatus
        NOT_AVAILABLE -> show yellow
        UP: Host is up and responding -> show green
        Everything else: host is not up or not responding or no key or whatever -> show red
        """
        self.__read_config()
        return check_host(self.remotehost.hostname, self.remotehost.port, str(sosconstants.SOS_KEYFILE))
