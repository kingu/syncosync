"""
managing all around drives, volumes etc.
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import subprocess
import time
import parted
import os
import stat
import logging

from soscore import soserrors, configbackup, sosconfig
from sosmodel import sosconstants
from sosmodel.sos_enums import Partition, VolMount
from sosmodel.sosconstants import MOUNT, MOUNTDEV, MOUNTOPTS

logger = logging.getLogger(__name__)


def set_local_ownership():
    os.chown(os.path.join(sosconstants.MOUNT[Partition.LOCAL], "."), 0, 0)
    try:
        os.system(f"chown -R root:root {os.path.join(sosconstants.MOUNT[Partition.LOCAL], 'lost+found')}")
    except:
        logger.error(f"Could not change owner root:root of /mnt/local/lost+found")
    try:
        os.system(f"chown -R root:root {os.path.join(sosconstants.MOUNT[Partition.LOCAL], 'syncosync')}")
    except:
        logger.error(f"Could not change owner root:root of /mnt/local/syncosync")


def set_remote_ownership():
    try:
        os.system(
            f"chown -R syncosync:syncosync {os.path.join(sosconstants.MOUNT[Partition.REMOTE], '.')}")
    except:
        logger.error(f"Could not change owner syncosync:syncosync of /mnt/rjail/remote")


def mount_drive(device, destination, params) -> bool:
    """
    mounts a drive
    :param device: see man mount
    :param destination:
    :param params:
    :return: True if success
    """
    logger.debug(f"Mounting {device} to {destination} with opts: {params}")
    try:
        subprocess.run(f"mount -o \'{params}\' {device} {destination}", shell=True)
    except Exception as e:
        logger.error(f"Was not able to mount {device}: {e}")
        return False
    return True


def umount_volume(volume) -> bool:
    """
    unmounts a single volume
    :param volume: enum which defines a volume (e.g. sos_enum.Partition.LOCAL)
    :return: True if success
    """
    result = True
    try:
        logger.debug(f"Unmounting volume {MOUNTDEV[volume]}")
        subprocess.run(f"umount {MOUNTDEV[volume]}", shell=True)
    except Exception as e:
        logger.error(f"Was not able to unmount {MOUNTDEV[volume]}: {e}")
        result = False
    return result


def umount_sos_volumes(volumes=sosconstants.SOS_VOLUMES) -> bool:
    """
    unmount a list of volumes (see above)
    :param volumes:
    :return:
    """
    """ just unmount the volumes in the list"""
    result = True
    # TODO: move this to the right place: subprocess.call(["service", "sosaccountaccess", "stop"])
    for volume in volumes:
        result = result and umount_volume(volume)
    return result


def get_mountpoint(blkdev):
    if os.path.islink(blkdev):
        blkdev = os.readlink(blkdev)
    with open('/proc/mounts') as fp:
        for line in fp:
            if line[0] == '/':
                line = line.split()
                if os.path.islink(line[0]):
                    chkdev = os.readlink(line[0])
                else:
                    chkdev = line[0]
                if chkdev == blkdev:
                    arglist = []
                    if line[3] != "":
                        arglist = line[3].split(",")
                    return line[1], arglist
        return None, []


class PartitionInfo:
    """ info about a partition on a drive """

    def __init__(self, number, name, length, fstype=None):
        self.number = number
        self.name = name
        self.length = length
        self.fstype = fstype


class Drive:
    """ status of a drive at boot(?) time"""

    def __init__(self):
        self.id = None
        self.product = None
        self.revision = None
        self.device = None
        self.sata_size = 0
        self.vg_uuid = None
        self.vg_name = None
        self.vg_state = None
        self.partitions = []
        pass

    def generate(self, lsscsi_line):
        self.id = lsscsi_line[0:9]
        self.product = lsscsi_line[30:47]
        self.revision = lsscsi_line[47:51]
        self.device = lsscsi_line[58:61]
        try:
            with open('/sys/class/block/' + self.device + '/size') as f:
                self.sata_size = int(f.read()) * 512
        except:
            self.sata_size = 0
        return self.__get_details()

    def __get_details(self):
        """
        tries to find out, what is on a hdd
        :return:
        True if the drive was readable in any way
        Fals else
        """
        try:
            device = parted.getDevice("/dev/" + self.device)
        except Exception as e:
            logger.debug(f"Could not get details from drive {self.device}: {e}")

            return False
        try:
            disk = parted.newDisk(device)
        except:
            return True
        # ok, this disk should be readable
        for partition in disk.partitions:
            filesystem = partition.fileSystem
            if filesystem:
                fstype = filesystem.type
            else:
                fstype = None
            try:
                temp_partition_name = partition.name
            except:
                temp_partition_name = ""
            new_partition = PartitionInfo(partition.number, temp_partition_name, partition.getLength(), fstype)
            self.partitions.append(new_partition)
        return True

    def initialise(self):
        """
        initialises a drive
        :return: True if everything is fine, False else
        """
        # TODO: should we check if the drive is mounted anywhere?
        subprocess.run("wipefs -af /dev/" + self.device, shell=True)
        time.sleep(3)
        subprocess.run("pvcreate -ffy /dev/" + self.device, shell=True)
        logger.log(logging.INFO, "Drive " + self.device + " initialised")
        return True


class PhysicalVolumeGroup:
    def __init__(self):
        self.pv_dev = None
        self.vg_uuid = None
        self.vg_name = None

    def generate(self, line):
        fields = line.split(',')
        self.pv_dev = fields[0][7:]
        self.vg_uuid = fields[1]
        self.vg_name = fields[2]


class PhysicalVolumeGroups:
    def __init__(self):
        self.pv = []
        self.update()

    def update(self):
        self.pv = []
        pvs_output = os.popen('pvs -o pv_name,vg_uuid,vg_name --noheadings --separator=","').readlines()
        for line in pvs_output:
            line = line.rstrip()
            mypv = PhysicalVolumeGroup()
            mypv.generate(line)
            self.pv.append(mypv)


class VolumeGroup:
    def __init__(self):
        self.vg_uuid = None
        self.vg_extent_size = None
        self.vg_extent_count = None
        self.vg_free = None
        self.vg_status = None
        self.vg_name = None

    def generate(self, line):
        fields = line.split(',')
        self.vg_uuid = fields[0][2:]
        self.vg_name = fields[1]
        self.vg_extent_size = fields[2]
        self.vg_extent_count = fields[3]
        self.vg_free = fields[4]
        self.vg_status = fields[5][3]


class LogicalVolume:
    """
    holding information about one logical volume
    """
    # lv_name, lv_uuid, lv_full_name, lv_path, lv_active, lv_size, vg_uuid, vg_name

    def __init__(self):
        self.lv_name = None
        self.lv_uuid = None
        self.lv_full_name = None
        self.lv_path = None
        self.lv_active = None
        self.lv_size = 0
        self.vg_uuid = None
        self.vg_name = None
        self.mountpoint = None

    def generate(self, line):
        line = line.rstrip()
        fields = line.split(',')
        self.lv_name = fields[0][2:]
        self.lv_uuid = fields[1]
        self.lv_full_name = fields[2]
        self.lv_path = fields[3]
        self.lv_active = fields[4]
        self.lv_size = int(float(fields[5]))
        self.vg_uuid = fields[6]
        self.vg_name = fields[7]
        self.mountpoint = get_mountpoint(os.readlink(self.lv_path))


class LogicalVolumes:
    """
    holding all logical volumes detected
    """

    def __init__(self):
        self.lv = dict()
        ## If false, there is a lv from vg sos which is not active ...
        self.active = True
        self.update()

    def update(self):
        lvs_output = os.popen(
            'lvs -o lv_name,lv_uuid,lv_full_name,lv_path,lv_active,lv_size,vg_uuid,vg_name --units 4m --nosuffix --noheadings --separator=","').readlines()
        for line in lvs_output:
            mylv = LogicalVolume()
            mylv.generate(line)

            # when at least one volume is not active, set the whole vg state to inactive
            if mylv.vg_name == "sos" and mylv.lv_active != "active":
                self.active = False
            self.lv[mylv.lv_full_name] = mylv
        # also if there is no lv at all
        if not self.lv:
            self.active = False


class VolumeGroups:
    """
    holding all volumegroups detected. Should be only one: sos
    """

    def __init__(self):
        self.vg = dict()
        self.bad = False
        self.multiple_sos = False
        self.sos_found = False
        self.cfgmd5sum = None
        self.cfgdate = 0
        self.cfghostname = ""
        self.update()

    def update(self):
        vgs_output = os.popen(
            'vgs -o vg_uuid,vg_name,vg_extent_size,vg_extent_count,vg_free,vg_attr --units 4m --nosuffix --noheadings --separator=","').readlines()
        for line in vgs_output:
            myvg = VolumeGroup()
            myvg.generate(line)
            # check if the volume group is good or bad
            if myvg.vg_status == 'p':
                self.bad = True
            # check and set if there are multiple sos groups
            if myvg.vg_name == 'sos':
                if self.sos_found:
                    self.multiple_sos = True
                self.sos_found = True
            self.vg[myvg.vg_uuid] = myvg   # add this group

        # now check here, if there is a configuration on it
        # this is only possible if there is exact one sos vg not none, not more
        if self.sos_found and not self.multiple_sos:
            # test if the special device exists
            try:
                blk_exists = stat.S_ISBLK(os.stat("/dev/mapper/sos-local").st_mode)
            except:
                blk_exists = False
            if blk_exists:
                # now check first if the /dev/mapper/sos-local is already mounted
                was_mounted = True
                if not os.path.ismount(sosconstants.MOUNT[Partition.LOCAL]):
                    was_mounted = False
                    mount_drive(MOUNTDEV[Partition.LOCAL], MOUNT[Partition.LOCAL], "ro")
                configs = configbackup.get_stored()
                if len(configs) > 0:
                    self.cfgmd5sum = configs[0].md5sum
                    self.cfgdate = configs[0].timestamp
                    self.cfghostname = configs[0].hostname
                if not was_mounted:
                    umount_volume(Partition.LOCAL)

    def get_free_sos_extents(self):
        if self.multiple_sos:
            return -1
        else:
            for myvg in self.vg.values():
                if myvg.vg_name == "sos":
                    return int(float(myvg.vg_free))

    def __getitem__(self, item):
        return self.vg[item]


class Drives:
    """ reading semistatic information drivelist """

    def __init__(self):
        self.non_sos_drive = False
        self.drives = []
        self.pvs = []
        self.vgs = None
        self.pvs = None
        self.lvs = None
        self.sos_vol_mount = False

    def read_cache(self):
        """
        reads boot status from cache, this should not really happen.
        :return:
        """

        if os.path.isfile(sosconstants.DRIVESTATUS_CACHE):
            with open(sosconstants.DRIVESTATUS_CACHE) as json_file:
                file_data = json.load(json_file)
                for drive in file_data['drive']:
                    newdrive = Drive()
                    newdrive.__dict__ = drive
                    self.drives.append(newdrive)
            return True
        else:
            return False

    def generate(self):
        """
        this should be done once at boot time
        :return:
            True if it is a startable situation
            False else
        """
        logger.debug("Generating drive status")
        myconfigcaller = sosconfig.SosConfig()
        myconfig = myconfigcaller.get_system_description()
        drive_order = myconfig.drives
        # initialise all drives which could be found
        for i in range(0, len(drive_order)):
            self.drives.append(None)

        lsscsi_output = os.popen('/usr/bin/lsscsi').readlines()
        for line in lsscsi_output:
            drive = Drive()
            if drive.generate(line):
                # now find at which entry in the array, this drive should show up
                for i in range(0, len(drive_order)):
                    # print("id: "+drive.id+" order: "+drive_order[i])
                    if drive.id == drive_order[i]:
                        # print("drive added:"+drive.device)
                        self.drives[i] = drive

        # now, read the whole LVM setup
        self.vgs = VolumeGroups()
        self.pvs = PhysicalVolumeGroups()
        self.lvs = LogicalVolumes()
        for pv in self.pvs.pv:
            for i in self.drives:
                if i and i.device == pv.pv_dev[0:3]:
                    i.vg_uuid = pv.vg_uuid
                    i.vg_name = pv.vg_name
                #    self.vgs[pv.vg_uuid].vg_name = pv.vg_name
                    if pv.vg_uuid:
                        i.vg_state = self.vgs[pv.vg_uuid].vg_status
        self.non_sos_drive = False
        for drive in self.drives:
            if drive and drive.vg_name != "sos":
                self.non_sos_drive = True
        if not self.vgs.multiple_sos and self.vgs.sos_found:
            # ok, there is only one sos vg, now check, if all drives are mounted
            local_mountpoint, local_params = get_mountpoint(MOUNTDEV[Partition.LOCAL])
            system_mountpoint, system_params = get_mountpoint(MOUNTDEV[Partition.SYSTEM])
            remote_mountpoint, remote_params = get_mountpoint(MOUNTDEV[Partition.REMOTE])
            if local_mountpoint is None and system_mountpoint is None and remote_mountpoint is None:
                self.sos_vol_mount = VolMount.NONE
            elif local_mountpoint == MOUNT[Partition.LOCAL] and system_mountpoint == MOUNT[Partition.SYSTEM] \
                    and remote_mountpoint == MOUNT[Partition.REMOTE]:
                self.sos_vol_mount = VolMount.NORMAL
            elif local_mountpoint == MOUNT[Partition.REMOTE] and system_mountpoint == MOUNT[Partition.SYSTEM] \
                    and remote_mountpoint == MOUNT[Partition.LOCAL]:
                self.sos_vol_mount = VolMount.SWAP
            else:
                self.sos_vol_mount = VolMount.INCOMPLETE

        return not (self.non_sos_drive or self.vgs.bad or self.vgs.multiple_sos)

    def to_json(self):
        """
        produces json dump of all data: drives, volumes, physical volumes...
        :return:  formatted json string
        """
        return json.dumps(self.__dict__, default=lambda o: o.__dict__, indent=4)

    def get_free_data(self) -> int:
        """
        outputs available free extents
        :return:
        """
        return self.vgs.get_free_sos_extents()

    def get_partitioning(self) -> (int, int):
        """
        outputs local and remote volume size in 4MB blocks
        :return:
        int: local
        int: remote
        """
        pass

    def get_drivelist(self):
        """
        returns array with device filenames e.g. ['sda','sdb']
        """
        drivelist = []
        for drive in self.drives:
            if drive:
                drivelist.append(drive.device)
        return drivelist

    def get_drive_by_device(self, device):
        """
        returns drive selected by device (e.g. sda)
        :param device: e.g. sda
        :return: drive object if found
        """
        for drive in self.drives:
            if drive and drive.device == device:
                return drive
        return None

    def create_sos(self, drivelist):
        """
        creates sos volumegroup
        :param drivelist:  array of devicenames e.g. ["sdb","sdc","sdd"]
        :return: available extents
        """
        for volume in sosconstants.SOS_VOLUMES:
            if os.path.ismount(sosconstants.MOUNT[volume]):
                raise soserrors.SosError(f"{sosconstants.MOUNT[volume]} is still mounted. "
                                         f"Stop sos services and unmount")
        # now, the rest is really up to the risk of the caller
        subprocess.run("vgremove -fy sos", shell=True)
        for volume in sosconstants.SOS_VOLUMES:
            try:
                blk_exists = stat.S_ISBLK(os.stat(f"{sosconstants.MOUNTDEV[volume]}").st_mode)
            except:
                blk_exists = False
            if blk_exists:
                cp = subprocess.run(f"dmsetup remove -f sos-{volume}", shell=True)
                if cp.returncode != 0:
                    logger.log(logging.ERROR, f"Could not remove volume {volume}")
                else:
                    logger.log(logging.INFO, f"Volume {volume} removed")
        devices = []
        for drivestr in drivelist:
            drive = self.get_drive_by_device(drivestr)
            if drive:
                drive.initialise()
                devices.append("/dev/" + drive.device)
        if devices:
            s = ' '
            driveliststr = s.join(devices)
            subprocess.run("vgcreate -fy sos " + driveliststr, shell=True)
            logger.log(logging.INFO, "Created VG group sos with " + driveliststr)
            self.generate()
        else:
            logger.log(logging.ERROR, "Empty drivelist for creating sos")
        return self.vgs.get_free_sos_extents()

    def mount_sos_volumes(self, swap=False, local_ro=False, remote_ro=False):
        """
        mounts all sos volumes (local, remote, system)
        :param swap: if True mount reverse -> see disaster swap
        :param local_ro: mount to local mountpoint read only
        :param remote_ro: mount to remote mountpoint read only
        :return: True if successful, false else
        TODO: should we throw more detailed exceptions?
        """
        logging.debug(f"Mounting all sos volumes witch swap mode: {swap}")
        result = True
        if not (self.non_sos_drive or self.vgs.bad or self.vgs.multiple_sos):
            ## we can do only something if there is at least something to mount
            local_mountpoint, local_params = get_mountpoint(MOUNTDEV[Partition.LOCAL])
            system_mountpoint, system_params = get_mountpoint(MOUNTDEV[Partition.SYSTEM])
            remote_mountpoint, remote_params = get_mountpoint(MOUNTDEV[Partition.REMOTE])

            logger.debug(
                f"Mountpoints: Local:{local_mountpoint}, Remote:{remote_mountpoint}, System:{system_mountpoint}")

            # First, check, if the partitions should swap - this means, we have to unmount them both
            if local_mountpoint is not None:
                if not swap and local_mountpoint == MOUNT[Partition.REMOTE] \
                        or (swap and local_mountpoint == MOUNT[Partition.LOCAL]):
                    subprocess.run(f"quotaoff {MOUNT[Partition.LOCAL]}", shell=True)
                    umount_volume(Partition.LOCAL)
                    local_mountpoint = None
            if remote_mountpoint is not None:
                if swap and remote_mountpoint == MOUNT[Partition.REMOTE] \
                        or (not swap and remote_mountpoint == MOUNT[Partition.LOCAL]):
                    umount_volume(Partition.REMOTE)
                    remote_mountpoint = None

            # Now mount everything according the requests:
            # remount if mode change (ro <-> rw)
            # system_mountpoint is always mounted rw
            if system_mountpoint is None:
                mount_drive(MOUNTDEV[Partition.SYSTEM], MOUNT[Partition.SYSTEM], MOUNTOPTS[Partition.SYSTEM])

            if swap:
                local_device = MOUNTDEV[Partition.REMOTE]
                remote_device = MOUNTDEV[Partition.LOCAL]
            else:
                local_device = MOUNTDEV[Partition.LOCAL]
                remote_device = MOUNTDEV[Partition.REMOTE]

            local_paramstr = MOUNTOPTS[Partition.LOCAL]
            if local_ro:
                local_paramstr = local_paramstr + ",ro"
            #    subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)
            else:
                local_paramstr = local_paramstr + ",rw"

            if local_mountpoint is None:
                # this means, no remount necessary
                mount_drive(local_device, MOUNT[Partition.LOCAL], local_paramstr)
            else:
                if (local_ro and not "ro" in local_params) or (not local_ro and "ro" in local_params):
                    local_paramstr = local_paramstr + ",remount"
                    mount_drive(local_device, MOUNT[Partition.LOCAL], local_paramstr)

            if not local_ro:
                if not os.path.isfile(os.path.join(MOUNT[Partition.LOCAL], "aquota.user")):
                    logger.warning(f"MOUNT[Partition.LOCAL]/aquota.user is missing and will be generated, "
                                   f"this should happen only once after formatting")
                    subprocess.run(f"quotacheck -c {MOUNT[Partition.LOCAL]}", shell=True)
                    # TODO: should we also reset the quota per user here? Basically yes.
                    subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)
                else:
                    subprocess.run(f"quotacheck {MOUNT[Partition.LOCAL]}", shell=True)
                    subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)

            # now the remote mount
            remote_paramstr = MOUNTOPTS[Partition.REMOTE]
            if remote_ro:
                remote_paramstr = remote_paramstr + ",ro"
            else:
                remote_paramstr = remote_paramstr + ",rw"

            if remote_mountpoint is None:
                # this means, no remount necessary
                mount_drive(remote_device, MOUNT[Partition.REMOTE], remote_paramstr)
            else:
                if (remote_ro and not "ro" in remote_params) or (not remote_ro and "ro" in remote_params):
                    remote_paramstr = remote_paramstr + ",remount"
                    mount_drive(remote_device, MOUNT[Partition.REMOTE], remote_paramstr)

            if not remote_ro:
                subprocess.run(f"chown -R syncosync:syncosync {MOUNT[Partition.REMOTE]}", shell=True)

        else:
            logger.error(f"Drive status does not allow mounting sos volumes"
                         f"non_sos_drive: {self.non_sos_drive}"
                         f"vgs_bad: {self.vgs.bad}"
                         f"vgs.multiple_sos: {self.vgs.multiple_sos}")
            result = False
        return result

    def create_volumes(self, volumes):
        """
        creates local,remote and system volumes on the sos group
        :param volumes: json representation of local and remote volume size in extents (4MB)
        :return: True if successful
        """
        available_extents = self.vgs.get_free_sos_extents()
        extents = json.loads(volumes)
        if 'local' in extents:
            extents['local'] = int(extents['local'])
        else:
            logger.log(logging.ERROR, f"json does not provide local volume size:"
                                      f"{volumes}")
            return False
        if 'remote' in extents:
            extents['remote'] = int(extents['remote'])
        else:
            logger.log(logging.ERROR, f"json does not provide remote volume size:"
                                      f"{volumes}")
            return False
        if (extents['local'] + extents['remote'] + sosconstants.SYSTEM_MIN_SIZE) > available_extents:
            logger.log(logging.ERROR, f"Requested volume sizes of {extents['local']}(local)"
                                      f"{extents['remote']}(remote) {sosconstants.SYSTEM_MIN_SIZE}(system min size)"
                                      f"exceeds size of sos volume group of {available_extents}")
            return False
        else:
            extents['system'] = available_extents - (extents['local'] + extents['remote'])
        for volume in ['local', 'remote', 'system']:
            cp = subprocess.run(f"lvcreate -y --name {volume} -l {extents[volume]} sos", shell=True)
            if cp.returncode != 0:
                logger.log(logging.ERROR,
                           f"Can not create {volume} volume with a size of {extents[volume]} extents.")
                return False
            else:
                logger.log(logging.INFO, f"Created {volume} volume with a size of {extents[volume]} extents.")
        for volume in sosconstants.SOS_VOLUMES:
            cp = subprocess.run(f"mkfs.ext4 -F -F {MOUNTDEV[volume]}", shell=True)
            if cp.returncode != 0:
                logger.log(logging.ERROR, f"Can not create ext4 filesystem on {MOUNTDEV[volume]}")
                return False
            else:
                logger.log(logging.INFO, f"Created ext4 filesystem on {MOUNTDEV[volume]}")
            cp = subprocess.run(f"tune2fs -r 0 {MOUNTDEV[volume]}", shell=True)
            if cp.returncode != 0:
                logger.log(logging.ERROR, f"Can't tune2fs {MOUNTDEV[volume]}")
                return False
        self.generate()
        # now, as these volumes are empty. We should store a configuration on them
        # therefore, we have to mount them
        self.mount_sos_volumes()
        sosconfigcall = sosconfig.SosConfig()
        myconfig = sosconfigcall.get()
        myconfig.local = extents['local']
        myconfig.remote = extents['remote']
        sosconfigcall.set()
        my_configbackup = configbackup.ConfigBackup()
        my_configbackup.backup()
        umount_sos_volumes()
        return True
