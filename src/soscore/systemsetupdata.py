"""
Handling of systemsetupdata json to exchange between sos buddies
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sosmodel.systemsetupdata import SystemSetupDataModel
from sosmodel.sos_enums import SsdAction
from soscore import soskey, drivemanager


def get_system_ssd():
    # collect the data from different areas
    myssd = SystemSetupDataModel()
    mykey = soskey.SosKey()
    mykeykey = mykey.get()
    myssd.pub_key = mykeykey.pub_key
    mydrive = drivemanager.Drives()
    mydrive.generate()
    myssd.free = mydrive.get_free_data()
    try:
        myssd.local = mydrive.lvs.lv["sos/local"].lv_size
    except KeyError:
        myssd.local = None
    try:
        myssd.remote = mydrive.lvs.lv["sos/remote"].lv_size
    except KeyError:
        myssd.remote = None
    return myssd
