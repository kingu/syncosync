"""
About generating and handling the syncosync key
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Class to handle the syncosync key. Note: key exchange is a different file and class

import os
import subprocess
import logging

from soscore import soshelpers, configbackup
from sosmodel import sosconstants
from sosmodel.soskey import SosKeyModel

logger = logging.getLogger(__name__)


class SosKey:

    def __init__(self):
        self.soskey = SosKeyModel()
        self.__update()

    def __update(self):
        """
        sets fingerprint from keyfile
        :return:
        """
        if os.path.isfile(sosconstants.SOS_KEYFILE_PUB):
            with open(sosconstants.SOS_KEYFILE_PUB) as f:
                self.soskey.pub_key = f.read().rstrip()
            self.soskey.fingerprint = soshelpers.sshfingerprint(self.soskey.pub_key)
        else:
            logger.debug("Requested fingerprint for sos key but no key file is existing")
            self.soskey.fingerprint = None

    def generate(self):
        """
        Generates the syncosync key without
        :return:
        True if success
        False else
        """
        result = True
        if not os.path.isdir(sosconstants.SOS_KEYFILE_DIR):
            os.mkdir(sosconstants.SOS_KEYFILE_DIR)
        try:
            subprocess.run(
                f"echo 'y' | ssh-keygen -q -N '' -f {sosconstants.SOS_KEYFILE} -C 'syncosync'",
                shell=True)
            self.__update()
            logger.info(f"Generated syncosync key with fp {self.soskey.fingerprint}")
            my_configbackup = configbackup.ConfigBackup()
            my_configbackup.backup()
        except Exception as e:
            logger.error(f"Was not able to generate sos key in {sosconstants.SOS_KEYFILE_DIR}: {e}")
            self.soskey.fingerprint = None
            result = False
        return result

    def get(self):
        """
        standard getter
        :return:  SosKeyModel
        """
        return self.soskey
