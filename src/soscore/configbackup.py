"""
classes and functions for handling backup of sos configuration
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import calendar
import crypt
import glob
import os
import re
import shutil
import socket
import subprocess
import tarfile
import tempfile
import time
import json
import logging
import spwd

import gnupg

from soscore import soshelpers, sosaccounts, soserrors, drivemanager, remotehost
from sosmodel.configbackup import ConfigBackupModel, ConfigBackupContentModel
from sosmodel.sosaccount import SosAccountModel, SosAccountAddModel
from sosmodel import sos_enums, sosconstants
from sosmodel.hostname import HostNameModel
from sosmodel.sosconstants import MOUNT
from sosmodel.sos_enums import Partition
from sosmodel.serializerbase import SerializerBaseJsonEncoder, SerializerBaseJsonDecoder

logger = logging.getLogger(__name__)


def parse_filepath(filepath):
    """
    parses filepath for date, md5sum and salt
    :param filepath:
    :return: { "date": date, "md5sum": md5sum, "salt": salt }
    """
    m = re.search(
        '.*sosconf_(?P<date>\d*)_(?P<hostname>[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])_(?P<md5sum>[a-z0-9]*)_(?P<salt>.*)\.tgz\.gpg',
        filepath)
    if m:
        return m.groupdict()
    else:
        return None


def get_metadata_from_filename(filepath):
    """
    reads the metadata from a config backup file by parsing the filname

    :param filepath: the path to the config file
    :return: ConfigBackupModel with metadata set from file
    """
    configdata = ConfigBackupModel()
    mydict = parse_filepath(filepath)
    if mydict is not None:
        configdata.timestamp = round(calendar.timegm(time.strptime(mydict["date"], "%Y%m%d%H%M%S")))
        configdata.md5sum = mydict["md5sum"]
        configdata.hostname = mydict["hostname"]
        configdata.filepath = filepath
    return configdata


def get_stored(dirpath=os.path.join(sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.SOS_CONF_BACKUP_PATH)):
    """
    returns a list backup configs which are stored
    on the path

    :return:  list of ConfigBackupModel objects
    """
    logger.debug(f"retrieving stored configuration backups from {dirpath}")
    stored_list = []
    if os.path.isdir(dirpath):
        files = sorted(glob.glob(dirpath + "*"), reverse=True)
        for file in files:
            stored_list.append(get_metadata_from_filename(file))
    logger.debug(f"found {len(stored_list)} configuration backups")
    return stored_list


def decrypt(filepath, password) -> str:
    """
    decrypts and unpacks a backup file to a tmp directory
    check for exceptions raised.
    :param filepath: full path to a backup file
    :param password: password
    :return: full path to directory of unpacked file, "" if not successful
    """
    mydict = parse_filepath(filepath)
    if mydict is not None:
        md5sum = mydict["md5sum"]
        salt = soshelpers.base642str(mydict["salt"])
    else:
        raise soserrors.ConfigBackupFilenameError

    if password is None:
        mypwdict = {}
        mypwdict = soshelpers.parse_shadowpwd_data(spwd.getspnam("sosadmin")[1])
        if mypwdict is None or 'salt' not in mypwdict:
            logger.log(logging.ERROR, f"sosadmin passwordhash has no salt!? Aborting backupconfig")
            return ""
        pwhash = mypwdict['salt'] + '$' + mypwdict['pwhash']
    else:
        pwhash = crypt.crypt(password, salt=salt)
    gpg = gnupg.GPG()
    # this trick should give you a tmpfilename, for the decrypt output. Do we need more?
    mytmpfile = tempfile.NamedTemporaryFile(delete=False)
    mytmpfile.write(b"foo")
    outname = mytmpfile.name
    mytmpfile.close()
    logger.debug(f"Trying to decrypt: {filepath} with passphrase {pwhash}")
    with open(filepath, 'rb') as f:
        gpg.decrypt_file(f, passphrase=pwhash, output=outname)
        if soshelpers.md5Checksum(outname) != md5sum:
            raise soserrors.ConfigBackupFileDecryptionError
    return outname


def unpack(filepath, password) -> str:
    """
    decrypts and unpacks a backup file to a tmp directory
    check for exceptions raised.
    :param filepath: full path to a backup file
    :param password: password
    :return: full path to directory of unpacked file, "" if not successful
    """
    logger.debug(f"entered unpack with {filepath}, {password}")
    outname = decrypt(filepath, password)
    if outname == "":
        return ""
    logger.debug(f"got {outname} as tar file")
    with tarfile.open(outname) as tar:
        target_dir = tempfile.mkdtemp()
        logger.debug(f"target_dir:{target_dir}")
        tar.extractall(path=target_dir)
    logger.debug(f"unpacked successful {filepath} to {target_dir}")
    os.remove(outname)
    return target_dir


def get_data(filepath, password) -> ConfigBackupContentModel:
    my_output = ConfigBackupContentModel()
    src_dir = unpack(filepath, password)
    if src_dir == "":
        logger.warning("Could not unpack config backup file")
        return None
    else:
        my_output.sosaccounts = []
        if os.path.isfile(os.path.join(src_dir, "sosaccounts.json")):
            with open(os.path.join(src_dir, 'sosaccounts.json')) as f:
                accounts_to_restore_json = f.read()
                my_output.sosaccounts = SerializerBaseJsonDecoder.decode(SosAccountAddModel, accounts_to_restore_json)
                for account in my_output.sosaccounts:
                    if account.name == "sosadmin":
                        my_output.sosaccounts.remove(account)
        if os.path.isfile(os.path.join(src_dir, "etc/hostname")):
            my_output.hostname = HostNameModel()
            with open(os.path.join(src_dir, 'etc/hostname')) as f:
                my_output.hostname.hostname = f.read().rstrip()
        my_output.remotehost = remotehost.read_from_file(os.path.join(src_dir, "etc/lsyncd/lsyncd.conf.lua"))
        shutil.rmtree(src_dir)
        return my_output


def restore(filepath, password):
    """
    restores a configuration backup to the actual configuration.
    Handle with care!

    :param filepath: the path and filename of the configuration backup
    :param password: password

    :return: True if everything went fine, false else.
    """
    report = []
    src_dir = unpack(filepath, password)
    if src_dir == "":
        report.append("Could not unpack restore file for unknown reason")
        return False, report
    result, report = restore_from_dir(src_dir)
    # remove the unpacked dir finally
    shutil.rmtree(src_dir)
    return result, report


def restore_from_dir(src_dir):
    """
    restore from an already unpacked configuration backup
    the src_dir will not be removed after restore!
    :param src_dir: the dir of the unpacked configuration
    :return: result -> bool, report -> list of strings
    """
    report = []
    # we also do not restore if sshd is open - there could be glitches
    # let's restore the usual files, this is easy
    # but switch off quota first to be able to restore also aquota.user
    subprocess.run(f"quotaoff {MOUNT[Partition.LOCAL]}", shell=True)
    os.remove(os.path.join(MOUNT[Partition.LOCAL], "aquota.user"))
    for restorefile in sosconstants.CONFIGRESTORE_ITEMS:
        src = os.path.join(src_dir, restorefile[1:])
        dest = restorefile
        if os.path.isdir(src):
            if os.path.isdir(dest):
                shutil.rmtree(dest)
            shutil.copytree(src, dest)
            logger.debug(f"Restored directory {dest}")
        elif os.path.isfile(src):
            shutil.copy(src, dest)
            logger.debug(f"Restored file {dest}")
        else:
            logger.warning(f"Item {restorefile} not found in archive to restore")
    # check quotas and restart quota
    subprocess.run(f"quotacheck -c {MOUNT[Partition.LOCAL]}", shell=True)
    subprocess.run(f"quotaon {MOUNT[Partition.LOCAL]}", shell=True)
    # now the more complicated part: restoring accounts
    accounts_to_restore = []
    if os.path.isfile(os.path.join(src_dir, "sosaccounts.json")):
        with open(os.path.join(src_dir, 'sosaccounts.json')) as f:
            accounts_to_restore_json = f.read()
            accounts_to_restore = SerializerBaseJsonDecoder.decode(SosAccountAddModel, accounts_to_restore_json)
    else:
        logger.warning("No sosaccounts.json file to restore. Is that correct?")
        return False, report
    # now check if accounts have to be modified or added
    my_accounts = sosaccounts.SosAccounts()
    for account in accounts_to_restore:
        if account.name != "sosadmin":
            logger.debug(f"Restoring account {account.name}")
            my_account: SosAccountModel = my_accounts.get_by_name(account.name)
            if not os.path.isdir(os.path.join(MOUNT[Partition.LOCAL], account.name)):
                logger.warning(f"Won't add or modify account {account.name}: no homedir")
                report.append(f"Won't add or modify account {account.name}: no homedir")
            else:
                if my_account is not None:
                    # this account is already existing and should me modified
                    logger.info(f"Modifiying account: {account.name}")
                    # first we transform it to an AddModel
                    mod_account = SosAccountAddModel.from_json(my_account.to_json())
                    # print(f"mod_account1: {mod_account.to_json()}")
                    # then we mod json with the to restored data
                    mod_account.update_from_json(account.to_json())
                    # print(f"mod_account2: {mod_account.to_json()}")
                    # then we mod it. Voilá!
                    my_accounts.mod(mod_account, no_backup=True)
                else:
                    # this account does not exist
                    logger.info(f"Restore by adding new account: {account.name}")
                    report.append(f"Restored account: {account.name}")
                    my_accounts.add(account, no_backup=True)
    # now for the danger zone:
    # first remove accounts which have no home dir. This should not happen
    accountlist = my_accounts.get()
    for account in accountlist:
        if not os.path.isdir(os.path.join(MOUNT[Partition.LOCAL], account.name)):
            logger.warning(f"Account {account.name} removed: no home directory")
            report.append(f"Account {account.name} removed: no home directory")
            sosaccounts.del_by_name(account.name)
    # now create accounts which have a home dir but no account. This should also not happen
    accountlist = sosaccounts.getsosaccount_names()
    for itempath in glob.glob(os.path.join(MOUNT[Partition.LOCAL], "*")):
        item = os.path.basename(itempath)
        logger.debug(f"Checking for local directory {item}")
        if os.path.isdir(os.path.join(MOUNT[Partition.LOCAL], item)) and item != "lost+found" \
                and item != "syncosync" and item not in accountlist:
            # we should try to add an account
            logger.debug(f"Creating account for orphaned dir: {item}")
            newaccount = SosAccountAddModel()
            try:
                newaccount.name = item
            except ValueError:
                logger.warning(f"{item} is not an allowed account name. No account created.")
                report.append(f"{item} is not an allowed account name. No account created.")
            newaccount.rlongname = "Created from config restore"
            try:
                my_accounts.add(newaccount, no_backup=True)
                logger.info(f"Created account for orphaned dir: {item}")
                report.append(f"Created account for orphaned dir: {item}")
            except soserrors.AccountCreationError:
                logger.warning(f"Could not create an account for {item}")
                report.append(f"Could not create an account for {item}")
    # iterate once again to correct home dirs and quotas
    accountlist = my_accounts.get()
    for account in accountlist:
        # lets create the home again, to be sure, all stuff is there and ownership is set right
        sosaccounts.create_account_home(account.name)
        # recreate quota
        sosaccounts.set_quota(account.name, account.max_space_allowed)
    # finally sync everything
    os.sync()
    return True, report


class ConfigBackup:
    """
    This class handles all actions about the _actual_ sos configuration.
    The model is described in sosmodel.configbackup
    """

    def __init__(self):
        self.configbackup = ConfigBackupModel()

    def backup(self):
        """
        backups the configuration to /mnt/local.. and sets metadata

        :return: destfilepath of backed up config, "" if not successful
        """
        local_mountpath, local_params = drivemanager.get_mountpoint(sosconstants.MOUNTDEV[sos_enums.Partition.LOCAL])
        if local_mountpath != sosconstants.MOUNT[sos_enums.Partition.LOCAL]:
            logger.error(f"Local volume not mounted locally. No configuration backup possible")
            return ""
        else:
            self.configbackup.timestamp = round(time.time())
            self.configbackup.hostname = socket.gethostname()
            soshelpers.write_to_file(sosconstants.SOS_CONF_TIMESTAMP_FILEPATH,
                                     json.dumps({'timestamp': self.configbackup.timestamp}))
            filepath = self.__create_archive()
            # this is the timestamp for the filename
            file_timestamp = time.strftime("%Y%m%d%H%M%S", time.gmtime(self.configbackup.timestamp))

            # now, we know already, that the local volume is mounted, but we should also check if the destination
            # directory exists
            if not os.path.isdir(
                    os.path.join(sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.SOS_CONF_BACKUP_PATH)):
                os.makedirs(
                    os.path.join(sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.SOS_CONF_BACKUP_PATH))
            gpg = gnupg.GPG()
            mypwdict = {}
            mypwdict = soshelpers.parse_shadowpwd_data(spwd.getspnam("sosadmin")[1])
            if mypwdict is None or 'salt' not in mypwdict:
                logger.log(logging.ERROR, f"sosadmin passwordhash has no salt!? Aborting backupconfig")
                return ""

            base64_salt = soshelpers.str2base64(mypwdict['salt'])
            destfilepath = os.path.join(sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                                        sosconstants.SOS_CONF_BACKUP_PATH) + "sosconf_" + file_timestamp + "_" \
                           + self.configbackup.hostname + "_" + self.configbackup.md5sum + "_" \
                           + base64_salt + ".tgz.gpg"

            with open(filepath, 'rb') as f:
                passphrase = mypwdict['salt'] + '$' + mypwdict['pwhash']
                gpg.encrypt_file(f, recipients=None, symmetric="AES256", output=destfilepath, passphrase=passphrase,
                                 armor=False)
            logger.log(logging.INFO, f"written configuration backup to {destfilepath}")
            os.remove(filepath)
            soshelpers.write_to_file(sosconstants.SOS_CONF_BACKUP_METADATA, self.configbackup.to_json())
            return destfilepath

    def __create_archive(self):
        """
        creates a config backup tar gz as a temp file without deleting it

        :return: the filepath of the archive
        """
        # ok so, we have some place, where we can backup the configuration, let's go
        start_dir = os.getcwd()
        with tempfile.TemporaryDirectory() as target_dir:
            os.chdir(target_dir)
            # now get the sosusers
            mysosaccounts = sosaccounts.SosAccounts()
            accountlist = mysosaccounts.get()
            saveaccountlist = []
            # accountjson = "["
            for account in accountlist:
                saveaccount = SosAccountAddModel.from_json(account.to_json())
                #  add the password
                saveaccount._password = spwd.getspnam(account.name)[1]
                # add the ssh key
                if os.path.isfile(os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, saveaccount.name)):
                    # ok, we have a file. Should we check it or is it ok, just to add it?
                    with open(os.path.join(sosconstants.ACCOUNT_KEYFILE_DIR, saveaccount.name)) as keyfile:
                        saveaccount.ssh_pub_key = keyfile.read()
                saveaccountlist.append(saveaccount)
            sosadminaccount = SosAccountAddModel.from_json('{"name":"sosadmin"}')
            sosadminaccount.password = spwd.getspnam("sosadmin")[1]
            saveaccountlist.append(sosadminaccount)
            # accountjson = accountjson + "]"
            accountjson = SerializerBaseJsonEncoder.to_json(saveaccountlist)
            soshelpers.write_to_file(target_dir + "/sosaccounts.json", accountjson)
            with tempfile.NamedTemporaryFile('wb', suffix='.tgz', delete=False) as f:
                with tarfile.open(fileobj=f, mode='w:gz') as tar:
                    for name in sosconstants.CONFIGBACKUP_ITEMS:
                        if os.path.isfile(name) or os.path.isdir(name):
                            tar.add(name)
                        else:
                            logger.warning(f"file {name} not found for backup")
                    tar.close()
                f.close()
            self.configbackup.md5sum = soshelpers.md5Checksum(f.name)
        os.chdir(start_dir)
        return f.name

    def get(self):
        """
        provides actual configuration

        :return: ConfigBackupModel Note: if timestamp is 0, this means, that the configuration was not
        backupped. This is mainly because there was no local volume mounted yet.
        """
        if self.configbackup.timestamp == 0:
            # ok, lets get the metadata from the actual configuration, hope there is a backup
            if os.path.isfile(sosconstants.SOS_CONF_BACKUP_METADATA):
                with open(sosconstants.SOS_CONF_BACKUP_METADATA) as file:
                    self.configbackup = ConfigBackupModel.from_json(file.read())
                    logger.debug(f"found a configuration backup with md5sum: {self.configbackup.md5sum}")
        return self.configbackup
