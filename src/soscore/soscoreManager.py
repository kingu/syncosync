#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import time
import signal
from multiprocessing.managers import SyncManager
from threading import Thread, Event
from typing import List, Optional

from soscore.remotehost import RemoteHost
from soscore.soskey import SosKey
from soscore.soskeyx import SosKeyExchange
from sosmodel import sosconstants
from soscore.authmanager import AuthManager
from soscore.sysstate import SysState
from soscore.hostname import HostName
from soscore.nameserver import NameServer
from soscore.nicconfig import NicConfig
from soscore.sosconfig import SosConfig
from soscore.sosaccounts import SosAccounts
from soscore.syncstat import SyncStatUpdater
from sosmodel.hostname import HostNameModel
from sosmodel.nameserver import NameServerModel
from sosmodel.nicconfig import NicConfigModel
from sosmodel.remotehost import RemoteHostModel
from sosmodel.sos_enums import SystemMode
from sosmodel.sosaccount import SosAccountAddModel, SosAccountModel
from sosmodel.soskey import SosKeyModel
from sosmodel.syncstat import SyncStatModel, BandWidthModel

import logging

from sosmodel.systemsetupdata import SystemSetupDataModel
from sosmodel.uilanguage import UiLanguage

logger = logging.getLogger(__name__)


class SoscoreManagerManager(SyncManager):
    pass


class SoscoreManager:
    def __init__(self):
        self.__sosuser_list: SosAccounts = SosAccounts()
        self.__sos_config: SosConfig = SosConfig()
        self.__syncstat: SyncStatUpdater = SyncStatUpdater()
        self.__sysstate: SysState = SysState()
        self.__local_sos_key: SosKey = SosKey()
        self.__remote_host: RemoteHost = RemoteHost()

        self._hostname_controller: HostName = HostName()
        self._nameserver_controller: NameServer = NameServer()
        self._nic_controller: NicConfig = NicConfig()
        self._auth_manager: AuthManager = AuthManager()

        self.__stop_signal: Event = Event()
        self.__soscore_maintainer: Thread = Thread(name="soscore_main", target=self.__soscore_main, )
        self.__soscore_maintainer.daemon = False
        self.__soscore_maintainer.start()

    def __soscore_main(self) -> None:
        reset_state = self.__sysstate.resetmode()
        logger.info(f"Started system with state: {reset_state}")
        try:
            while not self.__stop_signal.is_set():
                # self.__report_ids("Soscore Main")
                time.sleep(sosconstants.SYNCSTAT_REFRESH_TIME)
                self.__syncstat.update_data()
        except Exception as e:
            logger.warning("Unhandled exception in soscore main!")
        finally:
            logger.info("SosCore stopping")

    def shutdown_soscore(self):
        self.__sysstate.changemode(SystemMode.SHUTDOWN)
        self.__stop_signal.set()
        self.__soscore_maintainer.join()

    def get_userlist(self) -> List[SosAccountModel]:
        return self.__sosuser_list.get()

    def get_syncstat(self) -> SyncStatModel:
        return self.__syncstat.get()

    def get_bandwidth_history(self) -> List[BandWidthModel]:
        return self.__syncstat.get_bw_history()

    def account_add(self, new_user: SosAccountAddModel):
        # TODO: Catch errors!
        self.__sosuser_list.add(new_user)

    def account_mod(self, edit_user: SosAccountAddModel):
        # TODO: use boolean return or try/catch
        self.__sosuser_list.mod(edit_user)

    def account_delete(self, user_to_delete):
        self.__sosuser_list.delete(user_to_delete)
    # actual usage of controllers

    # Language getter/setter for the entire system
    def get_systemlanguage(self) -> UiLanguage:
        return self.__sos_config.get().ui_language

    def set_systemlanguage(self, language: UiLanguage) -> None:
        self.__sos_config.get().ui_language = language
        self.__sos_config.set()

    # Hostname controller
    def get_hostname(self) -> HostNameModel:
        return self._hostname_controller.get()

    def set_hostname(self, hostname: HostNameModel) -> None:
        # TODO: catch error/read result/whatever
        self._hostname_controller.set(hostname)

    # nameserver controller
    def get_nameservers(self) -> NameServerModel:
        return self._nameserver_controller.get()

    def set_nameservers(self, nameservers: NameServerModel) -> None:
        self._nameserver_controller.set(nameservers)

    # nic config controller
    def get_nic_configs(self) -> List[NicConfigModel]:
        return self._nic_controller.get()

    def set_nic_configs(self, nics: List[NicConfigModel]) -> None:
        self._nic_controller.set(nics)

    # Auth related
    def login(self, password: str) -> Optional[str]:
        return self._auth_manager.login(password)

    def validate_token(self, token: str) -> bool:
        return self._auth_manager.validate_token(token)

    # Sync setup
    def generate_local_key(self) -> SosKeyModel:
        if not self.__local_sos_key.generate():
            raise ValueError("Failed generating sos key")
            # TODO: throw specific exception for the UI
        else:
            return self.__local_sos_key.get()

    def get_local_key(self) -> SosKeyModel:
        return self.__local_sos_key.get()

    def get_remote_host(self) -> RemoteHostModel:
        return self.__remote_host.get()

    def set_remote_host(self, new_remote_host) -> None:
        self.__remote_host.set(new_remote_host)

    def delete_remote_key(self) -> None:
        SosKeyExchange.remove_remote_key()

    def get_remote_key(self) -> SosKeyModel:
        return SosKeyExchange.get_remote_key()

    def accept_remote_key(self) -> None:
        SosKeyExchange.accept_key()

    def key_exchange_lead(self) -> SystemSetupDataModel:
        return SosKeyExchange.send_ssd()

    def key_exchange_follow(self) -> SystemSetupDataModel:
        # SosKeyExchange.receive_cancel_signal.clear()
        SosKeyExchange.receive_cancel_signal = False
        return SosKeyExchange.receive_ssd()

    def key_exchange_cancel(self) -> None:
        logger.debug("key exchange canceled from soscoreManager")
        # SosKeyExchange.receive_cancel_signal.set()
        SosKeyExchange.receive_cancel_signal = True
        pass

    @staticmethod
    def __report_ids(msg):
        logger.debug('uid, gid = %d, %d; %s' % (os.getuid(), os.getgid(), msg))

    @staticmethod
    def get_foo():
        return "foo"






