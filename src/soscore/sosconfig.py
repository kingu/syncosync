"""
managing sos dedicated configuration which is not in other configuration areas
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import copy

from soscore import soshelpers, configbackup
from sosmodel import sosconstants
from sosmodel.sosconfig import SosConfigModel, SystemDescriptionModel
import os
import logging

logger = logging.getLogger(__name__)


class SosConfig:
    last_config_read = 0
    old_config: SosConfigModel = None
    last_soskey_fp_read = 0

    def __init__(self):
        self.sosconfig = SosConfigModel()

    def get_system_description(self) -> SystemDescriptionModel:
        """
        load the system description part of the config
        :return:
        """
        self.__update_config()
        type_config_file = os.path.join(sosconstants.SOS_SYSTEM_DESCRIPTIONS, self.sosconfig.systype,
                                        "type_config.json")
        if os.path.isfile(type_config_file):
            with open(type_config_file) as json_file:
                mysystemdescription = SystemDescriptionModel.from_json(json_file.read())
                if mysystemdescription.version != sosconstants.SOS_VERSION:
                    logger.warning(f"type config file {type_config_file} has different version ")
        else:
            logger.log(logging.ERROR, f"type config file for system {self.sosconfig.systype} is not existing.")
            mysystemdescription = SystemDescriptionModel()
        return mysystemdescription

    def __update_config(self):
        """
        checks if config file is more recent due to update from somewhere else
        :return:
        """
        if os.path.isfile(sosconstants.SOS_CONF_FILE) and round(
                os.stat(sosconstants.SOS_CONF_FILE).st_mtime) > self.last_config_read:
            with open(sosconstants.SOS_CONF_FILE) as json_file:
                data = json_file.read()
                mysosconfig = SosConfigModel()
                mysosconfig.update_from_json(data)
                if mysosconfig.version != sosconstants.SOS_VERSION:
                    logger.warning(f"conffile has different version {mysosconfig.version}"
                                   f"from soscore version {str(sosconstants.SOS_VERSION)}")
                self.sosconfig = mysosconfig
                self.old_config = copy.deepcopy(mysosconfig)

    def get(self):
        self.__update_config()
        return self.sosconfig

    def set(self):
        """
        sets a new configuration, but only, when there is a change in attributes
        :return: True if configuration has changed and was written, False else.
        """
        # self.sosconfig.version = sosconstants.SOS_VERSION
        if self.old_config is None or self.old_config.to_json() != self.sosconfig.to_json():
            self.old_config = copy.deepcopy(self.sosconfig)
            self.__write_config()
            return True
        else:
            return False

    def __write_config(self):
        """
        writes the configuration as json
        :return: True if everything is fine
        """
        # we do not store the whole json, as drives and nics are not stored here
        soshelpers.write_to_file(sosconstants.SOS_CONF_FILE, self.sosconfig.to_json())
        if os.path.isfile(sosconstants.SOS_CONF_FILE):
            self.last_config_read = round(os.stat(sosconstants.SOS_CONF_FILE).st_mtime)
        myconfig = configbackup.ConfigBackup()
        myconfig.backup()
        return True
