"""
Module to generate content for mails
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import logging
import time
import os
from sosmodel import sosconstants
from sosmodel.sos_enums import SupportedLanguage

my_logging = logging.getLogger(__name__)


def get_raw_text(key, lang: SupportedLanguage = SupportedLanguage.ENGLISH):
    """This function loads and returns the mail content in the given ui language

    :param key: key to message being loaded
    :type key: str
    :param lang: language the message should be loaded in
    :type lang: str
    :return: message with unfilled keywords
    :rtype: str
    """

    # Check if language translation exists
    source = os.path.join(sosconstants.trans_place, "housekeeper."
                          + SupportedLanguage.to_iso(lang, SupportedLanguage.ENGLISH)
                          + ".json")
    if not os.path.isfile(source):
        # If translation doesnt exist, use english as default
        my_logging.log(logging.INFO, "For the given ui language exists no translation! English was used as default")
        source = os.path.join(sosconstants.trans_place, "housekeeper.en.json")

    with open(source, 'r') as f:
        data = f.read()
    obj = json.loads(data)

    try:
        mail_content = obj[key]
    except KeyError:
        my_logging.log(logging.ERROR,
                       "The given translation of "
                       + SupportedLanguage.to_iso(lang, SupportedLanguage.ENGLISH)
                       + " is somehow corrupted or a wrong key was used. Error on loading, used key: " + key)
        raise

    return mail_content


def get_subject(key, lang, hostname):
    """This function returns the subject of an eMail to a given key and language

    :param key: key to mail content type
    :type key: str
    :param lang: Language of the mail being loaded
    :type lang: SupportedLanguage
    :param hostname: hostname of sos
    :type hostname: str
    :return: subject of an eMail
    :rtype: str
    """
    return get_raw_text(key, lang) + " " + hostname


def get_mail_msg(key, lang, dictionary):
    """This function returns the content of a mail with filled in values

    :param key: key to mail content type
    :type key: str
    :param lang: Language of the mail being loaded
    :type lang: SupportedLanguage
    :param dictionary: dictionary for value replacement
    :type dictionary: dict
    :return: ready mail content
    :rtype: str
            """
    mail_content = get_raw_text(key, lang)
    if key == 'backup_overdue_user_msg':
        mail_content = mail_content.replace('ACCOUNT_NAME', dictionary['ACCOUNT_NAME'])
        mail_content = mail_content.replace('OVERDUE_DAYS', dictionary['OVERDUE_DAYS'])
        mail_content = mail_content.replace('OVERDUE_HOURS', dictionary['OVERDUE_HOURS'])
        mail_content = mail_content.replace('LAST_ACCESS_DATE', dictionary['LAST_ACCESS_DATE'])
        mail_content = mail_content.replace('LAST_ACCESS_TIME', dictionary['LAST_ACCESS_TIME'])
    elif key == 'backup_overdue_admin_msg':
        mail_content = mail_content.replace('ACCOUNT_NAME', dictionary['ACCOUNT_NAME'])
        mail_content = mail_content.replace('OVERDUE_DAYS', dictionary['OVERDUE_DAYS'])
        mail_content = mail_content.replace('LAST_ACCESS_DATE', dictionary['LAST_ACCESS_DATE'])
        mail_content = mail_content.replace('LAST_ACCESS_TIME', dictionary['LAST_ACCESS_TIME'])
    elif key == 'remote_host_down_admin_msg':
        mail_content = mail_content.replace('LSYNCD_HOST', dictionary['LSYNCD_HOST'])
        mail_content = mail_content.replace('LSYNCD_PORT', dictionary['LSYNCD_PORT'])
        mail_content = mail_content.replace('REMOTE_DOWN_DATE', dictionary['REMOTE_DOWN_DATE'])
        mail_content = mail_content.replace('REMOTE_DOWN_TIME', dictionary['REMOTE_DOWN_TIME'])
    elif key == 'testmail_content':
        mail_content = mail_content.replace('HOSTNAME', dictionary['HOSTNAME'])
        mail_content = mail_content.replace('LAST_ACCESS_DATE', dictionary['LAST_ACCESS_DATE'])
        mail_content = mail_content.replace('LAST_ACCESS_TIME', dictionary['LAST_ACCESS_TIME'])
    elif key == 'admin_empty_mail':
        pass
    return mail_content


def get_mail_hl(key, lang):
    """This function returns html mail with given content and headline

        :param key: key for right headline
        :type key: str
        :param lang: Language of the headline
        :type lang: SupportedLanguage
        :return: headline in given language
        :rtype: str
            """
    return get_raw_text(key, lang)


def get_html_mail(headline, content, hostname):
    """This function returns html mail with given content and headline

    :param headline: headline of eMail
    :type headline: str
    :param content: content of the eMail
    :type content: str
    :param hostname: hostname for mail signature
    :type hostname: str
    :return: mail ready to be sent
    :rtype: str
        """
    try:
        f = open(sosconstants.html_mail_place, "r")
        html = f.read()
        f.close()
    except PermissionError:
        my_logging.log(logging.WARNING, "Html frame could not be opened, sending eMail without html")
        html = ""
    except OSError:
        my_logging.log(logging.ERROR, "Html frame could not be found! Sending mail without html")
        html = ""

    if html:
        html = html.replace('HOSTNAME', hostname)
        html = html.replace('Headline', headline)
        html = html.replace('Content_Mail', content)
        return html
    else:
        return "<h1>{}</h1>\n{}".format(headline, content)


if __name__ == '__main__':
    print(
        "The mail content generator was called as main. This module should be used to generate mail content in the housekeeper!")
