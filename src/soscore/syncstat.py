"""
syncstat, is used by the syncstat deamon (or by the sos manager)
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import glob
import pwd
import time
import os
import uuid

from soscore import remotehost, sosconfig, soshelpers, drivemanager
from sosmodel import sosconstants, sos_enums
from sosmodel.sos_enums import SyncstatStatus, RemoteUpStatus, SyncStatus, SystemMode
from sosmodel.syncstat import SyncStatModel, BandWidthModel
from sosmodel.sostocheck import SosToCheck
from sosmodel.sysstate import SysStateModel
import socket
import logging

logger = logging.getLogger(__name__)


class SyncStatUpdater:
    """"
    Actively updates all status and stores this also, should normally be only called from
    one process on the system
    """
    def __init__(self):
        self.last_checked = 0
        self.syncstat: SyncStatModel = SyncStatModel()
        self.syncstat.date = 0
        # self.last_stat: int = 0  # this is when we got the last stat
        # last time sysstate was read
        self._last_sysstate = 0
        self.old_remote_up: RemoteUpStatus = RemoteUpStatus.NOT_AVAILABLE  # this stores the state
        # if the remote partner is reachable
        self.remotehostconfig = remotehost.RemoteHost()
        self.remotehostip = ""
        self.oldremotehostip = ""
        self.mylsyncd = self.remotehostconfig.get()
        self.mysosconfig = sosconfig.SosConfig()
        self.aktconfig = self.mysosconfig.get()
        self.aktsystemdescription = self.mysosconfig.get_system_description()
        self.old_syncup: SyncStatus = SyncStatus.NOT_AVAILABLE
        self.old_syncdown: SyncStatus = SyncStatus.NOT_AVAILABLE
        self.oldtx = 0
        self.oldrx = 0
        self.oldbw_up = 0
        self.oldbw_down = 0
        self.oldtime = 0
        self.oldakt_file = ""
        self.drivestatus = drivemanager.Drives()
        self.last_sostocheck_read = 0
        # this is the ringbuffer for bandwidth values
        self.bw_buffer = []

    def update_data(self):
        """
        updates the structure
        :return: True, if data has changed (to issue a get afterwards)
        """
        has_changed = False
        if self.syncstat.date == 0:
            force = True
            logger.debug("First run, forcing update")
        else:
            force = False
        if self.sysstatecheck():
            has_changed = True
        if self.remotecheck():
            has_changed = True
        if self.syncsyncstat():
            has_changed = True
        if self.bandwidth():
            has_changed = True
        if self.drivespin():
            has_changed = True
        if self.sostocheck():
            has_changed = True
        if self.diskspace(force):
            has_changed = True
        if has_changed:
            self.syncstat.date = round(time.time())
            self.syncstat.syncstat_status = SyncstatStatus.AVAILABLE
            soshelpers.write_to_file(sosconstants.SYNCSTAT_CACHE, self.syncstat.to_json())
        return has_changed

    def sostocheck(self):
        """
        gets the lsyncinfo from the sosrsyncwrapper script, which outputs it's results in a cache file
        :return: True if data has changed
        """
        has_changed = False
        if os.path.isfile(sosconstants.SOSTOCHECK_CACHE):
            if round(os.stat(sosconstants.SOSTOCHECK_CACHE).st_mtime) > self.last_sostocheck_read:
                with open(sosconstants.SOSTOCHECK_CACHE) as json_file:
                    try:
                        sostocheck = SosToCheck.from_json(json_file.read())
                        self.syncstat.to_check = sostocheck.to_check
                        # anonymize the uuid
                        if self.oldakt_file != sostocheck.akt_file:
                            self.syncstat.akt_file = uuid.uuid4()
                            self.oldakt_file = sostocheck.akt_file
                        self.syncstat.akt_file = sostocheck.akt_file
                        self.syncstat.percent = sostocheck.percent
                        has_changed = True
                        self.last_sostocheck_read = round(os.stat(sosconstants.SOSTOCHECK_CACHE).st_mtime)
                    except:
                        logger.log(logging.WARNING, f"Could not decode cache file {sosconstants.SOSTOCHECK_CACHE}")
        return has_changed

    def diskspace(self, force=False):
        """
        gets used space of mounted (aka hdd) volumes, but only checks if volumes are in use,
        to avoid spin up of drives.
        :param force: spin up drives, if necessary (normally for first check only)
        :return: True if dis
        """
        has_changed = False
        if (not os.system(
                f"fuser -s -f {sosconstants.MOUNT[sos_enums.Partition.LOCAL]} "
                f"{sosconstants.MOUNT[sos_enums.Partition.REMOTE]}"
                f" {sosconstants.MOUNT[sos_enums.Partition.SYSTEM]}")) \
                or force:
            statvfs = os.statvfs(sosconstants.MOUNT[sos_enums.Partition.LOCAL])
            new_size = statvfs.f_frsize * statvfs.f_blocks
            if new_size != self.syncstat.total_size:
                self.syncstat.total_size = new_size
                has_changed = True
            new_free_space = statvfs.f_bfree * statvfs.f_frsize
            if new_free_space != self.syncstat.free_space:
                self.syncstat.free_space = new_free_space
                has_changed = True
            new_iused = statvfs.f_files - statvfs.f_ffree
            if new_iused != self.syncstat.iused:
                self.syncstat.iused = new_iused
                has_changed = True
        return has_changed

    def drivespin(self):
        """
        gets spin status of all drives
        :return: True if spin status has changed
        """
        has_changed = False
        for drive in self.drivestatus.get_drivelist():
            if soshelpers.check_drive(drive):
                if drive in self.syncstat.driveup:
                    logger.log(logging.INFO, f"drive spin down: {drive}")
                    has_changed = True
                    self.syncstat.driveup.remove(drive)
            else:
                if drive not in self.syncstat.driveup:
                    logger.log(logging.INFO, f"drive spin up: {drive}")
                    has_changed = True
                    self.syncstat.driveup.append(drive)
        return has_changed

    def bandwidth(self):
        """
        update bandwidth information
        :return: True if bandwidth has changed
        """
        tx = 0
        rx = 0
        # get data from all nics
        for nic in self.aktsystemdescription.nics:
            try:
                with open('/sys/class/net/' + nic + '/statistics/tx_bytes') as f:
                    tx += int(f.read())
            except:
                tx += 0
            try:
                with open('/sys/class/net/' + nic + '/statistics/rx_bytes') as f:
                    rx += int(f.read())
            except:
                rx += 0
        # calculate the bandwidth
        nowtime = round(time.time())
        timerange = nowtime - self.oldtime
        self.oldtime = nowtime
        if timerange <= 0:
            self.syncstat.bw_down = 0
            self.syncstat.bw_up = 0
        else:
            self.syncstat.bw_up = round(((tx - self.oldtx) * 8 / timerange) / 1000000, 2)
            self.syncstat.bw_down = round(((rx - self.oldrx) * 8 / timerange) / 1000000, 2)
        has_changed = False
        # ok, store this tuple also in the bw history
        bw_info = BandWidthModel(self.syncstat.bw_up, self.syncstat.bw_down, nowtime)
        if len(self.bw_buffer) >= sosconstants.BW_BUFFER_SIZE:
            self.bw_buffer.pop(0)
        self.bw_buffer.append(bw_info)
        if self.oldbw_down != self.syncstat.bw_down or self.oldbw_up != self.syncstat.bw_up:
            self.oldbw_down = self.syncstat.bw_down
            self.oldbw_up = self.syncstat.bw_up
            has_changed = True
        self.oldtx = tx
        self.oldrx = rx
        return has_changed

    def remotecheck(self):
        """
        checks in time boundaries from sosconstants, if remote host is reachable.
        Note: this is the only area, where lsyncd is started or stopped
        :return: True if status has changed
        """
        akttime = round(time.time())
        has_changed = False
        self.remotehostip = ""

        # first check, if the remote host is available at all and reachable and so on
        # check the remote host only, if there is a reason to do so
        # logger.debug(f"Remotecheck with mode {self.syncstat.actmode} and remote status {self.old_remote_up} time left if good: {akttime - (self.last_checked+sosconstants.CHECK_TIME_TRUE)} bad: {akttime - (self.last_checked+sosconstants.CHECK_TIME_FALSE)}")
        if self.syncstat.actmode == SystemMode.DEFAULT or self.syncstat.actmode == SystemMode.REMOTE_RECOVERY:
            if (
                    self.last_checked + sosconstants.CHECK_TIME_TRUE < akttime and self.old_remote_up == RemoteUpStatus.UP) or (
                    self.last_checked + sosconstants.CHECK_TIME_FALSE < akttime and self.old_remote_up != RemoteUpStatus.UP):
                self.last_checked = akttime
                self.syncstat.remote_up = self.remotehostconfig.check_host()
                try:
                    self.remotehostip = socket.gethostbyname(self.mylsyncd.hostname)
                except Exception as error:
                    logger.error(f"Could not resolve host {self.mylsyncd.hostname}: {str(error)}")
                logger.debug(
                    f"Checked remotehost {self.mylsyncd.hostname} ({self.remotehostip}) with result {self.syncstat.remote_up}")
        else:
            # That's the only thing we need, when de do not need to check the remote host.
            self.syncstat.remote_up = RemoteUpStatus.NOT_AVAILABLE

        # check if remote state has changed and change the timers
        if self.syncstat.remote_up != self.old_remote_up:
            logger.info(f"remote check status to {self.mylsyncd.hostname} has changed to {self.syncstat.remote_up}")
            has_changed = True
            self.old_remote_up = self.syncstat.remote_up
            if self.syncstat.remote_up != RemoteUpStatus.UP:
                self.syncstat.remote_down_since = round(time.time())

        # now process lsyncd
        if self.syncstat.actmode == SystemMode.DEFAULT and self.syncstat.remote_up == RemoteUpStatus.UP:
            # we process the state, even if it is unchanged, so we are sure, lsyncd is in the right state
            soshelpers.set_hw_outputs(self.aktconfig, self.syncstat.remote_up)
            if os.system('systemctl is-active --quiet lsyncd') != 0:
                logger.info(f"lsyncd will be started")
                os.system('service lsyncd start')
            elif self.oldremotehostip != "" and self.remotehostip != self.oldremotehostip:
                logger.info(f"IP address has changed, lsyncd will be restarted")
                os.system('service lsyncd restart')
        else:
            # in all other cases lsyncd will be stopped.
            if os.system('systemctl is-active --quiet lsyncd') == 0:
                logger.info(f"lsyncd will be stopped")
                os.system('service lsyncd stop')

        return has_changed

    def syncsyncstat(self):
        """
        tries to find out if there is any incoming or outgoing sync
        :return: True if status has changed
        """
        has_changed = False
        if self.syncstat.remote_up == RemoteUpStatus.UP:
            # now check if the last lsync action has finished, this is magic :-)
            if glob.glob('/var/log/user*') and os.system('zgrep -h lsyncd $(ls -1r /var/log/user*) | tail -1 | grep '
                                                         '-q -i finished'):
                self.syncstat.syncup = SyncStatus.ACTIVE
            else:
                self.syncstat.syncup = SyncStatus.IDLE
            # check if there is any active download, we do this by checking, if there is rsync running
            # on syncosync's user id
            try:
                self.syncstat.syncdown = SyncStatus.IDLE
                pwd.getpwnam('syncosync')
                if not os.system('ps -u syncosync -U syncosync | grep -q rsync'):
                    self.syncstat.syncdown = SyncStatus.ACTIVE
            except KeyError:
                logger.log(logging.CRITICAL, "There is no syncosync user in the system!?")
        else:
            self.syncstat.syncup = SyncStatus.NOT_AVAILABLE
            self.syncstat.syncdown = SyncStatus.NOT_AVAILABLE
        if self.old_syncup != self.syncstat.syncup:
            has_changed = True
            self.syncstat.syncup_change_since = round(time.time())
            self.old_syncup = self.syncstat.syncup
        if self.old_syncdown != self.syncstat.syncdown:
            has_changed = True
            self.syncstat.syncdown_change_since = round(time.time())
            self.old_syncdown = self.syncstat.syncdown
        return has_changed

    def sysstatecheck(self):
        """
        gets the actual system state from the cache file
        :return: True if status has changed
        """
        if os.path.isfile(sosconstants.ACTSTATE_FILE) \
                and round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime) > self._last_sysstate:
            with open(sosconstants.ACTSTATE_FILE) as json_file:
                try:
                    thissysstate = SysStateModel()
                    content = json_file.read()
                    thissysstate.update_from_json(content)
                except:
                    pass
            self._last_sysstate = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            self.syncstat.actmode = thissysstate.actmode
            self.syncstat.sysstate = thissysstate.sysstate
            return True
        return False

    def get(self):
        """
        the standard getter.
        :return: SyncStatModel object
        """
        return self.syncstat

    def get_bw_history(self):
        return self.bw_buffer


class SyncStatReader:
    """
    For clients which will just present the actual status from cached data. Dunno if there is any need for it, but it
    does not cost so much performance to write out the status as a json in tmp
    """

    def __init__(self):
        self.syncstat = SyncStatModel()
        self.last_syncstat_read = 0

    def update_data(self):
        has_changed = False
        if os.path.isfile(sosconstants.SYNCSTAT_CACHE):
            if round(os.stat(sosconstants.SYNCSTAT_CACHE).st_mtime) > self.last_syncstat_read:
                with open(sosconstants.SYNCSTAT_CACHE) as json_file:
                    mysyncstat = SyncStatModel.from_json(json_file.read())
                    self.syncstat = mysyncstat
                    self.last_syncstat_read = round(os.stat(sosconstants.SYNCSTAT_CACHE).st_mtime)
                    has_changed = True
        else:
            self.syncstat.syncstat_status = SyncstatStatus.NOT_AVAILABLE
        return has_changed

    def get(self):
        """
        the standard getter.
        :return: SyncStatModel object
        """
        return self.syncstat

