"""
checking which state the system is in
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import os
import shutil
import subprocess

import soscore.sosaccounts
from soscore import drivemanager, configbackup, soskey, remotehost, sshdmanager, soshelpers
from sosmodel import sosconstants, sos_enums
from sosmodel.sos_enums import SystemStatus, SystemMode, Sshd, SshdAccess, VolMount
from sosmodel.sysstate import SysStateModel, SysModeModel
import logging

logger = logging.getLogger(__name__)


def read_mode_from_file(filepath) -> SysModeModel:
    if os.path.isfile(filepath):
        with open(filepath) as json_file:
            my_mode = SysModeModel.from_json(json_file.read())
            return my_mode
    else:
        my_mode = SysModeModel(SystemMode.DEFAULT)
        return my_mode


class SysState:

    def __init__(self):
        if os.path.isfile(sosconstants.ACTSTATE_FILE):
            with open(sosconstants.ACTSTATE_FILE) as json_file:
                self.sysstate = SysStateModel.from_json(json_file.read())
        else:
            self.sysstate = SysStateModel(SystemStatus.NOTHING, SystemMode.STARTUP, SystemMode.STARTUP)

        my_storedmode = read_mode_from_file(sosconstants.STOREDMODE_FILE)
        self.sysstate.storedmode = my_storedmode.mode
        self.mydrives = drivemanager.Drives()
        self.mycfg_backup = configbackup.ConfigBackup()
        self.myconfigbackup = None
        self.mysoskey = soskey.SosKey()
        self.mysoskeykey = None
        self.myremotehost = remotehost.RemoteHost()
        self.myremotehostoutput = None
        self.myintrasshdstatus = None
        self.myextrasshdstatus = None
        self.last_update = 0
        self.update_sysstate()

    def update_sysstate(self):
        """
        updates sysstate by checking all possible stuff
        :return: True if state has changed, False else
        """
        newstate = SysStateModel()
        newstate.sysstate = SystemStatus.NOTHING
        # we do not check the modes here
        newstate.storedmode = self.sysstate.storedmode
        newstate.actmode = self.sysstate.actmode
        self.mydrives.generate()
        self.myconfigbackup = self.mycfg_backup.get()
        self.mysoskeykey = self.mysoskey.get()
        self.myremotehostoutput = self.myremotehost.get()
        self.myintrasshdstatus = sshdmanager.get(Sshd.INTRANET)
        self.myextrasshdstatus = sshdmanager.get(Sshd.EXTRANET)
        # now analyze the situation
        if self.mydrives.sos_vol_mount == VolMount.NONE or self.mydrives.sos_vol_mount == VolMount.INCOMPLETE:
            newstate.sysstate = newstate.sysstate + SystemStatus.SOS_VOLUMES_NOT_MOUNTED
        if self.mydrives.non_sos_drive or self.mydrives.vgs.bad or self.mydrives.vgs.multiple_sos \
                or not self.mydrives.vgs.sos_found \
                or (self.mydrives.vgs.sos_found and not self.mydrives.lvs.active):
            newstate.sysstate = newstate.sysstate + SystemStatus.DISK_SETUP_NEED
        if self.myconfigbackup.md5sum == "":
            newstate.sysstate = newstate.sysstate + SystemStatus.SOS_CONF_BACKUP_NOT_DONE
        else:
            if self.mydrives.vgs.cfgmd5sum != "" and (self.myconfigbackup.md5sum != self.mydrives.vgs.cfgmd5sum):
                logger.debug(f"config md5sum: {self.myconfigbackup.md5sum} vs. {self.mydrives.vgs.cfgmd5sum} on drive")
                newstate.sysstate = newstate.sysstate + SystemStatus.SOS_CONF_BACKUP_NOT_MATCH
        if self.mysoskeykey.fingerprint is None:
            newstate.sysstate = newstate.sysstate + SystemStatus.SOS_KEY_NOT_GENERATED
        if self.myremotehostoutput.hostname == "":
            newstate.sysstate = newstate.sysstate + SystemStatus.REMOTE_HOST_NOT_CONFIGURED
        if self.myintrasshdstatus.pid == -1:
            newstate.sysstate = newstate.sysstate + SystemStatus.INTRANET_SSHD_DOWN
        else:
            if self.myintrasshdstatus.access == SshdAccess.CLOSE:
                newstate.sysstate = newstate.sysstate + SystemStatus.INTRANET_SSHD_CLOSE
        if self.myextrasshdstatus.pid == -1:
            newstate.sysstate = newstate.sysstate + SystemStatus.EXTRANET_SSHD_DOWN
        else:
            if self.myextrasshdstatus.access == SshdAccess.CLOSE:
                newstate.sysstate = newstate.sysstate + SystemStatus.EXTRANET_SSHD_CLOSE
        if newstate != self.sysstate:
            self.sysstate = newstate
            soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
            self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            return True
        else:
            return False

    def get(self):
        if not os.path.isfile(sosconstants.ACTSTATE_FILE):
            self.update_sysstate()
        elif round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime) > self.last_update:
            with open(sosconstants.ACTSTATE_FILE) as json_file:
                try:
                    content = json_file.read()
                    self.sysstate.update_from_json(content)
                except:
                    pass
            self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
        return self.sysstate

    def resetmode(self) -> SystemMode:
        """
        starts the stored mode (or default, if there was no stored mode)
        :return: The mode, which has been started (as an SysModeModel object)
        """
        my_storedmode = read_mode_from_file(sosconstants.STOREDMODE_FILE)
        trymode = my_storedmode.mode
        logger.info(f"Try to reset mode to {SystemMode.message(trymode)}")
        result, report = self.changemode(mode=trymode)
        if result:
            return trymode
        else:
            return SystemMode.SETUP

    def changemode(self, mode: SystemMode, cfg_backup_filepath: str = "", config_pwd: str = "") -> (bool, list):
        """
        This is basically a FSM. If a new mode is applicable to set, this will happen
        :param mode: New Mode
        :param cfg_backup_filepath: path to configuration backup (will be needed for some modes)
        :param config_pwd: configuration backup password (will be needed for some modes)
        :return: True if mode change was successful
        """
        report = []
        logger.debug(f"Switching to mode \"{SystemMode.message(mode)}\" has been requested")
        if mode == SystemMode.SETUP:
            # this mode is to force the system for setup
            # just be sure everything is fine for setup
            # set the sshds according
            sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
            drivemanager.umount_sos_volumes()
            # at this point it may be the easiest thing to delete the stored mode
            if os.path.isfile(sosconstants.STOREDMODE_FILE):
                os.remove(sosconstants.STOREDMODE_FILE)
            # Save the state, if it has changed
            self.sysstate.actmode = SystemMode.SETUP
            soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
            self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            return True, report

        elif mode == SystemMode.DEFAULT:
            logger.debug("Default mode requested")
            if (self.sysstate.sysstate & SystemStatus.DISK_SETUP_NEED) \
                    or (self.sysstate.storedmode != SystemMode.DISASTER_SWAP
                        and self.sysstate.sysstate & SystemStatus.SOS_CONF_BACKUP_NOT_MATCH):
                logger.debug(f"Stop switching as disk setup is needed")
                report.append(f"Could not switch to mode \"{SystemMode.message(mode)}\": Disk setup needed")
                # set the sshds according
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
                sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.CLOSE)
                # Save the state, if it has changed
                self.sysstate.actmode = SystemMode.SETUP
                soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
                self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
                return False, report
            if self.sysstate.storedmode == SystemMode.DISASTER_SWAP:
                # we have to restore the stored configuration, dunno what to do if there is no such thing?
                # we also assume, that we get the cfg_backup_filepath from the caller - as it should be found out
                # that the password is right
                if config_pwd is None or config_pwd == "" or cfg_backup_filepath is None or cfg_backup_filepath == "":
                    logger.error("requested switch to default mode with backup restore with no or empty password"
                                 "or empty file path")
                    report.append("Requested switch to default mode with backup restore with no or empty password"
                                  "or empty file path")
                    return False, report
                # Close all sshd access for this time
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
                sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.CLOSE)
                # delete all sosaccounts from the system without deleting their homes as these are the accounts from
                # the swapped system
                soscore.sosaccounts.delete_all_accounts()
                logger.debug("Switch from Disaster Swap to Default mode requested")
            if cfg_backup_filepath is not None and cfg_backup_filepath != "":
                # ok this is for any config restore - either return from disaster swap or config restore as an action
                src_dir = configbackup.unpack(cfg_backup_filepath, config_pwd)
                if src_dir == "":
                    logger.error(f"cannot decrypt {cfg_backup_filepath}")
                    report.append(f"Can not decrypt {cfg_backup_filepath}, wrong password?")
                    return False, report
                # Close all sshd access for this time
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
                sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.CLOSE)
                # mount the drives in the usual way
                self.mydrives.mount_sos_volumes()
                success, cfg_bu_report = configbackup.restore_from_dir(src_dir)
                report.extend(cfg_bu_report)
                if not success:
                    logger.error("Was not able to restore configuration for default mode")
                    report.append("Was not able to restore configuration")
                    # right now, we switch to Setup mode, as we should be able to fix everything from there
                    # Save the state, if it has changed
                    self.sysstate.actmode = SystemMode.SETUP
                    soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
                    self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
                    return False, report
                else:
                    # ownership per account should have been already fixed in configbackup.restore
                    # still we have to look for lost+found, syncosync and .
                    drivemanager.set_local_ownership()
                    drivemanager.set_remote_ownership()
                    logger.info(f"Successful default state configuration restore: {cfg_backup_filepath}")
                    report.append("Configuration restore for default state successful")
                    # update the sysstate again, so we are sure everything is up to date
                    self.update_sysstate()
                # remove the unpacked dir finally
                # shutil.rmtree(src_dir)
            if self.sysstate.sysstate & SystemStatus.SOS_VOLUMES_NOT_MOUNTED:
                self.mydrives.mount_sos_volumes()
                self.sysstate.sysstate = self.sysstate.sysstate - SystemStatus.SOS_VOLUMES_NOT_MOUNTED
            if self.sysstate.sysstate & SystemStatus.SOS_CONF_BACKUP_NOT_DONE:
                logger.info("First time configuration backup while starting default state")
                self.mycfg_backup.backup()
                self.sysstate.sysstate = self.sysstate.sysstate - SystemStatus.SOS_CONF_BACKUP_NOT_DONE
            if self.sysstate.sysstate & SystemStatus.INTRANET_SSHD_DOWN:
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OPEN)
                self.sysstate.sysstate = self.sysstate.sysstate - SystemStatus.INTRANET_SSHD_DOWN
            if self.sysstate.sysstate & SystemStatus.INTRANET_SSHD_CLOSE:
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OPEN)
                self.sysstate.sysstate = self.sysstate.sysstate - SystemStatus.INTRANET_SSHD_CLOSE
            if not (self.sysstate.sysstate & SystemStatus.REMOTE_HOST_NOT_CONFIGURED):
                if self.sysstate.sysstate & SystemStatus.EXTRANET_SSHD_DOWN:
                    sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OPEN)
                    self.sysstate.sysstate = self.sysstate.sysstate - SystemStatus.EXTRANET_SSHD_DOWN
                if self.sysstate.sysstate & SystemStatus.EXTRANET_SSHD_CLOSE:
                    sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OPEN)
                    self.sysstate.sysstate = self.sysstate.sysstate - SystemStatus.EXTRANET_SSHD_CLOSE
            subprocess.call(["service", "sosaccountaccess", "start"])
            # Save the state, if it has changed
            self.sysstate.actmode = SystemMode.DEFAULT
            if self.sysstate.storedmode != SystemMode.DEFAULT:
                modeset = SysModeModel(SystemMode.DEFAULT)
                soshelpers.write_to_file(sosconstants.STOREDMODE_FILE, modeset.to_json())
                self.sysstate.storedmode = SystemMode.DEFAULT
            soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
            self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            return True, report

        elif mode == SystemMode.DISASTER_SWAP:
            logger.debug("Disaster Swap mode requested")
            if (self.sysstate.sysstate & SystemStatus.DISK_SETUP_NEED) \
                    or (self.sysstate.sysstate & SystemStatus.SOS_CONF_BACKUP_NOT_MATCH):
                logger.debug(f"Stop switching as disk setup is needed")
                report.append(f"Could not switch to mode \"{SystemMode.message(mode)}\": Disk setup needed")
                # set the sshds according
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
                sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.CLOSE)
                # Save the state, if it has changed
                self.sysstate.actmode = SystemMode.SETUP
                soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
                self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
                return False, report
            if self.sysstate.storedmode != SystemMode.DISASTER_SWAP:
                # This whole procedure only has to start, if we enter Disaster Swap State the first time
                if config_pwd is None or config_pwd == "" or cfg_backup_filepath is None or cfg_backup_filepath == "":
                    logger.error("requested switch to swap mode with backup restore with no or empty password"
                                 "or empty file path")
                    report.append("Requested switch to swap mode with backup restore with no or empty password"
                                  "or empty file path")
                    return False, report
                src_dir = configbackup.unpack(cfg_backup_filepath, config_pwd)
                if src_dir == "":
                    logger.error(f"cannot decrypt {cfg_backup_filepath}")
                    report.append(f"Can not decrypt {cfg_backup_filepath}, wrong password?")
                    return False, report
                logger.debug(f"unpacked {cfg_backup_filepath} to {src_dir}")
                # ok, we can decrypt the configbackup, so let's go
                # Close all sshd access for this time
                sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
                sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.CLOSE)
                # delete all sosaccounts from the system without deleting their homes
                soscore.sosaccounts.delete_all_accounts()
                # swap the drives, first the local part writeable to reset the ownership
                self.mydrives.mount_sos_volumes(swap=True, local_ro=False, remote_ro=True)
                success, cfg_bu_report = configbackup.restore_from_dir(src_dir)
                report.extend(cfg_bu_report)
                # remove the unpacked dir finally
                # shutil.rmtree(src_dir)
                if not success:
                    logger.error("Was not able to restore configuration for disaster swap"
                                 ", switching back to default mode")
                    report.extend(cfg_bu_report)
                    report.append("Was not able to restore configuration, switching back to default mode")
                    # at this point, we have to restore the actual configuration and switch back the volumes.
                    # We go to setup. This should be the best solution
                    self.sysstate.actmode = SystemMode.SETUP
                    soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
                    self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
                    return False, report
                else:
                    # ownership should have been already fixed in configbackup.restore
                    drivemanager.set_local_ownership()
                    logger.info(f"Successful Disaster Swap activation with configuration: {cfg_backup_filepath}")
                    report.append("Configuration restore for Disaster Swap successful")
            # This part is also running, when Disaster Swap is reactivated (after reboot)
            # mount the drives swapped, but this time only readable
            self.mydrives.mount_sos_volumes(swap=True, local_ro=True, remote_ro=True)
            # open the Intranet Port, so accounts could get access to their data
            sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OPEN)
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.CLOSE)
            # Save the state, if it has changed
            self.sysstate.actmode = SystemMode.DISASTER_SWAP
            if self.sysstate.storedmode != SystemMode.DISASTER_SWAP:
                modeset = SysModeModel(SystemMode.DISASTER_SWAP)
                soshelpers.write_to_file(sosconstants.STOREDMODE_FILE, modeset.to_json())
                self.sysstate.storedmode = SystemMode.DISASTER_SWAP
            soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
            self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            return True, report

        elif mode == SystemMode.SHUTDOWN:
            # set the sshds according
            sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OFF)
            sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
            drivemanager.umount_sos_volumes()
            # Save the state, if it has changed
            self.sysstate.actmode = SystemMode.SHUTDOWN
            soshelpers.write_to_file(sosconstants.ACTSTATE_FILE, self.sysstate.to_json())
            self.last_update = round(os.stat(sosconstants.ACTSTATE_FILE).st_mtime)
            return True, report
        else:
            logger.error(f"Requested to switch to unimplemented mode {SystemMode.message(mode)}")
            # in every case, open sshd for admin access
            sshdmanager.change_access(Sshd.INTRANET, SshdAccess.CLOSE)
            return False, report
