"""
configbackup tests
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from soscore import soshelpers


class TestSosHelpers(unittest.TestCase):

    def test_str2base64(self):
        # a password hash with salt from /etc/shadow
        inpstr = "$6$dvo/ndw7e35wbVST$rx4C6WJAnqHRcgwqkN3VxvnSBD8kRY54r1V184/3K4Tnb4Jhjgp8jRbCa6Nx2TjqVBLayOCXcmyrNgqAzjsvB/"
        output = soshelpers.str2base64(inpstr)
        print("output:", output)
        resstr = soshelpers.base642str(output)
        print("resstr:", resstr)
        assert resstr == inpstr


if __name__ == '__main__':
    unittest.main()
