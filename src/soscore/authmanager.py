"""
Authentication Manager for login
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import base64
import crypt
import json
import logging
import os
import spwd
from typing import Optional
import time

import cryptography
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

from soscore import soshelpers

logger = logging.getLogger(__name__)


class AuthManager:
    def __init__(self):
        # TODO: Replace/Dynamically read pw
        self.__admin_password_hash: str = ""
        self.__admin_password_salt: str = ""
        self.__read_password()
        self.__fernet: Fernet = self.__setup_fernet()

    def __issue_token(self) -> str:
        # since we do not want to store data on the box, we want to set cookies accordingly
        # What data do we store?
        # Timestamp of login -> After say 24h, a new login is required (validity)
        new_token_plain = {
            "issued_at": int(time.time())
        }

        return self.__fernet.encrypt(str.encode(json.dumps(new_token_plain))).decode()

    def validate_token(self, token: str) -> bool:
        """
        :returns: True if token is still valid, False otherwise
        """
        # TODO
        try:
            decoded_raw = self.__fernet.decrypt(token.encode()).decode()
        except cryptography.fernet.InvalidToken:
            logger.warning("Token could not be decrypted. A SOS restart or admin PW change may have occurred.")
            return False
        token_plain = json.loads(decoded_raw)
        token_issued_at = token_plain.get("issued_at", None)
        if token_issued_at is None or token_issued_at < time.time() - 24 * 60 * 60:
            return False
        else:
            return True

    def __read_password(self) -> None:
        # TODO: only update with a timestamp/interval
        mypwdict = soshelpers.parse_shadowpwd_data(spwd.getspnam("sosadmin")[1])
        if mypwdict is None or 'pwhash' not in mypwdict or 'salt' not in mypwdict:
            logger.log(logging.ERROR, f"sosadmin passwordhash has no pwhash!? Aborting.")
            self.__admin_password_hash = ""
            self.__admin_password_salt = ""
        self.__admin_password_hash = mypwdict['salt'] + '$' + mypwdict['pwhash']
        self.__admin_password_salt = mypwdict['salt']

    def __setup_fernet(self) -> Fernet:
        # Encrypt the token with the pw hash.
        # If we cannot decrypt it the next time we see the token, the PW has most likely changed
        # Additionally, a random salt will force fresh logins in case SOS is restarted
        backend = default_backend()
        salt = os.urandom(16)

        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=backend
        )
        key = base64.urlsafe_b64encode(kdf.derive(str.encode(self.__admin_password_hash)))

        return Fernet(key)

    def get_hash(self, password: str) -> str:
        return crypt.crypt(password, self.__admin_password_salt)

    def login(self, password: str) -> Optional[str]:
        self.__read_password()
        if self.get_hash(password) == self.__admin_password_hash:
            return self.__issue_token()
        else:
            return None
