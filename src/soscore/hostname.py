"""
About managing the hostname
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import subprocess

from soscore import configbackup
from sosmodel.hostname import HostNameModel
import logging
import socket

logger = logging.getLogger(__name__)


class HostName:
    """
    About getting and setting the systems hostname
    """

    def __init__(self):
        self.hostname = HostNameModel()
        self.hostname.set(socket.gethostname())

    def get(self):
        return self.hostname

    def set(self, new_hostname):
        logger.debug(f"new_hostname:{new_hostname.hostname} old_hostname:{self.hostname.hostname}")
        if new_hostname.hostname != self.hostname.hostname:
            self.hostname.hostname = new_hostname.hostname
            try:
                subprocess.run(f"hostnamectl set-hostname {self.hostname.hostname}", shell=True)
            except:
                logger.error(f"Was not able to set hostname to {self.hostname}")
            my_configbackup = configbackup.ConfigBackup()
            my_configbackup.backup()
