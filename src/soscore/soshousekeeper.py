"""
This is the sos housekeeper
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import logging
import json
import os
from soscore import hostname
from soscore import sosaccounts
from soscore import sosconfig
from sosmodel import sosconstants
from soscore import remotehost
from sosmodel.sos_enums import RemoteUpStatus
from soscore.mail import send_mail
from soscore.mail_content_gen import get_mail_hl, get_mail_msg, get_subject, get_html_mail
from sosmodel.soshousekeeper import HousekeeperModel


my_logging = logging.getLogger(__name__)


class Housekeeper:
    """Housekeeper class to handle housekeeper tasks

    The main functions provided are checking and notifying accounts if they should
    back up again, alerting the admin if the remote host is not reachable and
    sending the admin an email that contains the past events since the last
    mail, for example if any users were notified to do an overdue backup"""

    def __init__(self):
        self.values = HousekeeperModel()
        self.config = sosconfig.SosConfig().get()
        self.hostname = hostname.HostName().get().hostname
        self.mail_from = 'admin@' + self.hostname
        self.admin_info_period = self.config.mail_settings.admin_info_period
        self.admin_mail_address = self.config.mail_settings.admin_mail_address
        self.dictionary = {"HOSTNAME": "", "ACCOUNT_NAME": "", "OVERDUE_DAYS": "", "OVERDUE_HOURS": "",
                           "LAST_ACCESS_DATE": "",
                           "LAST_ACCESS_TIME": "", "BACKUP_OVERDUE": "", "RH_DOWN": "", "LSYNCD_HOST": "",
                           "LSYNCD_PORT": "",
                           "REMOTE_DOWN_DATE": "", "REMOTE_DOWN_TIME": ""}

        # Last seen is set to the time of initialization to prevent an to early mail if the system was down for example
        self.rm_last_seen = time.time()

        my_logging.log(logging.DEBUG,
                       'Init admin values: sender {} mail_address: {} mail_info_period: '.format(self.mail_from, self.admin_mail_address, str(self.admin_info_period)))


    def check_accounts(self):
        """This function checks if any accounts should be reminded to back up and notifies them if they should.

        It iterates through all accounts and checks last access times and if the user should be alerted with
        the current state of the system. If the user should get a notification mail, the admin will also be
        informed with his next status mail.

        :rtype: None"""

        my_accounts = sosaccounts.SosAccounts()
        for account in my_accounts.get():

            account_back_up_period = int(account.backup_period)
            account_info_period = int(account.info_period)
            account_remind_duration = int(account.remind_duration)
            account.last_mail = self.get_last_mail(account.name)

            my_logging.log(logging.DEBUG,
                           'checking: {}, mail_address: {}, mail_info_level: {}, info_period: {}, last mail: {}'.format(account.name, account.mail_address, str(account.mail_info_level), account_info_period, str(time.strftime('%d/%m/%Y at %H:%M:%S', time.localtime(account.last_mail)))))

            now = time.time()
            overdue_days = self.in_days(now - account.last_access)
            overdue_hours = int(time.strftime('%H',time.localtime(now - account.last_access)))

            my_logging.log(logging.DEBUG, 'overdue for {}: {} days and {} hours'.format(account.name, str(overdue_days), str(overdue_hours)))
            # If remind duration is set to 0 no notifications are sent
            if account_remind_duration != 0:
                if overdue_days >= account_back_up_period:
                    # The last backup has not been performed on time

                    if overdue_days >= account_remind_duration:
                        #To prevent spamming set the info period to a minimum of 1
                        if account_info_period <= 1:
                            account_info_period = 1
                        my_logging.log(logging.INFO, "user {} has backup overdue {} days {} hours".format(account.name,
                                                                                                          str(
                                                                                                              overdue_days),
                                                                                                          str(
                                                                                                              overdue_hours)))
                        # Set user mail
                        self.dictionary["ACCOUNT_NAME"] = account.name
                        self.dictionary["OVERDUE_DAYS"] = str(overdue_days)
                        self.dictionary["OVERDUE_HOURS"] = str(overdue_hours)
                        self.dictionary["LAST_ACCESS_DATE"] = time.strftime('%d/%m/%Y', time.localtime(account.last_access))
                        self.dictionary["LAST_ACCESS_TIME"] = time.strftime('%H:%M:%S', time.localtime(account.last_access))
                        self.dictionary["BACKUP_OVERDUE"] = str(overdue_days)

                        # Set admin mail (append new message)
                        # Only append if message is not already in admin mail
                        admin_msg = get_mail_msg('backup_overdue_admin_msg', self.config.ui_language, self.dictionary)
                        in_current_admin_msg = self.msg_in_admin_mail(account.name)
                        if not in_current_admin_msg:
                            self.values.admin_mail_append(admin_msg)
                        else:
                            my_logging.log(logging.INFO, "Current account notification is not appended because it is already in the current admin mail")

                        my_logging.log(logging.DEBUG,"for user {} there was a last mail on {} info_period is {}".format(account.name,time.strftime('%d/%m/%Y', time.localtime(account.last_mail)),  str(account_info_period)))

                        if self.in_days(now - account.last_mail) >= account_info_period:
                            # Set user mail
                            user_mail_content = get_mail_msg("backup_overdue_user_msg", self.config.ui_language,
                                                             self.dictionary)
                            user_mail_hl = get_mail_hl("backup_overdue_user_hl", self.config.ui_language)
                            user_subject = get_subject('backup_overdue_user_subject', self.config.ui_language,
                                                       self.hostname)
                            if account.mail_address:
                                # Now we can send a mail to the user
                                my_logging.log(logging.INFO, "sending mail to " + account.mail_address)
                                user_mail = get_html_mail(user_mail_hl, user_mail_content, self.hostname)
                                send_mail(self.mail_from, account.mail_address, user_subject, user_mail)
                                account.last_mail = time.time()
                                self.values.set_entry(sosconstants.LAST_MAIL_CACHE, account.name, account.last_mail)

                                my_logging.log(logging.DEBUG, "Mail: {} was sent to {}".format(user_mail, account.mail_address))
                            else:
                                if not in_current_admin_msg:
                                    self.values.admin_mail_append("Warning, the text above was not sent because the user has not set his email address!")
                                my_logging.log(logging.WARNING,
                                               "The user " + account.name + " has not set his mail address and can therefore not receive a reminder email")
                        else:
                            my_logging.log(logging.INFO, "Mail to {} will not be sent, because there was a mail sent on {}".format(account.name, time.strftime('%d/%m/%Y at %H:%M:%S', time.localtime(account.last_mail))))

    def remind_admin(self):
        """This function sends an eMail to the admin if there is any content and the admin info period is overdue"""


        #Set the info period to a minimum of 1 to prevent spamming
        if self.admin_info_period <= 1:
            self.admin_info_period = 1
        time_since_last_mail = self.in_days(time.time() - self.values.admin_last_mail)
        if time_since_last_mail >= self.admin_info_period:
            if self.admin_mail_address:
                # Send mail to admin now
                admin_subject = get_subject("admin_subject", self.config.ui_language, self.hostname)
                admin_headline = get_mail_hl("admin_headline", self.config.ui_language)

                if self.values.admin_mail_content:
                    mail_content = self.values.admin_mail_content
                else:
                    mail_content = get_mail_msg('admin_empty_mail', self.config.ui_language,{})

                admin_mail = get_html_mail(admin_headline, mail_content, self.hostname)
                if send_mail(self.mail_from, self.admin_mail_address, admin_subject, admin_mail):
                    self.values.admin_last_mail = time.time()
                    self.values.admin_mail_content = ""
                    my_logging.log(logging.DEBUG, "Sent Mail was: " + admin_mail)
                    my_logging.log(logging.INFO, "Sent mail to " + self.admin_mail_address)
            else:
                my_logging.log(logging.CRITICAL, "Admin mail was not set. Please set your admin mail immediately")
        else:
            my_logging.log(logging.INFO, "Admin will not be informed due to an eMail that was sent on {}".format(str(time.strftime('%d/%m/%Y at %H:%M:%S', time.localtime(self.values.admin_last_mail)))))

    def check_remotehost(self):
        """This function sends a warning eMail to the admin if the remote host is down"""

        rm_host = remotehost.RemoteHost()
        status = rm_host.check_host()
        if status == RemoteUpStatus.UP:
            self.rm_last_seen = time.time()
            # Reset the admin_last_down_mail, otherwise the admin only received mails every info period days
            self.values.admin_last_rmdown_mail = 0
        else:
            my_logging.log(logging.WARNING, "Remote host {} is not reachable: {}, last seen: {}, {}".format(rm_host.remotehost.hostname,RemoteUpStatus.message(status),time.strftime('%d/%m/%Y', time.localtime(self.rm_last_seen)),time.strftime('%H:%M:%S', time.localtime(self.rm_last_seen))))
            #Send mail if rmhost is down for longer then an hour
            if self.in_hours(time.time() - self.rm_last_seen) >= 1:
                if self.in_days(time.time() - self.values.admin_last_rmdown_mail) >= self.admin_info_period:
                    #Remind admin
                    self.dictionary["REMOTE_DOWN_DATE"] = time.strftime('%d/%m/%Y', time.localtime(self.rm_last_seen))
                    self.dictionary["REMOTE_DOWN_TIME"] = time.strftime('%H:%M:%S', time.localtime(self.rm_last_seen))
                    self.dictionary["LSYNCD_PORT"] = rm_host.remotehost.port
                    self.dictionary["LSYNCD_HOST"] = rm_host.remotehost.hostname
                    subject = get_subject("remote_host_down_admin_subject", self.config.ui_language, self.hostname)
                    mail_content = get_html_mail(get_mail_hl('remote_host_down_admin_hl', self.config.ui_language),get_mail_msg('remote_host_down_admin_msg', self.config.ui_language, self.dictionary), self.hostname)
                    send_mail(self.mail_from, self.admin_mail_address, subject, mail_content)
                    self.values.admin_last_rmdown_mail = time.time()
                    my_logging.log(logging.INFO, "Sent rmdown mail to {}".format(self.admin_mail_address))
                else:
                    my_logging.log(logging.DEBUG, "Admin will not be notified due to an eMail that was sent recently: {}, {}".format(time.strftime('%d/%m/%Y', time.localtime(self.values.admin_last_rmdown_mail)),time.strftime('%H:%M:%S', time.localtime(self.values.admin_last_rmdown_mail))))
            else:
                my_logging.log(logging.INFO, "Admin will not be notified because the remote host has not been down for an hour")

    def update(self):
        """Updates values of config and Houskeeper model
        
        :return: None
        :rtype: None"""# TODO set admin mail new (properly)

        self.values.update()
        self.config = sosconfig.SosConfig().get()

    def msg_in_admin_mail(self, account_name):
        """Checks if given account mail was already in the current admin mail (prevents
        the admin mail from containing the same message multiple times when the user has not set his mail for example)

        :param account_name: name of account
        :type account_name: str
        :return: True if message is already in admin mail
        :rtype: bool"""
        if self.values.admin_mail_content.find(account_name) == -1:
            return False
        else:
            return True

    def get_last_mail(self, account_name):
        """Method to get the time of the last mail of an account. Creates new entry for account if there currently isn't one

        :param account_name: name of account
        :type account_name: str
        :return: Time of last sent mail, 0 if a new entry was created
        :rtype: float"""
        last_mail = 0
        try:
            last_mail = self.values.get_entry(sosconstants.LAST_MAIL_CACHE, account_name)
        except KeyError:
            self.values.add_entry(sosconstants.LAST_MAIL_CACHE, account_name, 0)
        finally:
            return last_mail

    @staticmethod
    def in_days(value):
        """Returns giving value in days

        :param value: value being converted
        :type value: float
        :return: given float value in days
        :rtype: int
        """
        return int(value / (60*60*24))

    @staticmethod
    def in_hours(value):
        """Returns giving value in hours

        :param value: value being converted
        :type value: float
        :return: given float value in hours
        :rtype: int
        """
        return int(value / (60*60))

    def __str__(self):
        return 'Housekeeper( {},{},{},{},{},{},{},{},{})'.format(self.values.admin_last_rmdown_mail, self.values.admin_last_mail, self.values.admin_mail_content, self.values.remotehost_last_seen, self.config, self.hostname, self.mail_from, self.admin_info_period, self.admin_mail_address)


if __name__ == '__main__':
    logging.log(logging.INFO, "Housekeeper module was called as main, please use the according script to call it!")
