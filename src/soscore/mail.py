"""
Module to send mails
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import time
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from soscore.sosconfig import SosConfig
from soscore.hostname import HostName
from soscore.mail_content_gen import get_mail_hl,get_mail_msg,get_html_mail,get_subject

logger = logging.getLogger(__name__)


def send_mail(FROM, TO, SUBJECT, BODY):
    """This function sends an html email

    :param FROM: mail sender
    :type FROM: str
    :param TO: receiver of the mail
    :type TO: str
    :param SUBJECT: subject of the mail
    :type SUBJECT: str
    :param BODY: body of the mail (can be html)
    :type BODY: str
    :return: True is mail was sent, false if not
    :rtype: bool"""

    # Create message container
    message = MIMEMultipart('alternative')
    message['subject'] = SUBJECT
    message['To'] = TO
    message['From'] = FROM
    message.preamble = """Your mail reader does not support the report format."""

    html_body = MIMEText(BODY, 'html')
    message.attach(html_body)

    smtp_settings = SosConfig().get().mail_settings
    smtp_host = f'{smtp_settings.smtp_host}:{smtp_settings.smtp_port}'
    server = smtplib.SMTP(smtp_host)
    if __name__ == '__main__':
        server.set_debuglevel(1)

    server.starttls()
    password = smtp_settings.smtp_passwd
    sender = smtp_settings.smtp_user
    try:
        if len(password) > 0:
            server.login(sender, password)
        server.sendmail(FROM, [TO], message.as_string())
        logger.debug(f'Successfully sent the mail to {TO}')
        return True
    except smtplib.SMTPAuthenticationError:
        logger.error("An error occurred while trying to login into the server")
    finally:
        server.quit()
    # If we get here the mail was not send
    return False

def update_mail_config_from_json(new_mail_config):
    """Update the mail configuration form json

    :param new_mail_config: json with new values
    :type new_mail_config: str
    :return: True if writing to file succeeded, false if not
    :rtype: bool"""

    myconfig = SosConfig()
    mail_config = myconfig.get().mail_settings
    mail_config.update_from_json(new_mail_config)
    logger.debug("New mail settings:" + mail_config.to_json())
    return myconfig.set()


def send_test_mail(receiver):
    """Sends a test mail to the given receiver

    :param receiver: receiver of the testmail
    :type receiver: str
    :return: True if sent False if not
    :rtype: bool
    """
    config = SosConfig().get()
    admin_mail_address = config.mail_settings.admin_mail_address
    lang = config.ui_language.language
    hostname = HostName().get().hostname
    now = time.time()
    data = {'HOSTNAME': hostname, 'LAST_ACCESS_DATE': time.strftime('%d/%m/%Y', time.localtime(now)),
            'LAST_ACCESS_TIME': time.strftime('%H:%M:%S', time.localtime(now))}
    msg = get_mail_msg("testmail_content", lang, data)
    hl = get_mail_hl("testmail_hl", lang)
    body = get_html_mail(hl, msg, hostname)

    return send_mail(admin_mail_address, receiver, get_subject("testmail_subject", lang, hostname), body)


if __name__ == '__main__':
    print("Mail module was called as main. You should use the runscript to use the mail module!")