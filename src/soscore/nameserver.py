"""
For managing nameservers
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from soscore import configbackup
from sosmodel.nameserver import NameServerModel
import logging

my_logging = logging.getLogger(__name__)


class NameServer:
    """
    about getting and setting nameservers
    """

    def __init__(self):
        self.nameserver = NameServerModel()
        resolvers = []
        try:
            with open('/etc/resolv.conf', 'r') as resolvconf:
                for line in resolvconf.readlines():
                    line = line.split('#', 1)[0]
                    line = line.rstrip()
                    if 'nameserver' in line:
                        resolvers.append(line.split()[1])
        except IOError as error:
            my_logging.log(logging.ERROR, f"Could not analyse resolv.conf:{error}")
        if len(resolvers) >= 1 and resolvers[0]:
            self.nameserver.first_nameserver = resolvers[0]
        if len(resolvers) >= 2 and resolvers[1]:
            self.nameserver.second_nameserver = resolvers[1]
        if len(resolvers) >= 3 and resolvers[2]:
            self.nameserver.third_nameserver = resolvers[2]

    def get(self):
        return self.nameserver

    def set(self, new_nameserver):
        self.nameserver = new_nameserver
        output = []
        if self.nameserver.first_nameserver != '':
            output.append(f"nameserver {self.nameserver.first_nameserver}")
        if self.nameserver.second_nameserver != '':
            output.append(f"nameserver {self.nameserver.second_nameserver}")
        if self.nameserver.third_nameserver != '':
            output.append(f"nameserver {self.nameserver.third_nameserver}")
        try:
            with open("/etc/resolv.conf", "w") as outfile:
                outfile.write("\n".join(output))
                outfile.write("\n")
        except:
            my_logging.log(logging.ERROR, f"not able to output nameserver config to resolv.conf")
        my_configbackup = configbackup.ConfigBackup()
        my_configbackup.backup()
