#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import shutil
import time
import paramiko
import io
import socket
import logging
import signal

from threading import Event

from paramiko.sftp_server import SFTPServer
from paramiko.sftp_attr import SFTPAttributes
from paramiko.sftp_si import SFTPServerInterface
from paramiko.sftp_handle import SFTPHandle
from paramiko.sftp import SFTP_OP_UNSUPPORTED

from soscore import soserrors
from soscore import soshelpers
from sosmodel import sosconstants
from soscore import remotehost
from soscore import sshdmanager
from soscore import soskey
from soscore import systemsetupdata
from sosmodel.sos_enums import Sshd, SsdAction
from sosmodel.systemsetupdata import SystemSetupDataModel
from sosmodel.soskey import SosKeyModel

logger = logging.getLogger(__name__)
# FIXME:
logger.setLevel(logging.DEBUG)


class SosKeyExchange:
    """
    Class handling the receiving/sending/accepting of keys during key exchange as well as get/delete remote key
    """
    _ssd_in = b""
    follower_start_time = 0
    # receive_cancel_signal: Event = Event()
    receive_cancel_signal = False

    class SsdInFileHandle(SFTPHandle):
        def __init__(self, path, flags=0):
            super().__init__(flags)
            self.writefile = io.BytesIO(b"")
            self.readfile = self.writefile
            self.path = path

        def close(self):
            if self.writefile.tell() > 0:
                SosKeyExchange._ssd_in = self.writefile.getvalue()
            super().close()

        def stat(self):
            attr = SFTPAttributes()
            attr.st_mode = 644
            attr.st_uid = 0
            attr.st_gid = 0
            attr.st_size = 0
            return attr

    class SsdOutFileHandle(SFTPHandle):
        def __init__(self, path, flags=0):
            super().__init__(flags)
            # Get SSD data here
            myssd = systemsetupdata.get_system_ssd()
            myssd.action = SsdAction.INTERACTIVE_FOLLOWER
            ssd_json = bytes(myssd.to_json(), "UTF-8")
            self.len = len(ssd_json)
            self.readfile = io.BytesIO(ssd_json)
            self.path = path

        def close(self):
            super().close()

        def stat(self):
            attr = SFTPAttributes()
            attr.st_size = self.len
            attr.st_mode = 644
            attr.st_uid = 0
            attr.st_gid = 0
            attr.st_atime = 0
            attr.st_mtime = 0
            attr.filename = self.path
            return attr

    class SSHKeyxSI(SFTPServerInterface):
        def open(self, path, flags, attr):
            logger.debug(f"Open {path}")
            try:
                if path == sosconstants.SOS_KEYX_SSDPATH_IN:
                    logger.debug(f"Incoming file:{path}")
                    return SosKeyExchange.SsdInFileHandle(path, flags)
                elif path == sosconstants.SOS_KEYX_SSDPATH_OUT:
                    logger.debug(f"Outgoing file:{path}")
                    return SosKeyExchange.SsdOutFileHandle(path, flags)
                else:
                    logger.warning(f"Opening filename not supported: {path}")
                    return SFTP_OP_UNSUPPORTED

            except Exception as e:
                logger.error(f"In SSHKEyxSI: {e}")
                return SFTP_OP_UNSUPPORTED

        def stat(self, path):
            return SFTPAttributes()

    class SSHKeyxServer(paramiko.ServerInterface):
        def check_auth_publickey(self, username, key):
            # TODO: Check pubkey against own keyx fingerprint
            return paramiko.AUTH_SUCCESSFUL

        def get_allowed_auths(self, username):
            return "publickey"

        def check_channel_request(self, kind, chanid):
            # TODO: Only allow relevant channel to be opened?
            return paramiko.OPEN_SUCCEEDED

    @staticmethod
    def __accept_one_ssh_conn(host, port, keyfile):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((host, port))
        sock.listen(1)
        # TODO: cancel ability?
        sock.settimeout(1)
        # while not SosKeyExchange.receive_cancel_signal.is_set():
        while not SosKeyExchange.receive_cancel_signal:
            try:
                client, _addr = sock.accept()
                t = paramiko.Transport(client)
                # Get the ssh keyx key and
                key = paramiko.RSAKey.from_private_key_file(keyfile)
                t.add_server_key(key)
                t.set_subsystem_handler("sftp", SFTPServer, SosKeyExchange.SSHKeyxSI)
                t.start_server(server=SosKeyExchange.SSHKeyxServer())
                return t
            except socket.timeout:
                if time.time() > SosKeyExchange.follower_start_time + sosconstants.SOS_KEYX_TIMEOUT:
                    logger.warning("Timeout while waiting for leader")
                    raise soserrors.SosError("Timeout while receiving key")
        logger.debug("follower key reception cancelled")
        sock.close()
        raise soserrors.SosError("follower key reception cancelled")

    @staticmethod
    def accept_key(checksum=None):
        if checksum:
            logger.error("Not checking key checksum!")
        if not os.path.isfile(sosconstants.SOS_KEYX_PUBKEY):
            raise soserrors.SosError("No key received")
        try:
            shutil.copyfile(sosconstants.SOS_KEYX_PUBKEY, sosconstants.REMOTE_KEYFILE_PUB)
        except:
            raise soserrors.SosError("Could not copy key")

    @staticmethod
    def send_ssd() -> SystemSetupDataModel:
        # collect the data from different areas
        myssd = systemsetupdata.get_system_ssd()
        myssd.action = SsdAction.INTERACTIVE_LEADER
        ssd_json = myssd.to_json()
        ssd_to_send = io.BytesIO(bytes(ssd_json, "UTF-8"))

        remotecfg = remotehost.RemoteHost()
        remote = remotecfg.get()
        print(f"json:{ssd_to_send.getvalue()}")

        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.WarningPolicy())
        # TODO: Connect timeout/cancel ability?
        client.connect(remote.hostname, remote.port, key_filename=sosconstants.SOSKEYX_KEYFILE)
        sftp = paramiko.SFTPClient.from_transport(client.get_transport())
        sftp.putfo(ssd_to_send,
                   sosconstants.SOS_KEYX_SSDPATH_IN, confirm=False)
        with io.BytesIO() as fo:
            sftp.getfo(sosconstants.SOS_KEYX_SSDPATH_OUT, fo)
            fo.seek(0)
            ssdreceived = SystemSetupDataModel()
            logger.debug(f"Got ssd from remote: {fo.getvalue()}")
            fo.seek(0)
            ssdreceived.update_from_json(fo.getvalue())
        sftp.close()
        # maybe we should move this to some higher layer
        with open(sosconstants.SOS_KEYX_PUBKEY, "wb") as keyx:
            keyx.write(bytes(ssdreceived.pub_key, "UTF-8"))

        return ssdreceived

    @staticmethod
    def receive_ssd() -> SystemSetupDataModel:
        sshd = sshdmanager.get(Sshd.EXTRANET)
        SosKeyExchange.follower_start_time = time.time()
        transport = SosKeyExchange.__accept_one_ssh_conn("0.0.0.0", sshd.port, sosconstants.SOSKEYX_KEYFILE)
        while len(SosKeyExchange._ssd_in) == 0:
            if time.time() > SosKeyExchange.follower_start_time + sosconstants.SOS_KEYX_TIMEOUT:
                raise soserrors.SosError("Timeout while receiving key")
            print(f"waiting {time.time()}")
            time.sleep(0.2)

        logger.debug(f"Got ssd from remote: {SosKeyExchange._ssd_in}")

        myssdin = SystemSetupDataModel()
        myssdin.update_from_json(SosKeyExchange._ssd_in)
        # maybe we should move this to some higher layer
        with open(sosconstants.SOS_KEYX_PUBKEY, "wb") as keyx:
            keyx.write(bytes(myssdin.pub_key, "UTF-8"))

        while transport.is_active():
            time.sleep(2)

        return myssdin

    @staticmethod
    def remove_remote_key():
        if not os.path.isfile(sosconstants.REMOTE_KEYFILE_PUB):
            raise soserrors.SosError(f"No key present to remove: {sosconstants.REMOTE_KEYFILE_PUB}")
        try:
            os.remove(sosconstants.REMOTE_KEYFILE_PUB)
        except:
            raise soserrors.SosError("Could not remove key")

    @staticmethod
    def get_remote_key():
        """
        get remote pub key and fingerprint
        :return: remote key
        """
        remote_key = SosKeyModel()
        if os.path.isfile(sosconstants.REMOTE_KEYFILE_PUB):
            with open(sosconstants.REMOTE_KEYFILE_PUB, "r") as f:
                remote_key.pub_key = f.read().rstrip()
                remote_key.fingerprint = soshelpers.sshfingerprint(remote_key.pub_key)
        else:
            logger.debug("Requested fingerprint for remote key but no key file is existing")
            soskey.fingerprint = None
        return remote_key
