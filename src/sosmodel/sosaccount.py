"""
Model for syncosync accounts
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from sosmodel import sosconstants
from sosmodel.serializerbase import SerializerBase


class SosAccountModel(SerializerBase):
    """
    this is the base model for syncosync accounts
    """

    def __init__(self):
        self._name = ""
        self._rlongname = ""
        self._mail_address = ""
        self._backup_period = 7
        self._remind_duration = 22
        self._info_period = 7
        self._mail_info_level = "error"
        self._max_space_allowed = 100
        self.key_fingerprint = ""
        self._last_access = 0
        self._last_mail = 0
        self._space_used = -1

    def set(self, name, rlongname, mail_address, backup_period, remind_duration, info_period, mail_info_level,
            max_space_allowed, key_fingerprint):
        self._name = name
        self._rlongname = rlongname
        self._mail_address = mail_address
        self._backup_period = backup_period
        self._remind_duration = remind_duration
        self._info_period = info_period
        self._mail_info_level = mail_info_level
        self._max_space_allowed = max_space_allowed
        self.key_fingerprint = key_fingerprint

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        """
        sets the name of an account
        :param value: [a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$
        """
        user_re = re.compile(r'^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$')
        result = user_re.search(value)
        if not bool(result):
            raise ValueError("Invalid username value")
        self._name = value

    @property
    def rlongname(self):
        return self._rlongname

    @rlongname.setter
    def rlongname(self, value):
        """
        sets the real longname of an account
        :param value: ^[^:,]{0,64}$
        """
        if value is None:
            return
        user_re = re.compile(r'^[^:,]{0,64}$')
        result = user_re.search(value)
        if not bool(result):
            raise ValueError("Invalid longname value")
        self._rlongname = value

    @property
    def mail_address(self):
        return self._mail_address

    @mail_address.setter
    def mail_address(self, value):
        """
        sets the mail address of an account
        :param value: ^([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+|)$
        """
        if value is None:
            return
        user_re = re.compile(r'^([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+|)$')
        result = user_re.search(value)
        if not bool(result):
            raise ValueError("Invalid mailaddress value")
        self._mail_address = value

    @property
    def mail_info_level(self):
        return self._mail_info_level

    @mail_info_level.setter
    def mail_info_level(self, value):
        """
        sets the mail info level.
        :param level: "none", "warning" or "error"
        """
        if value is None:
            return
        elif value not in sosconstants.MAIL_INFO_LEVELS:
            raise ValueError("Invalid Mail info level value")
        self._mail_info_level = value

    @property
    def backup_period(self):
        return self._backup_period

    @backup_period.setter
    def backup_period(self, value):
        """
        The period in days where a backup should happen.
        :param backup_period: integer 1 to 1000
        """
        if value is None:
            return
        value = int(value)
        if value < 1 or value > 1000:
            raise ValueError("Invalid backup period value")
        self._backup_period = value

    @property
    def remind_duration(self):
        return self._remind_duration

    @remind_duration.setter
    def remind_duration(self, value):
        """
        How long the system should wait before sending a mail to the user and admin after inactivity. Should be
        at least backup_period + a few days
        :param remind_duration: in days. 0 = no reminder
        """
        if value is None:
            return
        value = int(value)
        if value < 0 or value > 1000:
            raise ValueError("Invalid remind duration value")
        self._remind_duration = value

    @property
    def info_period(self):
        return self._info_period

    @info_period.setter
    def info_period(self, value):
        """
        Period of time between two reminder notificiations.
        :param value: in days
        :return:
        """
        if value is None:
            return
        value = int(value)
        if value < 0 or value > 1000:
            raise ValueError("Invalid info period value")
        self._info_period = value

    @property
    def max_space_allowed(self):
        return self._max_space_allowed

    @max_space_allowed.setter
    def max_space_allowed(self, value):
        """
        set max space of complete harddisk in percent
        :param value: 1% - 100%
        """
        if value is None:
            return
        value = int(value)
        if value < 1 or value > 100:
            raise ValueError("Invalid max space allowed value")
        self._max_space_allowed = value

    @property
    def last_access(self):
        return self._last_access

    @last_access.setter
    def last_access(self, value):
        """
        change value, when account was last accessed
        this is time(time)
        :param value:
        """
        if value is None:
            return
        self._last_access = value

    @property
    def last_mail(self):
        return self._last_mail

    @last_mail.setter
    def last_mail(self, value):
        """
        change value, when account was last accessed
        this is time(time)
        :param value:
        """
        if value is None:
            return
        self._last_mail = value

    @property
    def space_used(self):
        return self._space_used

    @space_used.setter
    def space_used(self, value):
        """
        space used in ... which value?
        :param value:
        """
        if value is None:
            return
        self._space_used = value


class SosAccountAddModel(SosAccountModel):
    """
    This is the model for adding accounts
    """

    def __init__(self):
        super().__init__()
        self._password = None
        self._ssh_pub_key = None

    def set_add_params(self, password, ssh_pub_key):
        self._password = password
        self._ssh_pub_key = ssh_pub_key

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        """
        Checks the password if certain criteria is met
        Right now, no checks, could change in the future
        :param new_password: password to set, None for not changing (means not setting at add), empty string to delete password (or not to set at add)
        """
        self._password = value

    @property
    def ssh_pub_key(self):
        return self._ssh_pub_key

    @ssh_pub_key.setter
    def ssh_pub_key(self, value):
        """
        Checks the ssh_pub_key if certain criteria is met, set to None for not to change, empty string for not to set at add / delete at mod
        :param value: '^(ssh-rsa AAAAB3NzaC1yc2|ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNT|ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzOD|ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1Mj|ssh-ed25519 AAAAC3NzaC1lZDI1NTE5|ssh-dss AAAAB3NzaC1kc3)[0-9A-Za-z+/]+[=]{0,3}( .*)?\$'
        """
        if value != "" and value is not None:
            user_re = re.compile(
                r'^(ssh-rsa AAAAB3NzaC1yc2|ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNT|ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzOD|ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1Mj|ssh-ed25519 AAAAC3NzaC1lZDI1NTE5|ssh-dss AAAAB3NzaC1kc3)[0-9A-Za-z+/]+[=]{0,3}( .*)?$')
            result = user_re.search(value)
            if not bool(result):
                raise ValueError("Invalid SSH key")
        self._ssh_pub_key = value
