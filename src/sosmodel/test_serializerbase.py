import unittest
from sosmodel.serializerbase import SerializerBase, SerializerBaseJsonEncoder


class Simple(SerializerBase):
    def __init__(self):
        self._a = "a"
        self.b = "b"

    @property
    def a(self):
        return self._a

    @a.setter
    def a(self, value):
        print("Setter a called")
        self._a = value

    def test(self):
        raise Exception("Should not be called")


class Inner(SerializerBase):
    def __init__(self):
        self.c = "c"
        self.d = "d"


class Nested(SerializerBase):
    def __init__(self):
        self.a = "a"
        self.b = "b"
        self.inner = Inner()


class SimpleTest(unittest.TestCase):
    def test_simple_to_json(self):
        mysimple = Simple()
        mysimple_string = mysimple.to_json()
        self.assertEqual(mysimple_string, '{"a": "a", "b": "b"}', "set from init")

    def test_simple_to_json_null(self):
        mysimple = Simple()
        mysimple.a = None
        mysimple_string = mysimple.to_json()
        self.assertEqual(mysimple_string, '{"a": null, "b": "b"}', "set from init")

    def test_simple_from_json(self):
        mysimple = Simple.from_json('{"a": "a1", "b": "b1"}')
        mysimple_string = mysimple.to_json()
        self.assertEqual(mysimple_string, '{"a": "a1", "b": "b1"}', "set from json")

    def test_simple_partial_from_json(self):
        mysimple = Simple.from_json('{"a": "a1"}')
        mysimple_string = mysimple.to_json()
        self.assertEqual(mysimple_string, '{"a": "a1", "b": "b"}', "set partial from json")

    def test_simple_update_from_json(self):
        mysimple = Simple.from_json('{"a": "a1", "b": "b1"}')
        mysimple.update_from_json('{"a": "a1", "b": "b2", "test": "abc"}')
        mysimple_string = mysimple.to_json()
        self.assertEqual(mysimple_string, '{"a": "a1", "b": "b2"}', "simple update from json")


class ArrayTest(unittest.TestCase):
    def test_array_to_json(self):
        myarray = []
        mysimple = Simple()
        myarray.append(mysimple)
        myarray.append(mysimple)
        print(f"array to_json{SerializerBaseJsonEncoder.to_json(myarray)}")


class NestedTest(unittest.TestCase):
    def test_nested_to_json(self):
        mynested = Nested()
        mynested_string = mynested.to_json()
        self.assertEqual(mynested_string, '{"a": "a", "b": "b", "inner": {"c": "c", "d": "d"}}', "set from init")

    def test_nested_from_json(self):
        mynested = Nested.from_json('{"a": "a-set", "b": "b-set", "inner": {"c": "c-set", "d": "d-set"}}')
        mynested_string = mynested.to_json()
        self.assertEqual(mynested_string, '{"a": "a-set", "b": "b-set", "inner": {"c": "c-set", "d": "d-set"}}',
                         "set from json")

    def test_nested_partial_from_json(self):
        mynested = Nested.from_json('{"b": "b-set", "inner": {"d": "d-set"}}')
        mynested_string = mynested.to_json()
        self.assertEqual(mynested_string, '{"a": "a", "b": "b-set", "inner": {"c": "c", "d": "d-set"}}',
                         "set partial from json")


if __name__ == '__main__':
    unittest.main()
