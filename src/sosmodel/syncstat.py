"""
Model for synchronisation state (and mucho more)
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import List, Optional

from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import SyncStatus, RemoteUpStatus, SyncstatStatus, SystemMode, SystemStatus


class SyncStatModel(SerializerBase):
    """
    The model holding all necessary information to show the actual sync status
    """

    def __init__(self, syncstat_status: SyncstatStatus = SyncstatStatus.NOT_AVAILABLE,
                 sysstate: SystemStatus = SystemStatus.NOTHING,
                 actmode: SystemMode = SystemMode.STARTUP,
                 remote_up: RemoteUpStatus = RemoteUpStatus.NOT_AVAILABLE,
                 remote_down_since: int = 0, syncup: SyncStatus = SyncStatus.NOT_AVAILABLE,
                 syncup_change_since: int = 0,
                 syncdown: SyncStatus = SyncStatus.NOT_AVAILABLE, syncdown_change_since: int = 0,
                 bw_up: int = 0, bw_down: int = 0, date: int = 0, driveup: Optional[List[str]] = None,
                 total_size: int = 0, free_space: int = 0, iused: int = 0,
                 to_check: int = 0, akt_file: str = '', percent: int = 0, admin_last_mail: int = 0):
        ## Overall status
        self.syncstat_status: SyncstatStatus = syncstat_status
        ## System status
        self.sysstate: SystemStatus = sysstate
        ## Actual mode
        self.actmode: SystemMode = actmode
        ## is the remote host reachable or not?
        self.remote_up: RemoteUpStatus = remote_up
        ## and, since when it is down?
        self.remote_down_since: int = remote_down_since
        ## is there any data uploaded to remote host?
        self.syncup: SyncStatus = syncup
        ## and when has the state changed?
        self.syncup_change_since: int = syncup_change_since
        ## is there any data arriving from remote host?
        self.syncdown: SyncStatus = syncdown
        ## and when has this state changed last time?
        self.syncdown_change_since: int = syncdown_change_since
        ## Bandwidth upstream (to remote host) in bytes/sec
        self.bw_up: float = bw_up
        ## Bandwidth downstream (from remote host) in bytes/sec
        self.bw_down: float = bw_down
        ## the timestamp (as of round(time.time())) of this status
        self.date: int = date
        ## array of drives, which are up and running ("sda", "sdb")
        if driveup is None:
            self.driveup: List[str] = []
        else:
            self.driveup: List[str] = driveup
        ## total size of local volume in bytes
        self.total_size: int = total_size
        ## free space on local volume in bytes
        self.free_space: int = free_space
        ## inodes used (dunno why this is necessary, I think for calculation of new to sync files)
        self.iused: int = iused
        ## files which have to be checked for rsync
        self.to_check: int = to_check
        ## file name of actual file (informative)
        self.akt_file: str = akt_file
        ## how much of the akt_file is transferred in %
        self.percent: int = percent
        ## time stamp, when the last admin mail was set
        self.admin_last_mail: int = admin_last_mail


class BandWidthModel(SerializerBase):
    """
    The model holding bandwidth information at a dedicated point of time.
    """

    def __init__(self, bw_up: int = 0, bw_down: int = 0, date: int = 0):
        ## Bandwidth upstream (to remote host) in bytes/sec
        self.bw_up: float = bw_up
        ## Bandwidth downstream (from remote host) in bytes/sec
        self.bw_down: float = bw_down
        ## the timestamp (as of round(time.time())) of this status
        self.date: int = date
