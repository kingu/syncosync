# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:##www.gnu.org/licenses/>.
from typing import Optional

from enum import IntEnum


class VolMount(IntEnum):
    NONE = 0,
    INCOMPLETE = 1,
    NORMAL = 2,
    SWAP = 3


class NicMode(IntEnum):
    DHCP = 0,
    STATIC = 1,
    ## v6 only
    AUTO = 2,
    ## off
    MANUAL = 3


class SupportedLanguage(IntEnum):
    UNSET = 0,
    ENGLISH = 1,
    GERMAN = 2

    @staticmethod
    def to_iso(language: 'SupportedLanguage', fallback_if_unset: Optional['SupportedLanguage'] = None) -> str:
        """
        Converts the given language (SupportedLanguage value) to an ISO 639.1 or 639.2 code.
        See https://www.loc.gov/standards/iso639-2/php/code_list.php
        """

        iso_codes = {0: "",
                     1: "en-US",
                     2: "de"
                     }

        if not language or language not in iso_codes:
            return "" if not fallback_if_unset else iso_codes[fallback_if_unset]
        else:
            return iso_codes[language]


class RemoteUpStatus(IntEnum):
    """
    Handling the status for checking remote hosts
    """
    ## not available (hostname is not set?)
    NOT_AVAILABLE = 0,
    ## host is not reachable (wrong hostname or port?)
    NO_REPLY = 1,
    ## not successful, may be an error we did not account for
    FAULTY = 2,
    ## host is up and key is fine
    UP = 3,
    ## No SSH session present
    NO_SSH = 4,
    ## SSH key file is corrupted or the remote host rejected it
    BAD_KEY = 5
    ## sshd is started but is closed
    HOST_CLOSED = 6

    def message(reason):
        if reason == RemoteUpStatus.NOT_AVAILABLE:
            return "Status is not available (Not set, Not right mode)"
        if reason == RemoteUpStatus.NO_REPLY:
            return "Remote host is not reachable (this is no reply on this port from this host)"
        if reason == RemoteUpStatus.FAULTY:
            return "Unknown error"
        if reason == RemoteUpStatus.UP:
            return "Remote host is up and key is correct"
        if reason == RemoteUpStatus.NO_SSH:
            return "No ssh session present"
        if reason == RemoteUpStatus.BAD_KEY:
            return "ssh key is not accepted"


class SyncStatus(IntEnum):
    ## not available (hostname is not set?)
    NOT_AVAILABLE = 0,
    ## there is an active session / synchronization ongoing
    ACTIVE = 1,
    ## Connection is idling, no active synchronization
    IDLE = 2


class SyncstatStatus(IntEnum):
    ## not available
    NOT_AVAILABLE = 0,
    ## available
    AVAILABLE = 1


class SystemStatus(IntEnum):
    """ system states - ui and other tools may decide on these states what to do
    """
    NOTHING = 0  # Nothing to report
    DETECTION = 1  # Detection on its way
    DISK_SETUP_NEED = 2  # The drive detection led to a situation where the UI should request interaction
    SOS_VOLUMES_NOT_MOUNTED = 4  # The sosvolumes are not mounted
    SOS_KEY_NOT_GENERATED = 8  # syncosync key is not there
    REMOTE_HOST_NOT_CONFIGURED = 16
    SOS_CONF_BACKUP_NOT_DONE = 32  # there is no metadata of save backup, most probably, the system is unconfigured
    SOS_CONF_BACKUP_NOT_MATCH = 64  # the metadata of actual config differs with last metadata on local volume
    INTRANET_SSHD_DOWN = 128
    INTRANET_SSHD_CLOSE = 256
    EXTRANET_SSHD_DOWN = 512
    EXTRANET_SSHD_CLOSE = 1024
    PARTITIONING_NEED = 2048  # there is just the partitioning part done from DISK_SETUP

    def message(reason):
        result = []
        if reason == SystemStatus.NOTHING:
            return ["Nothing to report"]
        if reason & SystemStatus.DETECTION:
            result.append("Detecion is on it's way")
        if reason & SystemStatus.DISK_SETUP_NEED:
            result.append("Manual disk reconfiguration is needed")
        if reason & SystemStatus.SOS_VOLUMES_NOT_MOUNTED:
            result.append("The syncosync volumes are not mounted")
        if reason & SystemStatus.SOS_KEY_NOT_GENERATED:
            result.append("The syncosync key is not existing")
        if reason & SystemStatus.REMOTE_HOST_NOT_CONFIGURED:
            result.append("The remote host is not configured")
        if reason & SystemStatus.SOS_CONF_BACKUP_NOT_DONE:
            result.append("The configuration was never backuped")
        if reason & SystemStatus.SOS_CONF_BACKUP_NOT_MATCH:
            result.append("The configuration backup does not match the backup on the HDD")
        if reason & SystemStatus.INTRANET_SSHD_DOWN:
            result.append("The intranet sshd is not started")
        if reason & SystemStatus.INTRANET_SSHD_CLOSE:
            result.append("The intranet sshd is in close state")
        if reason & SystemStatus.EXTRANET_SSHD_DOWN:
            result.append("The extranet sshd is not started")
        if reason & SystemStatus.EXTRANET_SSHD_CLOSE:
            result.append("The extranet sshd is in close state")
        if reason & SystemStatus.PARTITIONING_NEED:
            result.append("Drives are formatted but not partitioned yet")
        return result


class Partition(IntEnum):
    """
    The three different partitions of a sos system
    """
    ## local holds all local data from backup clients in the same intranet
    LOCAL = 0
    ## remote is the partition which get's the synced data from the remote host
    REMOTE = 1
    ## system holds logs etc. but _not_ backup configurations
    SYSTEM = 3


class SystemMode(IntEnum):
    """
    Possible Modes the system could get in (see wiki: Startup,-Recovery-and-Disaster-Szenarios)
    """
    ## Startup -> the system does not know, what mode it is right now
    STARTUP = 0
    ## Set Up Mode, some actions will be overwritten by manual interaction, this will lead to either
    ## the DEFAULT mode or the REMOTE_RECOVERY mode
    ## In SETUP Mode the volumes are not mounted
    SETUP = 1
    ## default mode -> mount drives, start ssh and lsyncd
    DEFAULT = 2
    ## Remote Recovery -> the system will recover from a remote host
    REMOTE_RECOVERY = 3
    ## Disaster Swap -> the system will swap local and remote volumes
    DISASTER_SWAP = 4
    ## This mode will be finally reached
    SHUTDOWN = 5

    def message(mode):
        if mode == SystemMode.STARTUP:
            return "Startup"
        if mode == SystemMode.SETUP:
            return "Setup"
        if mode == SystemMode.DEFAULT:
            return "Default"
        if mode == SystemMode.REMOTE_RECOVERY:
            return "Remote Recovery"
        if mode == SystemMode.DISASTER_SWAP:
            return "Disaster Swap"
        if mode == SystemMode.SHUTDOWN:
            return "Shutdown"


class Sshd(IntEnum):
    """
    Internal oder External sshd
    """
    INTRANET = 0
    EXTRANET = 1


class SshdAccess(IntEnum):
    """
    if access is open or close
    """
    OFF = 0
    OPEN = 1
    CLOSE = 2


class SsdAction(IntEnum):
    """
    This is the idea that for setting up a remote system an action could be defined.
    """
    ## Interactive: like documented in the wiki: leader provides key and pulls key and free blocks
    ## then provides partitions
    INTERACTIVE_LEADER = 0
    INTERACTIVE_FOLLOWER = 1


class GenericUiResponseStatus(IntEnum):
    """
    Enum indicating the type/status of the response given to the UI if an error occurs or a simple OK is sufficient
    """
    OK = 0,
    ERROR = 1
