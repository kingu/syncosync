"""
This is the model for the remote host, the one to which the local data will be synced
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import SsdAction


class SystemSetupDataModel(SerializerBase):
    """
    The objects necessary to exchange setup information between alice and bob
    There are no setters, as all the parameters come from internal providers
    """

    def __init__(self, pub_key: str = "", free: int = 0, local: int = 0, remote: int = 0,
                 action: int = SsdAction.INTERACTIVE_LEADER):
        ## syncosync.pub
        self.pub_key = ""
        ## unassigned volume group blocks (of 4MB)
        self.free = 0
        ## local partition size in blocks of 4MB
        self.local = 0
        ## remote partition size in blocks of 4MB
        self.remote = 0
        ## action
        self.action = SsdAction.INTERACTIVE_LEADER
        self.pub_key = pub_key
        self.free = free
        self.local = local
        self.remote = remote
        self.action = action
