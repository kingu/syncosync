import collections
import json
from json import JSONEncoder
from typing import List, Union, TypeVar, Type, Collection, Dict
import logging

logger = logging.getLogger(__name__)


class SerializerBase(object):
    """
    Represents the system's hostname
    """

    def _update_from_dict(self, values: Dict):
        """
        Initializes values of instance with values given (dict).
        If your SerializerBase derived class uses instances of other derived classes you will need to overwrite this
        method and initialize the instances!
        """
        if values is None:
            return
        for key in self.__dir__():
            if key.startswith("__") and key.endswith("__"):
                continue
            if key in values:
                attr_or_method = getattr(self, key, None)
                new_values = values.get(key)
                if attr_or_method is not None and callable(attr_or_method):
                    # we have a method, if there is a variable with _ prefix, call it
                    if "_" + key in self.__dict__:
                        attr_or_method(new_values)
                    else:
                        continue
                elif (attr_or_method is not None and isinstance(new_values, dict)
                        and isinstance(attr_or_method, SerializerBase)):
                    attr_or_method._update_from_dict(values.get(key))
                elif (attr_or_method is not None and isinstance(new_values, list)
                        and isinstance(attr_or_method, list)
                        and all(isinstance(item, dict) for item in new_values)
                        and all(isinstance(item, SerializerBase) for item in attr_or_method)):
                    # TODO: This may still be wonky
                    # all items in class variable list are Serializer base and the items in the new values list are
                    # dicts. We can assume intended updated
                    type_of_list = None
                    for val_of_list in attr_or_method:
                        if (type_of_list is not None and type_of_list != type(val_of_list)
                                or not isinstance(val_of_list, SerializerBase)):
                            return
                        else:
                            type_of_list = type(val_of_list)
                    if type_of_list is None:
                        return
                    attr_or_method.clear()
                    for new_val in new_values:
                        attr_or_method.append(type_of_list.from_json(new_val))
                else:
                    setattr(self, key, values.get(key))

    @classmethod
    def from_json(cls, json_string):
        obj = cls.__new__(cls)
        cls.__init__(obj)
        try:
            loaded = json.loads(json_string)
            obj._update_from_dict(loaded)
        except Exception as e:
            logger.error(f"Could not decode json ({str(e)}): \"{json_string}\"")
        return obj

    def update_from_json(self, json_string):
        loaded = json.loads(json_string)
        self._update_from_dict(loaded)

    def to_json(self) -> str:
        return json.dumps(self, cls=SerializerBaseJsonEncoder, sort_keys=True)


class SerializerBaseJsonEncoder(JSONEncoder):
    def __encode_serializer_base_list(self, o: Collection[SerializerBase]) -> str:
        buf = '['
        separator = self.item_separator
        first = True
        for value in o:
            if first:
                first = False
            else:
                buf += separator

            buf += value.to_json()
        buf += ']'
        return buf

    def encode(self, o):
        """Return a JSON string representation of a Python data structure.

        >>> from json.encoder import JSONEncoder
        >>> JSONEncoder().encode({"foo": ["bar", "baz"]})
        '{"foo": ["bar", "baz"]}'

        """
        # TODO: Enums break if a wrong value has been set before
        if isinstance(o, SerializerBase):
            dump_dict: Dict = {}
            for key in dir(o):
                if key.startswith("__"):
                    continue
                attr_or_method = getattr(o, key)
                if callable(attr_or_method) or key.startswith("__"):
                    # method or private var
                    continue
                # we have a variable/attribute
                # we need to first check if it starts with _ or __, then check if there is a callable without
                key_to_use: str = key
                if key.startswith("_"):
                    potential_setter = getattr(o, key.replace("_", "", 1))
                    if callable(potential_setter):
                        key_to_use = key.replace("_", "", 1)
                    else:
                        # unable to locate setter, we don't want to place protected/private into json tho
                        continue

                if isinstance(attr_or_method, SerializerBase):
                    dump_dict[key_to_use] = json.loads(attr_or_method.to_json())
                elif isinstance(attr_or_method, list) \
                        and all(isinstance(item, SerializerBase) for item in attr_or_method):
                    # if it is a list of serializerbase, we need to handle it accordingly...
                    dump_dict[key_to_use] = json.loads(self.__encode_serializer_base_list(attr_or_method))
                else:
                    # TODO: If a dict contains serializerbase, we would need to handle it...
                    dump_dict[key_to_use] = attr_or_method
            return JSONEncoder.encode(self, dump_dict)
        elif (isinstance(o, collections.Collection)
              and all(isinstance(item, SerializerBase) for item in o)):
            # list of SerializerBase items TODO
            return self.__encode_serializer_base_list(o)
        else:
            return JSONEncoder.encode(self, o)

    @staticmethod
    def to_json(o: Union[SerializerBase, List[SerializerBase]]) -> str:
        return json.dumps(o, cls=SerializerBaseJsonEncoder, sort_keys=True)


class SerializerBaseJsonDecoder:
    T = TypeVar('T', bound=SerializerBase)

    @staticmethod
    def decode(cls: Type[T], serialized_string: str) \
            -> Union[T, List[T]]:
        """
        Decodes a given json formatted string (serialized string) using the class passed (cls)
        """
        json_decoded = json.loads(serialized_string)
        if isinstance(json_decoded, list):
            # the data we received was a list, we have to decode multiple items
            result = []
            for item in json_decoded:
                # item is a dict we need to transform to an instance...
                # since settattr does not play nicely with constructors, we transform back to str
                # performance hell
                result.append(cls.from_json(json.dumps(item)))
            return result
        elif isinstance(json_decoded, dict):
            # assume the data is a plain instance
            return cls.from_json(serialized_string)
        else:
            # we don't know how to deal with the data, throw a TypeError
            raise TypeError("Unable to decode string from json to class instances: %s".format(serialized_string))
