"""
This is the model for the syncosync configuration
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sosmodel import sosconstants
from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import SupportedLanguage
from sosmodel.uilanguage import UiLanguage


class MailConfigModel(SerializerBase):
    def __init__(self):
        self.admin_mail_address = ""
        ## the time in days between the reception of consecutive warning/error mails for the admin
        self.admin_info_period = 7
        ## smtp_host or smart host
        self.smtp_host = ""
        ## the user on this host
        self.smtp_user = ""
        ## and the password for this user
        self.smtp_passwd = ""
        ## the port (defaults to 465)
        self.smtp_port = 465


class SystemDescriptionModel(SerializerBase):
    """
    The system description, which holds static data how some hw ist related to the sysncosync setup
    """

    def __init__(self):
        ## Version is a constant from sosconstants.SOS_VERSION
        # The version is kept here to check for compatibility on upgrades
        self.version = sosconstants.SOS_VERSION
        ## systype is a string defining what system type to use for this configuration. This should _not_ be
        # changeable via the UI as the systype is hardware dependent
        self.systype = "generic"
        ## array of network interface names fro systype configuration - these are the all the allowed nics,
        # they do not necessarily have to be used
        # Note: this array can't be changed by the setter
        self.nics = []
        ## array of network drives from systype configuration - these are the all the allowed drives,
        # they do not necessarily have to be used
        # Note: this array can't be changed by the setter!
        self.drives = []


class SosConfigModel(SerializerBase):
    """
    Represents the syncosync config
    """

    def __init__(self):
        ## Version is a constant from sosconstants.SOS_VERSION
        # The version is kept here to check for compatibility on upgrades
        super().__init__()
        self.version = sosconstants.SOS_VERSION
        ## systype is a string defining what system type to use for this configuration. This should _not_ be
        # changeable via the UI as the systype is hardware dependent
        self.systype = "generic"
        ## a boolean, defining if sos should try to request to the gateway to open the incoming port for
        # syncing via upnp
        self.upnp = False
        ## two letter synonym for the ui language - does override browser language - if empty, browser language is
        # used
        self.ui_language: UiLanguage = UiLanguage()
        ## the mail settings
        self.mail_settings = MailConfigModel()
        ## local drive size
        self.local = None
        ## remote drive size
        self.remote = None
