"""
This is the model for configuration backups
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sosmodel.serializerbase import SerializerBase


class ConfigBackupModel(SerializerBase):
    """
    Holds the attributes to identify a configuration backup file
    """

    def __init__(self):
        ## timestamp
        # This is the timestamp when the backup was done. This is the integer part of time.time(), so, seconds since
        # epoch (January 1, 1970, 00:00:00 (UTC)) or Unix time
        self.timestamp = 0
        ## md5sum
        # This is the sum of the whole compressed archive, e.g. 078717193ba94ea7ac381d43cb539129
        # The value should be the same than issuing "md5sum <archive-file>" from the command line
        self.md5sum = ""
        ## hostname
        # this is the hostname of the system at the time the backup was made
        # so the hostname of the actual machine could be different, but there is no reason to change it in the metadata
        self.hostname = ""
        ## filepath
        # this is the complete path and filename of the file
        self.filepath = ""


class ConfigBackupContentModel(SerializerBase):
    """
    Holds all models which are of interest to be shown in an info dialog
    """

    def __init__(self):
        self.sosaccounts = []
        self.hostname = None
        self.remotehost = None
