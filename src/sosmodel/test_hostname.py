import unittest
from sosmodel import hostname


class MyTestCase(unittest.TestCase):
    def test_correct_name1(self):
        mymodel = hostname.HostNameModel.from_json('{"hostname": "test"}')
        self.assertEqual(mymodel.hostname, "test", "test good")

    def test_correct_name2(self):
        mymodel = hostname.HostNameModel.from_json('{"hostname": "te23-45st"}')
        self.assertEqual(mymodel.hostname, "te23-45st", "test good")

    def test_correct_name3(self):
        mymodel = hostname.HostNameModel.from_json('{"hostname": "te-st"}')
        self.assertEqual(mymodel.hostname, "te-st", "test good")


class ExpectedFailureTestCase(unittest.TestCase):
    @unittest.expectedFailure
    def test_wrong_name1(self):
        mymodel = hostname.HostNameModel.from_json('{"hostname": "-test"}')
        self.assertEqual(mymodel.hostname, "-test", "broken")

    @unittest.expectedFailure
    def test_wrong_name2(self):
        mymodel = hostname.HostNameModel.from_json('{"hostname": "test-"}')
        self.assertEqual(mymodel.hostname, "test-", "broken")


if __name__ == '__main__':
    unittest.main()
