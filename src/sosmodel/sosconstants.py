"""
a lot of constants needed in the sos world
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os.path
from sosmodel.sos_enums import Partition, SystemMode, SshdAccess, Sshd
import textwrap


## the config file version - should we need it ever
SOS_VERSION = "1"

## config related
LSYNCD_CONF_FILE = "/etc/lsyncd/lsyncd.conf.lua"
SOS_CONF_DIR_RELATIVE = "etc/syncosync/"
SOS_CONF_DIR = os.path.join("/", SOS_CONF_DIR_RELATIVE)
SOS_CONF_FILE = os.path.join(SOS_CONF_DIR, "sosconfig.json")
STOREDMODE_FILE = os.path.join(SOS_CONF_DIR, "storedmode.json")
SOS_CONF_TIMESTAMP_FILE = "last_configure_time.json"
SOS_CONF_TIMESTAMP_FILEPATH = os.path.join(SOS_CONF_DIR, SOS_CONF_TIMESTAMP_FILE)
SOS_CONF_TIMESTAMP_FILEPATH_RELATIVE = os.path.join(SOS_CONF_DIR_RELATIVE, SOS_CONF_TIMESTAMP_FILE)
## This is where my own keys are
SOS_KEYFILE_DIR = os.path.join(SOS_CONF_DIR, "sshkeys")
## syncosync private key
SOS_KEYFILE = os.path.join(SOS_KEYFILE_DIR, "syncosync")
## syncosync public key
SOS_KEYFILE_PUB = os.path.join(SOS_KEYFILE_DIR, "syncosync.pub")
## This is where the pub key from the remote host get's copied to after it is accepted
REMOTE_KEYFILE_DIR = os.path.join(SOS_CONF_DIR, "remotekeys")
## This is the remote public key itself
REMOTE_KEYFILE_PUB = os.path.join(REMOTE_KEYFILE_DIR, "syncosync.pub")
## This is the directory where all account keys are stored
ACCOUNT_KEYFILE_DIR = os.path.join(SOS_CONF_DIR, "accountkeys")
## The relative path for storing config backups
SOS_CONF_BACKUP_PATH = "syncosync/configbackup/"
## The metadata of the last stored configuration backup
SOS_CONF_BACKUP_METADATA = os.path.join(SOS_CONF_DIR, "configbackup.json")
## sshd related
## All the sshd config files
SSHD_CFG = dict()
SSHD_CFG[Sshd.INTRANET, SshdAccess.CLOSE] = os.path.join(SOS_CONF_DIR, "sshd_config_intranet_close")
SSHD_CFG[Sshd.INTRANET, SshdAccess.OPEN] = os.path.join(SOS_CONF_DIR, "sshd_config_intranet_open")
SSHD_CFG[Sshd.EXTRANET, SshdAccess.CLOSE] = os.path.join(SOS_CONF_DIR, "sshd_config_extranet_close")
SSHD_CFG[Sshd.EXTRANET, SshdAccess.OPEN] = os.path.join(SOS_CONF_DIR, "sshd_config_extranet_open")

## These are all items which should be backed up for configbackup
CONFIGRESTORE_ITEMS = ["/etc/network/interfaces.d", "/etc/resolv.conf",
                       "/etc/hostname",
                       SOS_CONF_FILE,
                       SOS_KEYFILE_DIR, REMOTE_KEYFILE_DIR,
                       ACCOUNT_KEYFILE_DIR,
                       SSHD_CFG[Sshd.INTRANET, SshdAccess.CLOSE],
                       SSHD_CFG[Sshd.INTRANET, SshdAccess.OPEN],
                       SSHD_CFG[Sshd.EXTRANET, SshdAccess.CLOSE],
                       SSHD_CFG[Sshd.EXTRANET, SshdAccess.OPEN],
                       "/etc/lsyncd/lsyncd.conf.lua",
                       SOS_CONF_TIMESTAMP_FILEPATH]
CONFIGBACKUP_ITEMS = CONFIGRESTORE_ITEMS + ["sosaccounts.json"]

## key related stuff
SOS_KEYX_DIR = "/usr/share/syncosync/keyxkeys/"
SOSKEYX_KEYFILE = os.path.join(SOS_KEYX_DIR, "soskeyx")
SOSKEYX_KEYFILE_PUB = os.path.join(SOS_KEYX_DIR, "soskeyx.pub")

# SOS keyxchange
SOS_KEYX_HOME = "/home/soskeyx"
## this is where a remote key gets copied to before it is accepted
SOS_KEYX_PUBKEY = os.path.join(SOS_KEYX_HOME, "syncosync.pub")
SOS_KEYX_SSDPATH_IN = os.path.join(SOS_KEYX_HOME, "systemsetupdata_in.json")
SOS_KEYX_SSDPATH_OUT = os.path.join(SOS_KEYX_HOME, "systemsetupdata_out.json")
SOS_KEYX_TIMEOUT = 30 # so long we will wait while receiving

# the temp files - mainly for status sharing/caching
RUNDIR = "/tmp/syncosync/"  # this is, where all cache files go, could be run or tmp, tmp is also user writeable
SOSACCOUNTACCESS_CACHE = os.path.join(RUNDIR,
                                      "sosaccounts-access.json")  # this is, where the last access json is cached
ACTSTATE_FILE = os.path.join(RUNDIR, "sysstate.json")
SOSTOCHECK_CACHE = os.path.join(RUNDIR, "sostocheck.json")  # this is the rsyncwrapperscript cache file
SOSACCOUNTSPACE_CACHE = os.path.join(RUNDIR, "sosaccounts-space.json")  # this is, where the space json is cached
SOSACCOUNTMAIL_CACHE = os.path.join(RUNDIR, "sosaccounts-mail.json")  # this is, where the last mail json is cached
DRIVESTATUS_CACHE = os.path.join(RUNDIR, "bootstatus.json")  # bootstatus is cached here
SPACE_CACHE = os.path.join(RUNDIR, "space.json")  # this is, where the last access json is cached
SYNCSTAT_CACHE = os.path.join(RUNDIR, "syncstat.json")  # this is, where the last access json is cached

# static stuff
STATIC_BASE_DIR = "/usr/share/syncosync/"
SOS_SYSTEM_DESCRIPTIONS = os.path.join(STATIC_BASE_DIR, "system-descriptions/")
RECORD_DIR = os.path.join(STATIC_BASE_DIR, "demodata/")  # this is used from the recording script for simulation
SOSACCOUNT_GROUP_NAME = "sosaccount"
SOSADMIN_USER = "sosadmin"

# volumes and mounts
SOS_VOLUMES = [Partition.LOCAL, Partition.REMOTE, Partition.SYSTEM]
MOUNTDEV = {}
MOUNT = {}
MOUNTOPTS = {}
## This is for the local partition
MOUNTDEV[Partition.LOCAL] = "/dev/mapper/sos-local"
MOUNT[Partition.LOCAL] = "/mnt/local"
MOUNTOPTS[Partition.LOCAL] = "usrjquota=aquota.user,jqfmt=vfsv0"
## This is for the remote partition
MOUNTDEV[Partition.REMOTE] = "/dev/mapper/sos-remote"
MOUNT[Partition.REMOTE] = "/mnt/rjail/remote"
MOUNTOPTS[Partition.REMOTE] = ""
## This is for the system partition
MOUNTDEV[Partition.SYSTEM] = "/dev/mapper/sos-system"
MOUNT[Partition.SYSTEM] = "/mnt/system"
MOUNTOPTS[Partition.SYSTEM] = ""

SYSTEM_MIN_SIZE = 10  # this is counted in extends 4MB blocks
SYSTEM_RECOMMEND_SIZE = 2500  # in 4MB blocks, should be 10 gigs
# some timings - mainly for syncstat
CHECK_TIME_TRUE = 600  # if the remote host was available last time, check only every ten minutes
CHECK_TIME_FALSE = 30  # if the remote host was down, check every ten seconds
SYNCSTAT_REFRESH_TIME = 5  # update syncstat every 5 seconds (note: this is not remote host check!)

MAIL_INFO_LEVELS = ["none", "warning", "error"]

# FIXME: is there some better way to implement this?
# enum for error replies
emessage = dict()
EFALSE = "EFALSE"
emessage[EFALSE] = 'not successful'
ETRUE = "ETRUE"
emessage[ETRUE] = 'host is up and key is fine'
ENOREPLY = "ENOREPLY"
emessage[ENOREPLY] = 'host is not reachable (wrong hostname or port?)'
ENA = "ENA"
emessage[ENA] = 'not available (hostname is not set?)'
ENOSSH = "ENOSSH"
emessage[ENOSSH] = 'no ssh service on this port (wrong port?)'
EBADKEY = "EBADKEY"
emessage[EBADKEY] = 'no private key or wrong private key'
ACTIVE = "ACTIVE"
emessage[ACTIVE] = 'there is an active session'
IDLE = "IDLE"
emessage[IDLE] = 'idle'

# enum for content for harddisks
content = dict()
NO_PARTITION = "NO_PARTITION"
content[NO_PARTITION] = "Invalid/empty"

# Path to chroot environment
CHROOT_GEN_ENV = "/var/lib/syncosync/chroot"

# How many bandwidth values we should store in the syncstat object?
# if we store one value per second, this is 10 min.
BW_BUFFER_SIZE = 600

# Housekeeper related paths
trans_place = "/usr/share/syncosync/LANG/"
html_mail_place = "/usr/share/syncosync/mail.html"
SOS_HK_FILE = "/etc/syncosync/housekeeper.json"
LAST_MAIL_CACHE = "/etc/syncosync/lastmail.json"


def get_version() -> str:
    """
    gets version string of sos
    :return: str, e.g. 0.2-20200611124900
    """
    if os.path.isfile(os.path.join(STATIC_BASE_DIR, "BASEVERSION")):
        version_file = open(os.path.join(STATIC_BASE_DIR, "BASEVERSION"))
        version = version_file.read().strip()
    else:
        version = "unknown"
    if os.path.isfile(os.path.join(STATIC_BASE_DIR, "VERSIONTIMESTAMP")):
        version_file = open(os.path.join(STATIC_BASE_DIR, "VERSIONTIMESTAMP"))
        version = version + "-" + version_file.read().strip()
    else:
        version = version + "-unknown"
    return version


EPILOG = textwrap.dedent(f'''\
                            Version {get_version()}, part of:
                            syncosync - secure peer to peer backup synchronization
                            Copyright (C)2020 syncosync.org
                            ''')

# Flask Cookie IDs
COOKIE_LANGUAGE = "language"
