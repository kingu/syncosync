"""
Model for the hostname
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re
import json
from sosmodel.serializerbase import SerializerBase


class HostNameModel(SerializerBase):
    """
    Represents the system's hostname
    """

    def __init__(self):
        self._hostname = "syncosync"

    def set(self, hostname):
        self._hostname = hostname

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, value):
        """
        sets the hostname
        :param hostname: ^[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]$
        """
        hostname_re = re.compile(r"^[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]$")
        result = hostname_re.search(value)
        if not bool(result):
            raise ValueError
        self._hostname = value

    def to_json(self):
        return_dict = {"hostname": self.hostname}
        return json.dumps(return_dict)
