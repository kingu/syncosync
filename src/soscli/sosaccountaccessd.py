#!/usr/bin/env python3
"""
This is started as a daemon which updates sosusers last access and used_spaces values
based on inotify events
The structure is stored as a json file in /tmp/syncosync/sosuserlist-access.json
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This is started as a daemon which updates sosusers last access and used_spaces values
# based on inotify events
# The structure is stored as a json file in /tmp/syncosync/sosuserlist-access.json


import json
import re

import soscore.sosaccounts
from sosmodel import sosconstants, sos_enums
from soscore import soshelpers
import inotify.adapters
import inotify.constants
import time
import os
# following imports are all necessary for cli
import logging
import logging.handlers
import argparse
import sys

last_access = {}
space_used = {}
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

handler = logging.handlers.SysLogHandler(address='/dev/log')
logger.addHandler(handler)


def loop():
    """
    This loop waits for inotifications if a file inside /mnt/local has changed and updates then the cache files
    """
    i = inotify.adapters.InotifyTree(sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                                     mask=inotify.constants.IN_CREATE
                                          | inotify.constants.IN_CLOSE_WRITE | inotify.constants.IN_DELETE)

    for event in i.event_gen(yield_nones=False):
        (_, type_names, path, filename) = event
        logger.debug(f"Received event on {filename}")
        mtime = (round(time.time()))
        m = re.search(sosconstants.MOUNT[sos_enums.Partition.LOCAL] + '/([^/]*)/data.*', path)
        if m:
            last_access[m.group(1)] = mtime
            soshelpers.write_to_file(sosconstants.SOSACCOUNTACCESS_CACHE, json.dumps(last_access))
            space_used[m.group(1)] = soshelpers.get_disk_usage(m.group(1))
            soshelpers.write_to_file(sosconstants.SOSACCOUNTSPACE_CACHE, json.dumps(space_used))


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='reads - and caches - account size and access date',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)

    # if started first time, generate a json cache for all users, this takes a few seconds, but not too long
    # first check, if the directory exists
    if not os.path.isdir(sosconstants.RUNDIR):
        os.mkdir(sosconstants.RUNDIR)
    sosuserlist = soscore.sosaccounts.getsosaccount_names()
    for name in sosuserlist:
        last_access[name] = soshelpers.return_youngest_mod(os.path.join(
            sosconstants.MOUNT[sos_enums.Partition.LOCAL], name, "data"))
        space_used[name] = soshelpers.get_disk_usage(name)

    soshelpers.write_to_file(sosconstants.SOSACCOUNTACCESS_CACHE, json.dumps(last_access))
    soshelpers.write_to_file(sosconstants.SOSACCOUNTSPACE_CACHE, json.dumps(space_used))
    loop()


if __name__ == '__main__':
    main()
