#!/usr/bin/env python3
"""
The Script to test the housekeeper module
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging
import logging.handlers
import argparse
import sys
import textwrap
from soscore.soshousekeeper import Housekeeper
from soscore.mail import send_mail
from soscore import sosconfig
from soscore.hostname import HostName
from soscore.mail_content_gen import get_subject,get_mail_hl,get_mail_msg,get_html_mail
import time


def main():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler()
    logger.addHandler(handler)

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Test mail setup',
                                     epilog=textwrap.dedent(f'''\
                                     Version (version), part of:
                                     syncosync - secure peer to peer backup synchronization
                                     Copyright (C)2020 syncosync.org
                                     '''))
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")
    parser.add_argument("-ac", "--accounts", dest="accounts", action='store_true', help="Housekeeper checks all accounts")
    parser.add_argument("-r", "--remote", dest="remote", action='store_true',
                        help="checks the remotehost")
    parser.add_argument("-ad", "--admin", dest="admin", action='store_true', help="Housekeeper sends mail to admin if necessary")
    parser.add_argument("-all", "--all", dest="all", action='store_true', help="Housekeeper checks accounts remotehost and admin")
    parser.add_argument("-m", "--mail", dest="mail", help="Sends a mail to the given mail address")


    args = parser.parse_args()
    housekeeper = Housekeeper()
    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)
    if args.accounts or args.all:
        housekeeper.check_accounts()
    if args.remote or args.all:
        housekeeper.check_remotehost()
    if args.admin or args.all:
        housekeeper.remind_admin()
    if args.mail:
        config = sosconfig.SosConfig().get()
        admin_mail_addr = config.mail_settings.admin_mail_address
        lang = config.ui_language
        hostname = HostName().get().hostname
        now = time.time()
        dict ={'HOSTNAME': hostname,'LAST_ACCESS_DATE': time.strftime('%d/%m/%Y', time.localtime(now)),'LAST_ACCESS_TIME':time.strftime('%H:%M:%S', time.localtime(now))}
        msg = get_mail_msg("testmail_content", lang, dict)
        hl = get_mail_hl("testmail_hl", lang)
        body = get_html_mail(hl, msg, hostname)
        if send_mail(admin_mail_addr, args.mail, get_subject("testmail_subject", lang, hostname), body):
            print("Sending mail was successful")
        else:
            print("Mail could not be sent!")




if __name__ == '__main__':
    main()
