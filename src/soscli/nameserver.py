#!/usr/bin/env python3

"""
syncosync - secure peer to peer backup synchronization
Copyright (C) 2020  syncosync.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from soscore.nameserver import NameServer
from sosmodel import sosconstants
# following imports are all necessary for cli
import logging
import logging.handlers
import argparse
import sys


def main():
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='management of nameserver',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-g", "--get", help='get json of settings', action="store_true")
    parser.add_argument("-s", "--set", help='set hostname from json, e.g.: \'{ \"first_nameserver\": \"8.8.8.8\",'
                                            ' \"second_nameserver\":\"1.2.3.4\"}\'')
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    mynameserver = NameServer()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    if args.get:
        mynameserveroutput = mynameserver.get()
        print(mynameserveroutput.to_json())

    if args.set:
        mynewnameserver = mynameserver.get()
        mynewnameserver.update_from_json(args.set)
        mynameserver.set(mynewnameserver)


if __name__ == '__main__':
    main()
