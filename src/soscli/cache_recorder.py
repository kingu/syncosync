#!/usr/bin/env python3
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from sosmodel import sosconstants
from shutil import copyfile
import os
import time

"""
This records all possible cache files for later playback
"""

last_access = {}


def main():
    if not os.path.isdir(sosconstants.RECORD_DIR):
        os.makedirs(sosconstants.RECORD_DIR)
    starttime = round(time.time())
    files_to_monitor = [sosconstants.SOSTOCHECK_CACHE, sosconstants.DRIVESTATUS_CACHE,
                        sosconstants.SOSACCOUNTSPACE_CACHE,
                        sosconstants.SOSACCOUNTACCESS_CACHE, sosconstants.SYNCSTAT_CACHE]
    # initialise
    for file in files_to_monitor:
        last_access[file] = 0

    while True:
        # this is the loop
        nowtime = round(time.time()) - starttime
        for file in files_to_monitor:
            if os.path.isfile(file):
                if round(os.stat(file).st_mtime) > last_access[file]:
                    # ok, this file is newer
                    last_access[file] = round(os.stat(file).st_mtime)
                    copyfile(file, sosconstants.RECORD_DIR + str(nowtime) + "_" + os.path.basename(file))
                    print("Storing: " + sosconstants.RECORD_DIR + "_" + os.path.basename(file))

        time.sleep(1)


if __name__ == '__main__':
    main()
