#!/usr/bin/env python3
"""
sysstat script. This is for checking and changing the state of the system
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from soscore import sysstate
from sosmodel import sosconstants
from sosmodel.sos_enums import SystemStatus, SystemMode
# following imports are all necessary for cli
import logging
import logging.handlers
import argparse
import sys


def main():
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)

    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='state check and management',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-g", "--get", help="get system state (before and after switching)", action="store_true")
    parser.add_argument("--setup", help="switch to Setup state", action="store_true")
    parser.add_argument("--default", help="switch to Default state", action="store_true")
    parser.add_argument("--swap", help="switch to Disaster Swap state", action="store_true")
    parser.add_argument("--remote", help="switch to Remote Recovery state", action="store_true")
    parser.add_argument("--shutdown", help="switch to Shutdown state", action="store_true")
    parser.add_argument("-c", "--config", help="configuration backup file")
    parser.add_argument("-a", "--auth", help="backupfile password")
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    mystate = sysstate.SysState()

    if args.get:
        mystatestate = mystate.get()
        print(f"State (numeric): {mystatestate.sysstate}\nDecoded:")
        for line in SystemStatus.message(mystatestate.sysstate):
            print(line)
        print(f"Actual Mode: {SystemMode.message(mystatestate.actmode)}")
        print(f"Stored Mode: {SystemMode.message(mystatestate.storedmode)}")
        print(mystatestate.to_json())

    req_mode = None

    if args.setup:
        req_mode = SystemMode.SETUP
    elif args.default:
        req_mode = SystemMode.DEFAULT
    elif args.swap:
        req_mode = SystemMode.DISASTER_SWAP
    elif args.remote:
        req_mode = SystemMode.REMOTE_RECOVERY
    elif args.shutdown:
        req_mode = SystemMode.SHUTDOWN

    if req_mode is not None:
        (result, report) = mystate.changemode(req_mode, cfg_backup_filepath=args.config, config_pwd=args.auth)

        if result:
            print(f"Switching to {SystemMode.message(req_mode)} successful")
        else:
            print(f"Switching to {SystemMode.message(req_mode)} failed")
        print("Report:")
        print("\n".join(report))

        if args.get:
            mystatestate = mystate.get()
            print(f"State (numeric): {mystatestate.sysstate}\nDecoded:")
            for line in SystemStatus.message(mystatestate.sysstate):
                print(line)
            print(f"Actual Mode: {SystemMode.message(mystatestate.actmode)}")
            print(f"Stored Mode: {SystemMode.message(mystatestate.storedmode)}")
            print(mystatestate.to_json())


if __name__ == '__main__':
    main()
