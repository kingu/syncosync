#!/usr/bin/env python3
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
This is the script for sos key exchange
"""

import logging
import logging.handlers
import signal
import sys
import argparse
from threading import Event
from soscore import soshelpers, soserrors
from soscore.soskeyx import SosKeyExchange
from sosmodel import sosconstants




def main():
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)

    def sighandler(self, signum):
        # SosKeyExchange.receive_cancel_signal.set()
        SosKeyExchange.receive_cancel_signal = True
        my_logger.debug(f"received signal {signum}")
        raise soserrors.SosError("key exchange cancelled")

    signal.signal(signal.SIGUSR1, sighandler)

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='exchange of sync setup data (ssd)',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-a", "--accept", help="accept a remote key", action="store_true")
    parser.add_argument("-d", "--delete", help="delete the remote key", action="store_true")
    parser.add_argument("-g", "--get", help="get remote key info", action="store_true")
    parser.add_argument("-r", "--receive", help="receive", action="store_true")
    parser.add_argument("-s", "--send", help="send", action="store_true")
    parser.add_argument("-p", "--pretty", help="formats json output", action="store_true", default=False)
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    if args.get:
        remote_key = SosKeyExchange.get_remote_key()
        print(soshelpers.pretty_print(remote_key.to_json(), args.pretty))

    if args.delete:
        SosKeyExchange.remove_remote_key()

    if args.receive:
        fp = SosKeyExchange.receive_ssd()
        print(fp.to_json())
        exit()
    if args.send:
        fp = SosKeyExchange.send_ssd()
        print(fp.to_json())
        exit()
    if args.accept:
        SosKeyExchange.accept_key()
        exit()


if __name__ == '__main__':
    main()
