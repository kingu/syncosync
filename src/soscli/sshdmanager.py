#!/usr/bin/env python3

"""
This is the commandline access to sshd management
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from soscore import sshdmanager
from sosmodel import sos_enums, sosconstants
from sosmodel.sshdmanager import SshdConfig
import logging
import logging.handlers
import argparse
import sys


def main():
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)

    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='starting and stopping sshd instances within syncosync',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("--io", help="internal open", action="store_true")
    parser.add_argument("--ic", help="internal close", action="store_true")
    parser.add_argument("--intranet_stop", help="intranet stop", action="store_true")
    parser.add_argument("--eo", help="external open", action="store_true")
    parser.add_argument("--ec", help="external close", action="store_true")
    parser.add_argument("--extranet_stop", help="extranet stop", action="store_true")
    parser.add_argument("--si", help="set intranet from json")
    parser.add_argument("--se", help="set extranet from json")
    parser.add_argument("-g", "--get", help="get json for both", action="store_true")

    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    if args.io:
        sshdmanager.change_access(sos_enums.Sshd.INTRANET, sos_enums.SshdAccess.OPEN)
    if args.ic:
        sshdmanager.change_access(sos_enums.Sshd.INTRANET, sos_enums.SshdAccess.CLOSE)
    if args.intranet_stop:
        sshdmanager.change_access(sos_enums.Sshd.INTRANET, sos_enums.SshdAccess.OFF)
    if args.extranet_stop:
        sshdmanager.change_access(sos_enums.Sshd.EXTRANET, sos_enums.SshdAccess.OFF)
    if args.eo:
        sshdmanager.change_access(sos_enums.Sshd.EXTRANET, sos_enums.SshdAccess.OPEN)
    if args.ec:
        sshdmanager.change_access(sos_enums.Sshd.EXTRANET, sos_enums.SshdAccess.CLOSE)
    if args.si:
        mysshconfigmodel = SshdConfig.from_json(args.si)
        sshdmanager.set(sos_enums.Sshd.INTRANET, mysshconfigmodel)
    if args.se:
        mysshconfigmodel = SshdConfig.from_json(args.si)
        sshdmanager.set(sos_enums.Sshd.EXTRANET, mysshconfigmodel)
    if args.get:
        my_intranet = sshdmanager.get(sos_enums.Sshd.INTRANET)
        my_extranet = sshdmanager.get(sos_enums.Sshd.EXTRANET)
        print("Intranet:", my_intranet.to_json())
        print("Extranet:", my_extranet.to_json())


if __name__ == '__main__':
    main()
