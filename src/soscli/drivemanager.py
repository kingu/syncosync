#!/usr/bin/env python3
"""
This is script for managing and showing the drives
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import logging.handlers
import sys
import argparse
import json

from soscore import drivemanager, sshdmanager
from sosmodel import sos_enums, sosconstants


def main():
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)

    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='management of syncosync drives',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-f", "--format", help='comma separated e.g. "sda,sdb",... or "all" to use all available disks')
    parser.add_argument("-p", "--partition", help='json representation of partitioning: '
                                                  'e.g. \'{ \"local\": \"100\", \"remote\":\"100\"}\'')
    parser.add_argument("-m", "--mount",
                        help="mount all sos volumes - attention - this may not correspondent with the system state! ",
                        action="store_true")
    parser.add_argument("-u", "--umount",
                        help="unmount all sos volumes - attention - this may not correspondent with the system state! ",
                        action="store_true")
    parser.add_argument("-y", "--yes", help="confirmation for formatting", action="store_true")
    parser.add_argument("-g", "--get", help="get actual situation as json", action="store_true")
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    mydrives = drivemanager.Drives()
    mydrives.generate()
    if args.get:
        report = mydrives.to_json()
        print(json.dumps(json.loads(report), indent=4, sort_keys=True))
        exit()
    if args.mount:
        success = mydrives.mount_sos_volumes()
        if success:
            print("All sos volumes mounted - attention - this may not correspondent with the system state! ")
        else:
            print("could not mount sos volumes")
        exit()
    if args.umount:
        my_intranet = sshdmanager.get(sos_enums.Sshd.INTRANET)
        my_extranet = sshdmanager.get(sos_enums.Sshd.EXTRANET)
        if (my_intranet.pid != -1 and my_intranet.access == sos_enums.SshdAccess.OPEN) or \
                (my_extranet.pid != -1 and my_extranet.access == sos_enums.SshdAccess.OPEN):
            print("Won't unmount while sshds are in open state. Close them first.")
            exit()
        success = drivemanager.umount_sos_volumes()
        if success:
            print("All sos volumes unmounted - attention - this may not correspondent with the system state! ")
        else:
            print("could not unmount sos volumes")
        exit()
    if args.get:
        report = mydrives.to_json()
        print(json.dumps(json.loads(report), indent=4, sort_keys=True))
        exit()
    if args.format:
        drives = args.format.split(',')
        if drives[0] == "all":
            drives = mydrives.get_drivelist()
        s = ', '
        driveliststring = s.join(drives)
        if args.yes:
            print("Creating sos volume group on drives " + driveliststring)
            result = mydrives.create_sos(drives)
            if result != -1:
                print(f"Success, free space {result} extends.")
                # some calculation for a proposal...
                system = int(result / 10)
                if system > sosconstants.SYSTEM_RECOMMEND_SIZE:
                    system = sosconstants.SYSTEM_RECOMMEND_SIZE
                localremote = int((result - system) / 2)
                print("you could create volumes with \'{\"local\":", localremote, ", \"remote\":", localremote, "}\'")
        else:
            print("add -y to create sos volumes on drives " + driveliststring)
    if args.partition:
        if args.yes:
            print("Creating local and remote partitions")
            result = mydrives.create_volumes(args.partition)
            if result:
                print(f"Success!")
            else:
                print("Error")
        else:
            print(f"add -y to create volumes")


if __name__ == '__main__':
    main()
