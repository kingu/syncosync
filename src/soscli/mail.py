#!/usr/bin/env python3
"""
The Script to test and configure the email module
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging
import logging.handlers
import argparse
import sys
import textwrap
from soscore.mail import send_test_mail, update_mail_config_from_json
from soscore.sosconfig import SosConfig

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

handler = logging.handlers.SysLogHandler()
logger.addHandler(handler)

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Test mail setup',
                                     epilog=textwrap.dedent(f'''\
                                     Version (version), part of:
                                     syncosync - secure peer to peer backup synchronization
                                     Copyright (C)2020 syncosync.org
                                     '''))
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")
    parser.add_argument("-m", "--mod", dest="mod",
                        help="""Input json to modify mail setting variables, for example: '{"admin_mail_address":"example@test.com", "admin_info_period":7, "smtp_host":"smtp.gmail.com", "smtp_user":"myaddress@grmail.com", "smtp_passwd":"1234", "smtp_port":587}'""")
    parser.add_argument("-s", "--send", dest="receiver", help="Send an test eMail to the given mail, you have to configure the mail settings first!")
    parser.add_argument("-g", "--get", action='store_true',
                        help="get mail settings as json")


    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)
    if args.receiver:
        if send_test_mail(args.receiver):
            print('The mail was successfully sent!')
        else:
            print('There was a problem sending the mail')

    if args.get:
        mail_config = SosConfig().get().mail_settings
        print(mail_config.to_json())

    if args.mod:
        if update_mail_config_from_json(args.mod):
            logger.info("Successfully updated mail config")
        else:
            logger.warning("Could not update mail config. (Wrong key or values already exist?)")


if __name__ == '__main__':
    main()
