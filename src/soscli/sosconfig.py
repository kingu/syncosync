#!/usr/bin/env python3
"""
This is the managing program for managing syncosync configs.
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse

from soscore import sosconfig, soshelpers
from sosmodel import sosconstants
from sosmodel.sosconfig import SosConfigModel, SystemDescriptionModel
import logging
import sys
import logging.handlers


def main():
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Manages syncosync general configuration',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-g", "--get", help="get configuration, returns json", action="store_true")
    parser.add_argument("-d", "--description", help="get system description, returns json", action="store_true")
    parser.add_argument("-s", "--set", help="set new settings from json "
                                            "e.g. '{\"systype\": \"generic\", \"upnp\": false, \"ui_language\":"
                                            " {\"language\": 0}, \"admin_mail_address\": \"\", \"admin_info_period\":"
                                            " 7}")
    parser.add_argument("-p", "--pretty", help="formats json output", action="store_true", default=False)
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    myconfig = sosconfig.SosConfig()

    if args.set:
        # we have to get it first to change it. If it does not exist, no problem, it will be default
        mysosconfig: SosConfigModel = myconfig.get()
        mysosconfig.update_from_json(args.set)
        myconfig.set()

    if args.get:
        myconfigoutput: SosConfigModel = myconfig.get()
        print(soshelpers.pretty_print(myconfigoutput.to_json(), args.pretty))

    if args.description:
        myconfigoutput: SystemDescriptionModel = myconfig.get_system_description()
        print(soshelpers.pretty_print(myconfigoutput.to_json(), args.pretty))


if __name__ == '__main__':
    main()
