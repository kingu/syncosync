#!/usr/bin/env python3
"""
This script shows the usage of the syncstat classes. You could also start it in daemon mode e.g.
from a service script, but normally this functionality is bound into the master daemon of the
syncosync system
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import sys
import time
import syslog

from soscore import soshelpers, syncstat

import logging
import logging.handlers


def main():
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)

    parser = argparse.ArgumentParser(description='syncstat - deamon and reader')
    parser.add_argument("-d", "--daemon", help="starts as daemon (writes cache file)", action="store_true")
    parser.add_argument("-c", "--client", help="starts as client (reads cache file)", action="store_true")
    parser.add_argument("-o", "--output", help="verbose output (for daemon and client)", action="store_true")
    parser.add_argument("-l", "--loop", help="loop time in sec (0=once)", default=0)
    parser.add_argument("-p", "--pretty", help="formats json output", action="store_true", default=False)
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    if args.daemon:
        mysyncstat = syncstat.SyncStatUpdater()
        while True:
            mysyncstat.update_data()
            if args.output:
                mysyncstatoutput = mysyncstat.get()
                print(soshelpers.pretty_print(mysyncstatoutput.to_json(), args.pretty))
                # mybwhistory = mysyncstat.get_bw_history()
                # print("elements:", len(mybwhistory))
                # for element in mybwhistory:
                #     print(element.to_json())
            if args.loop == 0:
                break
            time.sleep(float(args.loop))

    if args.client:
        mysyncstat = syncstat.SyncStatReader()
        while True:
            mysyncstat.update_data()
            if args.output:
                mysyncstatoutput = mysyncstat.get()
                print(soshelpers.pretty_print(mysyncstatoutput.to_json(), args.pretty))
            if args.loop == 0:
                break
            time.sleep(float(args.loop))


if __name__ == '__main__':
    main()
