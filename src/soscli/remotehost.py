#!/usr/bin/env python3

"""
The commandline access to remotehost settings
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
import logging.handlers
import sys
from soscore import remotehost
from sosmodel import sosconstants
from sosmodel.remotehost import RemoteHostModel
from sosmodel.sos_enums import RemoteUpStatus


def main():
    # logging stuff
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)
    handler = logging.handlers.SysLogHandler(address='/dev/log')
    my_logger.addHandler(handler)

    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Manages and tests remote host and port settings',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-g", "--get", help="get settings as json", action="store_true")
    parser.add_argument("-s", "--set", help="set new settings from json "
                                            "e.g. \'{ \"hostname\": \"host.example.com\", \"port\":\"55055\"}\'")
    parser.add_argument("-c", "--check", help="check, if configured host is reachable", action="store_true")
    parser.add_argument("-o", "--other", help="check if other host is reachable defined by json")
    parser.add_argument("-k", "--keyfile", help="private keyfile (for -o)")
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")
    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        my_logger.addHandler(stdout_handler)

    if args.get:
        myremotehost = remotehost.RemoteHost()
        myremotehostoutput = myremotehost.get()
        print(myremotehostoutput.to_json())
        exit()

    if args.set:
        myremotehost = remotehost.RemoteHost()
        myremotehostoutput = myremotehost.get()
        myremotehostoutput.update_from_json(args.set)
        print(myremotehostoutput.to_json())
        myremotehost.set(myremotehostoutput)
        exit()

    if args.other:
        myremotehostinput = RemoteHostModel.from_json(args.other)
        print("Checking Host", myremotehostinput.hostname, "Port", myremotehostinput.port, "Keyfile", args.keyfile)
        result: RemoteUpStatus = remotehost.check_host(myremotehostinput.hostname, myremotehostinput.port, args.keyfile)
        print(result, ":", RemoteUpStatus.message(result))
        if result == RemoteUpStatus.UP:
            exit(0)
        else:
            exit(1)

    if args.check:
        myremotehost = remotehost.RemoteHost()
        result: RemoteUpStatus = myremotehost.check_host()
        print(result, ":", RemoteUpStatus.message(result))
        if result == RemoteUpStatus.UP:
            exit(0)
        else:
            exit(1)


if __name__ == '__main__':
    main()
