#!/usr/bin/env python3
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pwd
import time
import grp
import argparse
from typing import Optional

from soscore.soscoreManager import SoscoreManagerManager, SoscoreManager
from soscore import sshdmanager
from sosmodel import sosconstants
from multiprocessing import Event
from sosmodel.sos_enums import Sshd, SshdAccess

from sosui.sosui import Sosui
import logging
import logging.handlers
import signal
import sys


class Syncosync:
    def __init__(self, application_args):
        SoscoreManagerManager.register('SoscoreManager', SoscoreManager)
        self.__soscore_manager_manager: SoscoreManagerManager = SoscoreManagerManager()
        self.__soscore_manager: Optional[SoscoreManager] = None
        self.__sosui: Optional[Sosui] = None
        self.__application_args = application_args

    def start(self):
        if self.__sosui is not None or self.__soscore_manager is not None:
            logger = logging.getLogger(__name__)
            logger.fatal("Cannot start sosui or core twice")
            return
        self.__soscore_manager_manager.start()
        self.__soscore_manager: SoscoreManager = self.__soscore_manager_manager.SoscoreManager()
        self.__start_sosui()

    def __start_sosui(self):
        self.__sosui = Sosui(self.__soscore_manager, self.__application_args.root_path, self.__application_args.debug)

        uid = pwd.getpwnam('root').pw_uid  # should always be 0, but you never know
        if os.getuid() == uid:
            logger = logging.getLogger(__name__)
            logger.debug("Running as root")
            uid_to_run_sosui_with = pwd.getpwnam('www-data').pw_uid
            gid_sosui_is_run_with = grp.getgrnam('www-data').gr_gid

            # TODO: pass as config parameter
            fullname = self.__soscore_manager_manager.address
            dirname = os.path.dirname(fullname)
            os.chown(dirname, 0, gid_sosui_is_run_with)
            os.chmod(dirname, 0o770)

            os.chown(fullname, 0, gid_sosui_is_run_with)
            os.chmod(fullname, 0o770)

            self.__sosui.start(uid_to_run_sosui_with, gid_sosui_is_run_with)
        else:
            # TODO: only call this while developing with intelliJ/angular!
            self.__sosui.start()

    def shutdown(self):
        logger = logging.getLogger(__name__)
        logger.info("Syncosync shutting down")
        # Place any code to run in soscore before the shutdown!
        self.__soscore_manager.shutdown_soscore()
        self.__sosui.stop_sosui()

        # No funny business afterwards
        logger.info("Stopping soscore")
        self.__soscore_manager_manager.shutdown()
        self.__sosui.terminate()
        logger.info("Joining sosui")
        self.__sosui.join()


def main():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log')
    logger.addHandler(handler)
    stop_event: Event = Event()
    main_pid: int = os.getpid()
    logger.info("Main PID: " + str(main_pid))

    def signal_handler(sig, frame):
        if os.getpid() != main_pid:
            logger.warning("Signal in subprocess")
            if sig == signal.SIGTERM and stop_event.is_set():
                logger.info("Received SIGTERM and Syncosync is to be stopped")
                signal.signal(signal.SIGTERM, signal.SIG_DFL)
                os.kill(os.getpid(), signal.SIGTERM)
        logger.info(f"Received signal: {sig}")
        if not stop_event.is_set():
            logger.info("Setting stop signal")
        stop_event.set()

    # catches signal in all subprocesses for a proper shutdown
    signal.signal(signal.SIGINT, signal_handler)

    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='the syncosync main program (daemon)',
                                     epilog=sosconstants.EPILOG)
    parser.add_argument("-d", "--debug", help="do not detach and send logging also to stdout",
                        default=False, action='store_true')
    parser.add_argument('--root-path', help="Set location of Flask root", type=str,
                        default=sosconstants.STATIC_BASE_DIR)
    parser.add_argument("-v", "--verbosity", dest="verbosity", choices=['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR',
                                                                        'CRITICAL'],
                        default='WARNING', help="Set the logging level (default: %(default)s)")

    args = parser.parse_args()

    if args.verbosity:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(level=logging.getLevelName(args.verbosity))
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)

    syncosync: Syncosync = Syncosync(args)
    syncosync.start()

    # SIGTERM should only be handled by the main process, subprocesses cannot be stopped otherwise (except for
    # cases where we propagate the SIGTERM. Since adding individual SIGTERM handlers each time is not what we want,
    # we will just stick to this way of handling it for now
    signal.signal(signal.SIGTERM, signal_handler)
    try:
        while not stop_event.is_set():
            time.sleep(1)
    except KeyboardInterrupt or Exception:
        logger.info("Received (keyboard) interrupt")
    finally:
        syncosync.shutdown()
        sshdmanager.change_access(Sshd.INTRANET, SshdAccess.OFF)
        sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
        os.system("service sosaccountaccess stop")
        os.system("service lsyncd stop")
        sys.exit(0)


if __name__ == '__main__':
    main()
