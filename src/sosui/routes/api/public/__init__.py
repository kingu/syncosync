# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from flask import Flask

from soscore.soscoreManager import SoscoreManager
from sosui.routes.api.public.bandwidth_history import PublicBandwidthHistoryEndpoint
from sosui.routes.api.public.hostname_endpoint import PublicHostnameEndpoint
from sosui.routes.api.public.sync_status_endpoint import SyncStatusEndpoint
from sosui.routes.api.public.ui_language_endpoint import UiLanguageEndpoint


def register_public_endpoints(flask_app: Flask, soscore_manager: SoscoreManager):
    flask_app.add_url_rule("/api/public/hostname", "public/hostname",
                           PublicHostnameEndpoint(flask_app, soscore_manager),
                           methods=["GET"])
    flask_app.add_url_rule("/api/public/bandwidth_history", "public/bandwidth_history",
                           PublicBandwidthHistoryEndpoint(flask_app, soscore_manager),
                           methods=["GET"])
    flask_app.add_url_rule("/api/public/status/sync_status", "public/sync_status",
                           SyncStatusEndpoint(flask_app, soscore_manager),
                           methods=["GET"])
    flask_app.add_url_rule("/api/public/ui_language", "public/ui_language",
                           UiLanguageEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST"])
