# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import crypt
import json
from typing import Optional

from flask import Flask, request, Response

from soscore.soscoreManager import SoscoreManager
from sosmodel.sosaccount import SosAccountAddModel
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes


class AddAccountEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["POST"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None) -> Response:
        new_user: SosAccountAddModel = SosAccountAddModel.from_json(request.data)

        if new_user.name is None or len(new_user.name) == 0:
            # return error!
            return SosuiEndpoint.get_response(response_status=StatusCodes.INTERNAL_SERVER_ERROR)
        # hash the PW right now...
        new_user.password = crypt.crypt(new_user.password)

        self._soscore_manager.account_add(new_user)
        return SosuiEndpoint.get_response(response_status=StatusCodes.OK)
