# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask

from soscore.soscoreManager import SoscoreManager
from sosui.routes.api.setup.hostname_endpoint import HostnameEndpoint
from sosui.routes.api.setup.key_exchange_cancel import KeyExchangeCancelEndpoint
from sosui.routes.api.setup.key_exchange_follow import KeyExchangeFollowEndpoint
from sosui.routes.api.setup.key_exchange_lead import KeyExchangeLeadEndpoint
from sosui.routes.api.setup.local_key_endpoint import LocalKeyEndpoint
from sosui.routes.api.setup.nic_config_endpoint import NicConfigEndpoint
from sosui.routes.api.setup.remote_host_endpoint import RemoteHostEndpoint
from sosui.routes.api.setup.remote_key_endpoint import RemoteKeyEndpoint
from sosui.routes.api.setup.resolv_config_endpoint import ResolvConfigEndpoint
from sosui.routes.api.setup.ssh_config_endpoint import SshConfigEndpoint


def register_setup_endpoints(flask_app: Flask, soscore_manager: SoscoreManager):
    flask_app.add_url_rule("/api/setup/hostname", "hostname",
                           HostnameEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST"])
    flask_app.add_url_rule("/api/setup/nic_config", "nic_config",
                           NicConfigEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST"])
    flask_app.add_url_rule("/api/setup/resolv_config", "resolv_config",
                           ResolvConfigEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST"])
    flask_app.add_url_rule("/api/setup/ssh_config", "ssh_config",
                           SshConfigEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST"])
    flask_app.add_url_rule("/api/setup/local_key", "local_key",
                           LocalKeyEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST"])
    flask_app.add_url_rule("/api/setup/remote_host", "remote_host",
                           RemoteHostEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST"])
    flask_app.add_url_rule("/api/setup/remote_key", "remote_key",
                           RemoteKeyEndpoint(flask_app, soscore_manager),
                           methods=["GET", "POST", "DELETE"])
    flask_app.add_url_rule("/api/setup/key_exchange_cancel", "key_exchange_cancel",
                           KeyExchangeCancelEndpoint(flask_app, soscore_manager),
                           methods=["POST"])
    flask_app.add_url_rule("/api/setup/key_exchange_lead", "key_exchange_lead",
                           KeyExchangeLeadEndpoint(flask_app, soscore_manager),
                           methods=["POST"])
    flask_app.add_url_rule("/api/setup/key_exchange_follow", "key_exchange_follow",
                           KeyExchangeFollowEndpoint(flask_app, soscore_manager),
                           methods=["POST"])
