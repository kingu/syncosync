import json
from typing import List, Union, Optional

from flask import request, Flask, Response

from soscore.soscoreManager import SoscoreManager
from sosmodel.nicconfig import NicConfigModel, NicMode
from sosmodel.serializerbase import SerializerBaseJsonDecoder, SerializerBaseJsonEncoder
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes


class NicConfigEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["GET", "POST"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None) -> Response:
        if request.method == "GET":
            # List of nicConfig
            nics: List[NicConfigModel] = self._soscore_manager.get_nic_configs()
            return SosuiEndpoint.get_response(response_status=StatusCodes.OK,
                                              payload=json.dumps(nics, cls=SerializerBaseJsonEncoder,
                                                                 sort_keys=True, indent=4))

        elif request.method == "POST":
            nics: Union[NicConfigModel, List[NicConfigModel]] = \
                SerializerBaseJsonDecoder.decode(cls=NicConfigModel, serialized_string=request.data)

            self._soscore_manager.set_nic_configs(nics)
            return SosuiEndpoint.get_response(response_status=StatusCodes.OK,
                                              payload=json.dumps(nics, cls=SerializerBaseJsonEncoder,
                                                                 sort_keys=True, indent=4))
